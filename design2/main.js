$(".button-save").on("click", function() {
	
	if ($(this).hasClass("saved")) {
		$(this).removeClass( "saved" );;
		$(this).children(".saved-icon-click").attr("src", "icons-imgs/svgs/noun_bookmark_1633754.svg");
		$(this).children("span").text("Save");
	}else{
		$(this).addClass( "saved" );;
		$(this).children(".saved-icon-click").attr("src", "icons-imgs/svgs/noun_bookmark_1052523.svg");
		$(this).children("span").text("Saved");
	}
})

$(".button-add-to-shopify").on("click", function() {
	$(this).css("backgroundColor", "#ffbe8e")
});


var icons = new Array ('icons-imgs/plus-icon.png', 'icons-imgs/minus-icon.png')

$(".question-number").on("click", function() {
	$(this).children(".question-text").slideToggle()
	var currentImg = parseInt($(this).children("img").attr("data-expanded"))

	if(currentImg === 0) currentImg = 1
	else currentImg = 0

	$(this).children('.plus-icon').attr('src', icons[currentImg])
	$(this).children('.plus-icon').attr('data-expanded', currentImg);
});

$(".button-plus").on("click", function()
{
	var $el = $(this);

	$(this).css("backgroundColor", $el.attr('data_bg_on_click'));//"#f47b24")
	$(this).children("img").attr("src", $el.attr('data_src_on_click'));//"icons-imgs/noun-plus-active.png")
});

