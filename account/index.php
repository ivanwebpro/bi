<?php

error_reporting(E_ALL); ini_set('display_errors', 1);

include_once dirname(__DIR__)."/classes/common/code/dbg.php";
//hre($_SERVER);
require($app_root . '/vendor/autoload.php');
require($app_root . '/vendor/yiisoft/yii2/Yii.php');
require($app_root . '/classes/common/config/bootstrap.php');
require($app_root . '/classes/account/config/bootstrap.php');

$fs = [
    '/classes/common/config/main.php',
    '/classes/common/config/main-local.php',
    '/classes/account/config/main.php',
    '/classes/account/config/main-local.php',
];

$config = [];

foreach($fs as $f)
{
	$ff = $app_root.$f;
	if (file_exists($ff))
	{
		$config = yii\helpers\ArrayHelper::merge($config, require $ff);
	}
}

//$application = new yii\web\Application($config);
$application = new account\components\Application($config);

if (!isset($only_init_yii))
{
	$application->run();
}
