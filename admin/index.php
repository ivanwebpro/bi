<?php

$app_root = (dirname(__DIR__));
$app_root_classes = $app_root."/classes";

include_once $app_root_classes."/common/code/dbg.php";



if (
	true ||
	isLocalHost())
{
	defined('YII_DEBUG') or define('YII_DEBUG', true);
}

defined('YII_ENV') or define('YII_ENV', 'dev');


require($app_root . '/vendor/autoload.php');
require($app_root . '/vendor/yiisoft/yii2/Yii.php');
require($app_root_classes . '/common/config/bootstrap.php');
require($app_root_classes . '/admin/config/bootstrap.php');

$fs = [
    '/common/config/main.php',
    '/common/config/main-local.php',
    '/admin/config/main.php',
    '/admin/config/main-local.php',
];

$config = [];

foreach($fs as $f)
{
	$ff = $app_root_classes.$f;
	if (file_exists($ff))
	{
		$config = yii\helpers\ArrayHelper::merge($config, require $ff);
	}
}

$application = new admin\components\Application($config);

if (!isset($only_init_yii))
{
	$application->run();
}
