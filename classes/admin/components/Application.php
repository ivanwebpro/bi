<?php

namespace admin\components;

use common\components\Application as CommonApplication;
use common\helpers\Url;
use common\models\Order;
use admin\models\AppCfg;
use Yii;

class Application extends CommonApplication
{

	public $cron = 0;
	public $debug = true;
	public $preview = false;
	public $debug_data = '';
 	public $proxy_page = '';






	public function redirectAfterLogin()
	{
  	$u = ["store-suggested-admin/index"];
		Yii::$app->response->redirect($u);
		Yii::$app->end();
	}

	public function run()
	{

		//hr(\common\components\Options::st()->get('t1'));
		//hr(\common\components\Options::st()->getTimeDeltaFromNow('t1'));


  	$res = parent::run();

		return $res;
	}


}