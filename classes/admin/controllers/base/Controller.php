<?php

namespace admin\controllers\base;

use yii\web\Controller as YiiController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\helpers\Url;
use common\models\logs\PageLog;

class Controller extends YiiController
{
	public $layout = 'admin_layout';

 	public function behaviors()
  {
      return [
          'access' => [
              'class' => AccessControl::className(),
              'rules' => [
                	// allow authenticated users
                	[
                    'allow' => true,
                    'roles' => ['@'],
                	],
              ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }

	public function redirect($url, $statusCode = 302)
	{
		/*if (isset($_COOKIE['XDEBUG_SESSION']))
		{
			$url_s = Url::to($url);
			$msg = "<A href='$url_s'>Manual Redirect</A>";
			return $this->render('cron', ['msg' => $msg]);
		} */

  	return parent::redirect($url, $statusCode);
	}

  public function beforeAction($action)
	{
		if ($this->id != 'cron')
		{
			//PageLog::log();
		}

		return parent::beforeAction($action);
	}
}