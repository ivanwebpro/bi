<?php

namespace admin\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\db\Expression;
use admin\models\Preferences;
use admin\models\User;
use admin\models\AppCfg;
use admin\models\forms\LoginForm;

class ErrorController extends base\Controller
{

	public function actions()
	{
		return [
			'message' => [
					'class' => 'yii\web\ErrorAction',
				],
			];
	}

	public function behaviors()
	{
  	$bs = parent::behaviors();

		$r = [
    	'allow' => true,
			'actions' => ['message'],
      ];

		$bs['access']['rules'][] = $r;

		return $bs;
	}
}
