<?php

namespace admin\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\db\Expression;
use admin\models\Preferences;
use admin\models\AdminUser;
use admin\models\AppCfg;
use admin\forms\LoginForm;


class UserController extends base\Controller
{
 	public function actionRecurly()
	{
  	$a = new \common\apis\RecurlyApi;

		$res = \Recurly_SubscriptionList::get();
		hrb('Recurly_SubscriptionList');
		hr($res);

		foreach($res as $el)
		{
    	hr($el->uuid);
    	hr($el->plan->name);
    	hr($el->state);
    	//hr($el->account);
			$acc = $el->account->get();
    	hr($acc->email);
    	//hr("Acc: $el->account");
			//hr($el);
			exit;

			//print "el: $el";
		}
		exit;

		$res = \Recurly_AccountList::getActive();
		hrb('Recurly_AccountList');
		hr($res);

		foreach($res as $el)
		{
    	hr($el);
		}
		//Private API Key: 7e245613d55143138ab0645d54864b5c
		//Public key: ewr1-dqF5XYBovTLuV9oKMqSgz2
		//James set up a $1 subscription under my account: jamesbeattie@ecominsiders.co
	}

 	public function actionInit()
	{
  	AdminUser::createAdmin();
		print "OK";

	}

 	public function actionLogout()
	{
		Yii::$app->user->logout();
		return $this->goHome();
	}

 	public function actionLogin()
	{

		if (!\Yii::$app->user->isGuest)
		{
			return $this->redirect(['store-suggested-admin/index']);
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login())
		{
			return $this->redirect(['store-suggested-admin/index']);
		}
		else
		{
			return $this->render('login-user', ['model' => $model]);
		}
	}


	public function behaviors()
	{
  	$bs = parent::behaviors();

		$r = [
    	'allow' => true,
			'actions' => ['login', 'init', 'logout', 'recurly'],
      ];

		$bs['access']['rules'][] = $r;

		return $bs;
	}
}
