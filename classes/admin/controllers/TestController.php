<?php

namespace admin\controllers;

use Yii;
use common\apis\TelegramApi;

use account\models\Account;
use yii\data\ActiveDataProvider;


use yii\data\Sort;
use yii\data\ArrayDataProvider;

use yii\base\UserException;

use account\models\Alert;
use account\models\User;
use account\models\Store;
use account\models\LogAppUninstall;
use common\helpers\Url;
use common\helpers\DatesHelper;
use common\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\db\Expression;
use account\models\Shop;
use account\models\ShopifyAuthForm;
use account\models\LogAlert;
use account\models\LogImport;
use account\models\Product;
use yii\db\Query;
use admin\models\AppCfg;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use account\helpers\CronHelper;

class TestController extends base\Controller
{
  public $defaultAction = 'index';



	public function actionProductRefresh()
	{
    $q = Product::find();
		$q_pushed = LogImport::find()->select('product_id');
		$q->andWhere(['IN', 'id', $q_pushed]);

		$ps = $q->all();
		foreach($ps as $p)
		{
    	$p->pushed = 1;
			$p->saveEx();
      hr("Product updated: $p->title");
		}

	}

	public function actionStoreRefresh()
	{
		$ss = Store::find()->all();

		foreach($ss as $s)
		{
			$s->saveEx();
    	hr("Store updated: $s->domain");
		}

		hr("OK");
	}



}
