<?php

namespace admin\controllers;

use Yii;

use account\models\Account;
use account\models\SuggestedStore;
use yii\data\ActiveDataProvider;


use yii\data\Sort;
use yii\data\ArrayDataProvider;

use yii\base\UserException;

use account\models\Contact;
use account\models\AppActivateForm;
use account\models\Symbol;
use account\models\Tick;
use account\models\Alert;
use account\models\User;
use account\models\Candle;
use account\models\LogAppUninstall;
use common\helpers\Url;
use common\helpers\DatesHelper;
use common\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\db\Expression;
use account\models\Shop;
use account\models\ShopifyAuthForm;
use account\models\LogAlert;
use admin\models\Category;
use yii\db\Query;
use admin\models\AppCfg;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use account\helpers\CronHelper;

class StoreSuggestedAdminController extends base\Controller
{
  public $defaultAction = 'index';


	public function actionDelete($id)
	{
  	$m = $this->findModel($id);
		$m->delete();
		$EUF = ucfirst($this->entity_name);
		Yii::$app->session->setFlash('success', "Store has been deleted");
		return $this->redirect(['index']);
	}



	public function actionIndex()
	{
		$ec = $this->entity_class;
		$q = $ec::find();

		$tn = Category::tableName();
		$q->innerJoin($tn, "category_id = $tn.id");
		$q->orderBy(["$tn.title" => SORT_ASC, "domain" => SORT_ASC]);

		$ks = ['id', 'title'];
    foreach($ks as $k)
		{
			$s_attrs[$k] = ['default' => SORT_ASC];
		}
		$sort = new Sort([
    	'attributes' => $s_attrs,
			'defaultOrder' => [$ks[0] => SORT_DESC],
		]);


		$dp = new ActiveDataProvider(
			[
				'query' => $q,
				'pagination' => false,
				'sort' => $sort,
			]);

		$r = [];
    $r['dp'] = $dp;
		return $this->render("index-$this->entity_alias", $r);
	}

	public function actionCreate()
	{
		$ec = $this->entity_class;
		$model = new $ec;

		if ($model->load(Yii::$app->request->post()) && $model->save())
		{
			Yii::$app->session->setFlash('success', 'Data saved');
			return $this->redirect(['index']);
		}
		else
		{
			$r = [];
			$r['model'] = $model;
			return $this->render("create-$this->entity_alias", $r);
		}
	}


	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save())
		{
			Yii::$app->session->setFlash('success', 'Data saved');
			return $this->redirect(['index']);
		}
		else
		{
			$r = [];
			$r['model'] = $model;
			return $this->render("update-$this->entity_alias", $r);
		}
	}


	public $entity_name = 'Suggested store';
	public $entity_name_pl = 'Manage suggested stores';
	public $entity_alias = 'ss';
	public $entity_class = "account\\models\\StoreSuggested";

	protected function findModel($id)
	{
		$ec = $this->entity_class;

		if ($model = $ec::findOne($id))
		{
			return $model;
		}
		else
		{
			throw new NotFoundHttpException("The requested $this->entity_name does not exist");
		}
  }

}
