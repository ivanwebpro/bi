<?php

namespace admin\controllers;

use Yii;
use common\apis\TelegramApi;

use account\models\Account;
use yii\data\ActiveDataProvider;


use yii\data\Sort;
use yii\data\ArrayDataProvider;

use yii\base\UserException;

use account\models\Contact;
use account\models\AppActivateForm;
use account\models\Symbol;
use account\models\Tick;
use account\models\Alert;
use account\models\User;
use account\models\Candle;
use account\models\LogAppUninstall;
use common\helpers\Url;
use common\helpers\DatesHelper;
use common\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\db\Expression;
use account\models\Shop;
use account\models\ShopifyAuthForm;
use account\models\LogAlert;
use yii\db\Query;
use admin\models\AppCfg;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use account\helpers\CronHelper;

class StatController extends base\Controller
{
  public $defaultAction = 'index';



	public function actionStores()
	{
    $q = \account\models\Store::find();
		$q->select(['domain', 'count' => 'COUNT(*)']);
    $q->groupBy('domain');
		$q->andWhere(['NOT', ['domain' => null]]);
		$q->orderBy(['count' => SORT_DESC]);

		$cmd = $q->createCommand();
		$vs = $cmd->queryAll();

		$dp = new ArrayDataProvider(
			[
				'allModels' => $vs,
				//'pagination' => false,
				//'sort' => $sort,
			]);

		$r = [];
    $r['dp'] = $dp;
		return $this->render("stores-stat", $r);
	}

	public function actionProducts()
	{
    $q = \account\models\LogImport::find();
		$q->select(['title', 'count' => 'COUNT(*)']);
    $q->andWhere(['NOT', ['title' => null]]);
    $q->groupBy('title');
		$q->orderBy(['count' => SORT_DESC]);

		$cmd = $q->createCommand();
		$vs = $cmd->queryAll();

		$dp = new ArrayDataProvider(
			[
				'allModels' => $vs,
				//'pagination' => false,
				//'sort' => $sort,
			]);

		$r = [];
    $r['dp'] = $dp;
		return $this->render("products-stat", $r);
	}


}
