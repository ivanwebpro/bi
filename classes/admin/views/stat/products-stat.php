<?php
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\widgets\ActiveForm;
use common\widgets\SelObj;
use common\widgets\AjaxLoad;
use common\grid\BoolColumn;
use common\grid\DeltaColumn;
use common\grid\DecimalColumn;
use common\helpers\Url;
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\web\JsExpression;

$this->title = "Top products";

?>

<h1><?=$this->title?></h1>


<?php


	$cs = [];

	$cs[] = [
		//'label' => '',
		'attribute' => 'title',
	];

	$cs[] = [
		//'label' => '',
		'attribute' => 'count',
	];



	echo GridView::widget([
		//'summary' => '',
		'dataProvider' => $dp,
		'emptyText' => 'Nothing found',
  	'columns' => $cs,
	]);
