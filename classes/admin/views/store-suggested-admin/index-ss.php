<?php
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\widgets\ActiveForm;
use common\widgets\SelObj;
use common\widgets\AjaxLoad;
use common\grid\BoolColumn;
use common\grid\DeltaColumn;
use common\grid\DecimalColumn;
use common\helpers\Url;
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii2mod\c3\chart\Chart as C3Chart;
use yii\web\JsExpression;

$enm = $this->context->entity_name_pl;
$this->title = $enm;

?>

<h1><?=$this->title?></h1>


<p>
	<?=Html::a("Add suggested store", ["create"], ['class' => 'btn btn-primary']);?>
</p>


<?php



	$cs = [];

	$cs[] = [
		'label' => 'Category',
		'attribute' => 'category.title',
	];

	$cs[] = [
		//'label' => '',
		'attribute' => 'domain',
	];

	/*$cs[] = [
		'label' => 'URL',
		'attribute' => 'url',
	];*/



	$cs[] = [
		'class' => ActionColumn::className(),
		'template' => '{update} {delete}',
	];


	echo GridView::widget([
		'summary' => '',
		'dataProvider' => $dp,
		'emptyText' => 'Nothing found',
  	'columns' => $cs,
	]);
