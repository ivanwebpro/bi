<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap\Alert as AlertOne;
use common\widgets\LoadingAnimation;

/* @var $this \yii\web\View */
/* @var $content string */

//hre(123);
//hre(AppAsset::register($this));
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<link rel="icon" type="image/png" href="<?=appRootWeb();?>/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?=appRootWeb();?>/favicon-16x16.png" sizes="16x16" />

    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
<?php
  echo LoadingAnimation::widget();
?>

    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => Yii::$app->name.": App Admin Area",
								//'brandImage' => Yii::$app->appCfg->logoUrl,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar navbar-inverse navbar-fix1ed-top',
                ],
            ]);

						$menuItems = [];

            if (Yii::$app->user->isGuest)
						{
            	$menuItems[] = ['label' => 'Signup', 'url' => ['/app/login']];
            }
						else
						{

							$menuItems[] = [
								'label' => 'Contact requests',
                'url' => ['contact/index'],
							];

							$menuItems[] = [
								'label' => 'Shops',
                'url' => ['shop/index'],
							];

							/*$menuItems[] = [
								'label' => 'Categories',
                'url' => ['category/index'],
							];*/

							$menuItems[] = [
								'label' => 'Suggested stores',
                'url' => ['store-suggested-admin/index'],
							];

							$menuItems[] = [
								'label' => 'Pages',
                'url' => ['page/index'],
							];

							$menuItems[] = [
								'label' => 'Plans',
                'url' => ['plan/index'],
							];

							$menuItems[] = [
								'label' => 'Subscriptions',
                'url' => ['subscription/index'],
							];

						$menuItems[] = [
								'label' => 'Statistics',
                'items' => [
									[
										'label' => 'Top stores',
                  	'url' => ['stat/stores'],
									],
									[
										'label' => 'Top products',
                  	'url' => ['stat/products'],
									]
								]
							];

							$menuItems[] = [
								'label' => 'Settings',
                'url' => ['setting/index'],
							];

							$menuItems[] = [
								'label' => 'Logout',
                'url' => ['user/logout'],
                //'linkOptions' => ['data-method' => 'post'],
							];
						}


            $menuItems2 = [
                //['label' => 'Help', 'url' => ['/app/help']],
            ];

						$menuItems = array_merge($menuItems, $menuItems2);

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>

        <div class="container">
        <?php

?>

<div class='row'>
	<div class='col-xs-12'>

		<?php
    	if (!empty($shop) && $shop->uninstalled)
			{
      	$un = "App has been uninstalled from your Shopify store, no orders available. ".Html::a("Click to connect", ['app/index', 'shop' => $shop->host]);

				echo AlertOne::widget([
 					'body' => $un,
       		'closeButton' => false,
       		'options' => ['class' => 'alert-danger'],
       	]);
				echo "<br><br>";
			}
		?>

		<?= Alert::widget() ?>
	</div>
</div>





        <?= $content ?>
				<br><br><br>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <!--<p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>-->
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
