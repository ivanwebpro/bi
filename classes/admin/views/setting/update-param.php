<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

$this->title = "Settings";


?>
<?php
	$form = ActiveForm::begin();
?>


<div class='row'>
	<div class='col-xs-12'>
		<h1><?=$this->title?></h1>

		<?= $form->errorSummary($model); ?>
		<?php
			echo $form->field($model, 'purchase_link');
			//echo $form->field($model, 'exclude_from_billing')->textArea(['rows' => 4]);
 			echo $form->submitButton("Save");
		?>
	</div>
</div>
<br><br><br>
<?php
	ActiveForm::end();

?>
<br><br><br>
<?php
