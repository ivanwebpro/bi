<?php
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\widgets\ActiveForm;
use common\widgets\SelObj;
use common\widgets\AjaxLoad;
use common\grid\BoolColumn;
use common\grid\DeltaColumn;
use common\grid\DecimalColumn;
use common\helpers\Url;
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii2mod\c3\chart\Chart as C3Chart;
use yii\web\JsExpression;

$enm = $this->context->entity_name_pl;
$this->title = $enm;

?>

<h1><?=$this->title?></h1>


<?php

	$cs = [];

	$cs[] = 'name';

	$cs[] = [
		'label' => 'Store URL',
		'format' => 'raw',
		'value' => function($m)
		{
			$u = $m->store_url;
			$r = Html::a($u, $u, ['target'=>'_blank']);
			return $r;
		}
	];

	$cs[] = 'email';


	$cs[] = [
		'label' => 'Inquiry',
		'format' => 'raw',
		'value' => function($m)
		{
			$r = str_replace("\n", "<br>", $m->inquiry);
			return $r;
		}
	];


	echo GridView::widget([
		//'summary' => '',
		'dataProvider' => $dp,
		'emptyText' => 'Nothing found',
  	'columns' => $cs,
	]);
