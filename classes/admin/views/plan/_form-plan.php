<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\datecontrol\DateControl;
use kartik\daterange\DateRangePicker;
use kartik\file\FileInput;
use yii\redactor\widgets\Redactor;

?>

<?php
	$form = ActiveForm::begin();
?>


<div class='row'>
	<div class='col-xs-12'>
		<h1><?=$this->title?></h1>

		<p><i><?=$model->plan_description?></i></p>

		<?= $form->errorSummary($model); ?>
		<?php
			echo $form->field($model, 'app_access')->checkbox();
			echo $form->field($model, 'stores_count');
			echo $form->field($model, 'stores_count_unlimited')->checkbox();
			echo $form->submitButton("Save");
		?>
	</div>
</div>
<br><br><br>
<?php
	ActiveForm::end();

