<?php
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\widgets\ActiveForm;
use common\widgets\SelObj;
use common\widgets\AjaxLoad;
use common\grid\BoolColumn;
use common\grid\DeltaColumn;
use common\grid\DecimalColumn;
use common\helpers\Url;
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii2mod\c3\chart\Chart as C3Chart;
use yii\web\JsExpression;

$enm = $this->context->entity_name_pl;
$this->title = $enm;

?>

<h1><?=$this->title?></h1>





<?php

	$cs = [];

	$cs[] = [
		'label' => 'Code',
		'attribute' => 'plan_code',
	];

	$cs[] = [
		'label' => 'Name',
		'attribute' => 'plan_name',
	];

	$cs[] = [
		'label' => 'Description',
		'attribute' => 'plan_description',
	];

	$cs[] = [
		//'label' => '',
		'class' => BoolColumn::className(),
		'attribute' => 'app_access',
	];

	$cs[] = [
		//'label' => '',
		'attribute' => 'stores_count',
	];

	$cs[] = [
		//'label' => '',
		'class' => BoolColumn::className(),
		'attribute' => 'stores_count_unlimited',
	];



	$cs[] = [
		'class' => ActionColumn::className(),
		'template' => '{update}',
	];

/*$cs[] = [
  	'label' => 'Cancel',
		'format' => 'raw',
		'value' => function($m)
		{
			$r = '';
			$u = ['cancel', 'id' => $m->id];
      $r .= Html::a("Cancel", $u, ['class' => 'btn btn-danger']);
			return $r;
		}
	];*/


	echo GridView::widget([
		//'summary' => '',
		'dataProvider' => $dp,
		'emptyText' => 'Nothing found',
  	'columns' => $cs,
	]);
