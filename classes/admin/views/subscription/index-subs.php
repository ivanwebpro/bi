<?php
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\widgets\ActiveForm;
use common\widgets\SelObj;
use common\widgets\AjaxLoad;
use common\grid\BoolColumn;
use common\grid\DeltaColumn;
use common\grid\DecimalColumn;
use common\helpers\Url;
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii2mod\c3\chart\Chart as C3Chart;
use yii\web\JsExpression;

$enm = $this->context->entity_name_pl;
$this->title = $enm;

?>

<h1><?=$this->title?></h1>


<?php

	$cs = [];

	$cs[] = [
		'label' => 'Codes',
		'format' => 'raw',
		'value' => function($m)
		{
			$r = '';
			$r .= "<b>Account:</b><br>$m->account_code<br>";
			$r .= "<b>Subscription:</b><br>$m->recurly_id<br>";
			return $r;
		}
	];

	$cs[] = [
		'label' => 'Plan',
		'attribute' => 'plan_code',
		'format' => 'raw',
		'value' => function($m)
		{
			$r = '';
			$r .= "<b>$m->plan_code</b><br>";
			$r .= $m->plan_name;
			return $r;
		}
	];

	/*$cs[] = [
		//'label' => '',
		'attribute' => 'plan_name',
	];*/

	$cs[] = [
		//'label' => '',
		'attribute' => 'state',
	];

	$cs[] = [
		'label' => 'Current period ends at',
		'attribute' => 'current_period_ends_at',
	];






	/*$cs[] = [
		//'label' => '',
		'attribute' => 'account_email',
	];*/

$cs[] = [
  	'label' => 'Email',
		'format' => 'raw',
		'value' => function($m)
		{
			$r = '';
			$r .= $m->account_email."<br>";
			$u = ['cancel', 'id' => $m->id];

			if ('active' == $m->state || 'future'  == $m->state) 
			{
				$opts = ['class' => 'btn btn-danger'];
				$opts['data-confirm'] = "Are you sure want to cancel $m->plan_name subscription for $m->account_email?";
      	$r .= Html::a("Cancel", $u, $opts);
			}

			return $r;
		}
	];

	$cs[] = [
		'label' => 'Shopify store',
		//..'attribute' => '',
		'value' => function($m)
		{
			if ($m->shopify_name)
			{
    		return "$m->shopify_name.myshopify.com";
			}
		}
	];

	$cs[] = [
		'class' => ActionColumn::className(),
		'template' => '{update} ',
	];

/*$cs[] = [
  	'label' => 'Cancel',
		'format' => 'raw',
		'value' => function($m)
		{
			$r = '';
			$u = ['cancel', 'id' => $m->id];
      $r .= Html::a("Cancel", $u, ['class' => 'btn btn-danger']);
			return $r;
		}
	];*/


	echo GridView::widget([
		//'summary' => '',
		'dataProvider' => $dp,
		'emptyText' => 'Nothing found',
  	'columns' => $cs,
	]);
