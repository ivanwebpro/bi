<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\datecontrol\DateControl;
use kartik\daterange\DateRangePicker;
use kartik\file\FileInput;
use yii\redactor\widgets\Redactor;

?>

<?php
	$form = ActiveForm::begin();
?>


<div class='row'>
	<div class='col-xs-12'>
		<h1><?=$this->title?></h1>

		<?= $form->errorSummary($model); ?>
		<?php


			echo $form->field($model, 'title');
			echo $form->field($model, 'category_id')->dropDownList($model->ddCategories());
			echo $form->field($model, 'suggested_price');
			//echo $form->field($model, 'margin');

echo $form->field($model, $f = 'image')->widget(FileInput::classname(),
	[
    'options' => [
			'accept' => 'image/*',
      'name' => "Product[$f]",
			//'disabled' => $disabled,
			'data-file-current-url' => $model->imageUrl,
			'multiple' => false,
		],
		'pluginOptions' => [
    	'initialPreview' => $model->imageUrl,
			'initialPreviewAsData' => true,
			'multiple' => false,
		]
	]);


	echo $form->field($model, 'description')
			->widget(Redactor::className(),
			[
      	'clientOptions' => [
					//'plugins' => false,
					'fileUpload' => false,
					'imageUpload' => false,
					//'minHeight' => '300px',

				],
			]);




			//echo $form->field($model, 'vendor_url');
			echo $form->field($model, 'aliexpresss_link');
			echo $form->field($model, 'amazon_link');

			//echo $form->field($model, 'fb_ad_text_example');

	echo $form->field($model, 'ad_copy')
			->widget(Redactor::className(),
			[
      	'clientOptions' => [
					//'plugins' => false,
					'fileUpload' => false,
					'imageUpload' => false,
					//'minHeight' => '300px',

				],
			]);


		echo $form->field($model, 'tutorial_page')
			->widget(Redactor::className(),
			[
      	'clientOptions' => [
					//'plugins' => false,
					'fileUpload' => false,
					'imageUpload' => false,
					//'minHeight' => '300px',
				],
			]);


			echo $form->submitButton("Save");
		?>
	</div>
</div>

<br><br><br>
<?php
	ActiveForm::end();

