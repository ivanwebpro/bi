<?php
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\widgets\ActiveForm;
use common\widgets\SelObj;
use common\widgets\AjaxLoad;
use common\grid\BoolColumn;
use common\grid\DeltaColumn;
use common\grid\DecimalColumn;
use common\helpers\Url;
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii2mod\c3\chart\Chart as C3Chart;
use yii\web\JsExpression;

$enm = $this->context->entity_name_pl;
$this->title = $enm;

?>

<h1><?=$this->title?></h1>


<p>
	<?=Html::a("Add product", ["create"], ['class' => 'btn btn-primary']);?>
</p>


<?php



	$cs = [];

	$cs[] = [
		//'label' => 'Collection',
		'attribute' => 'title',
	];

	$cs[] = [
		'label' => 'Category',
		'attribute' => 'category.title',
	];


	$cs[] = [
		'label' => 'Image',
		'contentOptions' => ['class' => 'col-xs-2'],
		'format' => 'raw',
		'value' => function($m)
		{
    	$u = $m->getImageUrl();
			return Html::img($u, ['class' => 'img-responsive']);
		},
	];

	$cs[] = [
		'label' => 'Suggested price',
		//..'attribute' => '',
		'value' => function($m)
		{
    	return "$m->suggested_price USD";
		}
	];


	$cs[] = [
		'label' => 'Links',

		'format' => 'raw',
		'value' => function($m)
		{
			$r = [];

    	$u = $m->aliexpresss_link;
			if ($u)
			{
      	$r[] = Html::a("Aliexpress", $u, ['target' => '_blank']);
			}

    	$u = $m->amazon_link;
			if ($u)
			{
      	$r[] = Html::a("Amazon", $u, ['target' => '_blank']);
			}

			return implode("<br>", $r);
		},
	];



	$cs[] = [
		'label' => 'Ad copy',
		'contentOptions' => ['class' => 'col-xs-3'],
		'format' => 'raw',
		'attribute' => 'ad_copy',
	];

	/*$cs[] = [
		//'label' => 'Fa',
		'contentOptions' => ['class' => 'col-xs-3'],
		'format' => 'raw',
		'attribute' => 'description',
	];*/

	/*$cs[] = [
		//'label' => 'Fa',
		//'contentOptions' => ['class' => 'col-xs-5'],
		'format' => 'raw',
		'attribute' => 'notes',
	];*/


	$cs[] = [
		'class' => ActionColumn::className(),
		'template' => '{update} {delete}',
	];


	echo GridView::widget([
		'summary' => '',
		'dataProvider' => $dp,
		'emptyText' => 'Nothing found',
  	'columns' => $cs,
	]);
