<?php

namespace admin\models;

use Yii;
use yii\db\Expression;
use yii\db\Query;
use common\helpers\Url;
use common\helpers\Html;
use common\helpers\ArrayHelper;
use common\traits\St;
use mdm\upload\FileModel;
use account\models\LogImport;

class Plan extends \yii\db\ActiveRecord
{
 	use St;

	public static function refreshApiSafe()
	{
  	try
		{
    	self::refreshApi();
		}
		catch(Exception $e)
		{
    	throw $e;
		}
	}

	public static function refreshApi()
	{

  	$a = new \common\apis\RecurlyApi;

		$res = \Recurly_PlanList::get();

		foreach($res as $el)
		{
			if (!empty($_GET['sd']))
			{
      	hrb('el');
				hr($el);
				exit;
			}

			$p = [];
			$p['plan_code'] = $el->plan_code;
			$p['plan_name'] = $el->name;
			$p['plan_description'] = $el->description;
			self::st()->replaceBy($p);
		}

	}


	public static function tableName()
	{
		return '{{%admin_plan}}';
	}


	public function attributeHints()
	{
		$ahs = parent::attributeHints();
		return $ahs;
	}

	public function attributeLabels()
	{
		$als = [];
		$als['title'] = "Title";
		return $als;
	}

	public function rules()
	{
		$rs = [];
		$rs[] = [['app_access'], 'number'];
		$rs[] = [['stores_count'], 'number'];
		$rs[] = [['stores_count_unlimited'], 'number'];
		return $rs;
	}

	public function behaviors()
	{
    $bs = [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
			],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
			'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],
			'ReplaceBy' => [
				'attrs' => ['plan_code'],
				'class' => 'common\behaviors\ReplaceBy',
			],

    ];

		return $bs;

	}


}
