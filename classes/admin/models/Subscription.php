<?php

namespace admin\models;

use Yii;
use yii\db\Expression;
use yii\db\Query;
use common\helpers\Url;
use common\helpers\Html;
use common\helpers\ArrayHelper;
use common\traits\St;
use mdm\upload\FileModel;
use account\models\LogImport;

class Subscription extends \yii\db\ActiveRecord
{
 	use St;

	public static function refreshApiSafe()
	{
  	try
		{
    	self::refreshApi();
		}
		catch(Exception $e)
		{
    	throw $e;
		}
	}

	public static function refreshApi()
	{
    $a = new \common\apis\RecurlyApi;

		$res = \Recurly_SubscriptionList::get();
		//hrb('Recurly_SubscriptionList');
		//hr($res);

		foreach($res as $el)
		{
			$acc = $el->account->get();

			if (!empty($_GET['sd']))
			{
      	hrb('$acc');
				hr($acc);
				exit;


				hrb('$el->plan');
				hr($el->plan);

      	hrb('sub');
				hr($el);

			}

			//hr($el->plan->name);
    	//hr($el->state);
    	//hr($el->account);

    	//hr($acc->email);

			$p = [];
			$p['recurly_id'] = $el->uuid;
			$p['state'] = $el->state;
			$p['plan_name'] = $el->plan->name;
			$p['plan_code'] = $el->plan->plan_code;
			$p['account_email'] = $acc->email;
			$p['account_code'] = $acc->account_code;
			$p['current_period_ends_at'] = $el->current_period_ends_at->format('c');

			if (!empty($_GET['sdr']))
			{
				hr($p);
				exit;
			}

			self::st()->replaceBy($p);
    	//hr("Acc: $el->account");
			//hr($el);
		}

	}


	public static function tableName()
	{
		return '{{%admin_subscription}}';
	}


	public function attributeHints()
	{
		$ahs = parent::attributeHints();
		return $ahs;
	}

	public function attributeLabels()
	{
		$als = [];
		$als['title'] = "Title";
		return $als;
	}

	public function rules()
	{
		$rs = [];
		$rs[] = [['shopify_name'], 'string'];
		return $rs;
	}

	public function behaviors()
	{
    $bs = [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
			],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
			'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],
			'ReplaceBy' => [
				'attrs' => ['recurly_id'],
				'class' => 'common\behaviors\ReplaceBy',
			],

    ];

		return $bs;

	}

  public function getPlan()
  {
  	return $this->hasOne(Plan::className(), ['plan_code' => 'plan_code']);
  }

	public static function getActiveForHost($h)
	{
		$sn = str_ireplace(".myshopify.com", "", $h);
		return self::getActiveForShopifyName($sn);
	}

	public static function getActiveForShopifyName($sn)
	{
		$q = static::find();
	  $q->andWhere(['shopify_name' => $sn]);
		$q = static::addActiveCond($q);
		return $q->one();
	}

	public static function addActiveCond($q)
	{
	  $q->andWhere(['OR',
			['state' => 'active'],
			"current_period_ends_at > NOW()"
			]
		);
		return $q;
	}

	public static function getActiveForEmailAll($email)
	{
		$q = static::find();
	  $q->andWhere(['account_email' => $email]);
		$q = static::addActiveCond($q);
		return $q->all();
	}
}
