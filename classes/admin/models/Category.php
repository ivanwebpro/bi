<?php

namespace admin\models;

use Yii;
use yii\db\Expression;
use yii\db\Query;
use common\helpers\Url;
use common\helpers\Html;
use common\helpers\ArrayHelper;
use common\traits\St;
use mdm\upload\FileModel;
use account\models\LogImport;
use account\models\StoreSuggested;

class Category extends \yii\db\ActiveRecord
{
 	use St;

	public static function tableName()
	{
		return '{{%admin_category}}';
	}

	public function getStoresSuggestedQuery()
	{
    return $this->hasOne(StoreSuggested::className(), ['category_id' => 'id']);
	}



	public function attributeHints()
	{
		$ahs = parent::attributeHints();
		return $ahs;
	}

	public function attributeLabels()
	{
		$als = [];
		$als['title'] = "Title";
		return $als;
	}

	public function rules()
	{
		$rs = [];
		$rs[] = [['title'], 'string'];
		$rs[] = [['title'], 'required'];
		return $rs;
	}

	public function behaviors()
	{
    $bs = [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
			],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
			'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],
    ];

		return $bs;

	}


}
