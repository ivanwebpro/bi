<?php

namespace admin\models;

use Yii;
use yii\db\Expression;
use yii\db\Query;
use common\helpers\Url;
use common\helpers\Html;
use common\helpers\ArrayHelper;
use common\traits\St;
use mdm\upload\FileModel;
use account\models\LogImport;

class Product extends \yii\db\ActiveRecord
{
 	use St;

	public static function tableName()
	{
		return '{{%admin_product}}';
	}

	public function ddCollections()
	{
		$shop = $this->shop;
		$shop->refreshCollections();
		$colls = $shop->getCollections();
		$res = ArrayHelper::map($colls, 'id', 'title');
		return $res;
	}

	public function ddCategories()
	{
		$cats = Category::find()
      ->orderBy('title ASC')
			->all();

		$res = ArrayHelper::map($cats, 'id', 'title');
		return $res;
	}

	public function getCategory()
	{
    return $this->hasOne(Category::className(), ['id' => 'category_id']);
	}

	public function getShop()
  {
  	return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
  }

	public function attributeHints()
	{
		$ahs = parent::attributeHints();
		return $ahs;
	}

	public function attributeLabels()
	{
		$als = [];
		$als['category_id'] = "Category";
		$als['vendor_url'] = "Vendor URL";
		//$als['ad_copy'] = "Facebook Ad text example";
		return $als;
	}

	public function rules()
	{
		$rs = [];
		$rs[] = [['title'], 'string'];
		//$rs[] = [['vendor_url'], 'string'];
		$rs[] = [['aliexpresss_link'], 'string'];
		$rs[] = [['amazon_link'], 'string'];
		$rs[] = [['title'], 'required'];
		$rs[] = [['description'], 'safe'];
		$rs[] = [['suggested_price'], 'number'];
		$rs[] = [['category_id'], 'number'];
		//$rs[] = [['notes'], 'safe'];
		$rs[] = [['ad_copy'], 'safe'];
		$rs[] = [['tutorial_page'], 'safe'];
		//$rs[] = [['margin'], 'number'];

		/*$rs[] = [['date_from'], 'string'];
		$rs[] = [['date_to'], 'string'];
		$rs[] = [['col_order_id', 'col_order_name', 'col_order_created_at', 'col_customer_email', 'col_total', 'col_line_items'], 'number'];
		$rs[] = [['order_number_col'], 'number'];
		$rs[] = [['order_number_col'], 'required'];
		$rs[] = [['order_number_prefix'], 'string'];
		$rs[] = [['order_number_suffix'], 'string'];
		$rs[] = [['order_date_format'], 'string'];
		$rs[] = [['order_total_col'], 'number'];
		$rs[] = [['customer_email_col'], 'number'];
		$rs[] = [['customer_first_name_col'], 'number'];    */
		return $rs;
	}

	public function checkFile()
	{
  	return true;
	}

	public function getStatus()
	{
  	return "Configuring";
	}

	public function behaviors()
	{
    $bs = [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
			],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
			'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],
    ];

		$bs['upl_file'] = [
			'class' => 'mdm\upload\UploadBehavior',
			'attribute' => 'image', // required, use to receive input file
			'savedAttribute' => 'image_file_id', // optional, use to link model with saved file.
			'uploadPath' => '@admin/uploads/product-image', // saved directory. default to '@runtime/upload'
      'autoSave' => true, // when true then uploaded file will be save before ActiveRecord::save()
      'autoDelete' => false, // when true then uploaded file will deleted before ActiveRecord::delete()
		];

		/*$bs['slug_key'] = [
				'attr' => 'slug',
        'class' => 'common\behaviors\Slug',
      ];(*/


		return $bs;

	}

	public function ddRow1Data()
	{
  	$r = [];
		$r['headers'] = 'Headers';
		$r['data'] = 'Data';
		return $r;
	}

	public function ddDateFormats()
	{
  	$r = [];
		$f = "Y-m-d";
		$r[$f] = $f.", ".date($f);
		$f = "d/m/Y";
		$r[$f] = $f.", ".date($f);
		return $r;
	}

	public function ddCols()
	{
  	$fp = $this->filePath;
		$c = file_get_contents($fp);
		//pre($c);

		$lines = explode("\n", $c);
		$h = $lines[0];
		$hs = explode(",", $h);
		$l = $lines[1];
		$data_cols = explode(",", $l);

		foreach($hs as $k=>$v)
		{
    	$hs[$k] .= ", ".$data_cols[$k];
		}
		return $hs;
		//exit;
	}

	public function getImageUrl()
	{
  	return $this->getFileUrl('image_file_id');
	}

	public function getFileUrl($fld)
	{
		//hr($fld);
		$f_id = $this->$fld;
		//hr($f_id);

    $fm = FileModel::findOne($f_id);
    //hre($fm);

		if ($fm && file_exists($fm->filename))
		{
    	return Url::filePathToWebPath($fm->filename);
		}
		return null;
	}

	public function getImagePath()
	{
  	return $this->getUploadedFilePath('image_id');
	}

	public function getUploadedFilePath($fld)
	{
		//hr($fld);
		$f_id = $this->$fld;
		//hr($f_id);

    $fm = FileModel::findOne($f_id);

		return $fm->filename;
	}



  public function shopifyPush($shop)
	{
		$li = new LogImport;
		$li->shop_id = $shop->id;
		$li->product_id = $this->id;
		$li->saveEx();

		$api = $shop->shopifyApi;

		$ps = [];
		$ps['title'] = $this->title;
		$ps['body_html'] = $this->description;

		$ps['variants'] = [];
		$ps['variants'][] = ['price' => $this->price];

    $ps['images'] = [];
		$h = $_SERVER["HTTP_HOST"];
    $ps['images'][] = ['src' => "https://$h".$this->imageUrl];



		$li->request = json_encode($ps, JSON_PRETTY_PRINT);

		try
		{
			$resp = $api->productPost($ps);

			$li->response  = json_encode($resp, JSON_PRETTY_PRINT);
			$li->success = 1;
			$li->completed = 1;
			$li->saveEx();
			$li->handle = $resp['product']['handle'];

			return $li;
		}
		catch(\GuzzleHttp\Exception\ServerException $e)
		{
			$li->response = (string)$e->getResponse()->getBody();
			$li->error = 1;
      $li->error_msg = $e->getMessage();
			$li->saveEx();
			return $li;
		}
 		catch(\GuzzleHttp\Exception\ClientException $e)
		{
			$err_msg = (string)$e->getResponse()->getBody();
			$li->response = (string)$e->getResponse()->getBody();
			$li->error = 1;
      $li->error_msg = $e->getMessage();
			$li->saveEx();
			return $li;
		}


	}

}
