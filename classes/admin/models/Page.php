<?php

namespace admin\models;

use Yii;
use yii\db\Expression;
use yii\db\Query;
use common\helpers\Url;
use common\helpers\Html;
use common\helpers\ArrayHelper;
use common\traits\St;
use mdm\upload\FileModel;
use account\models\LogImport;

class Page extends \yii\db\ActiveRecord
{
 	use St;

	public static function tableName()
	{
		return '{{%admin_page}}';
	}


	public function attributeHints()
	{
		$ahs = parent::attributeHints();
		return $ahs;
	}

	public function attributeLabels()
	{
		$als = [];
		$als['title'] = "Title";
		return $als;
	}

	public function rules()
	{
		$rs = [];
		$rs[] = [['title'], 'string'];
		$rs[] = [['title'], 'required'];

		$rs[] = [['alias'], 'string'];
		$rs[] = [['alias'], 'required'];
		$rs[] = [['content'], 'safe'];


		return $rs;
	}

	public function behaviors()
	{
    $bs = [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
			],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
			'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],
    ];

		return $bs;

	}


}
