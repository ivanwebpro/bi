<?php

namespace admin\models;

use Yii;
use yii\db\Expression;
use yii\db\Query;
use common\helpers\Url;
use common\helpers\Html;
use common\helpers\ArrayHelper;
use common\traits\St;
use mdm\upload\FileModel;
use account\models\LogImport;

class Setting extends \yii\db\ActiveRecord
{
 	use St;

	public static function tableName()
	{
		return '{{%admin_setting}}';
	}


	public function attributeHints()
	{
		$ahs = parent::attributeHints();
		$ahs['purchase_link'] = 'For non-subscribed users, users with cancelled / expired subscription';
		$ahs['exclude_from_billing'] = 'Provide list of Shopify stores like abc.myshopify.com, each store from new line';
		return $ahs;
	}

	public function attributeLabels()
	{
		$als = [];
		$als['exclude_from_billing'] = "Exclude from billing";
		return $als;
	}

	public function rules()
	{
		$rs = [];
		$rs[] = [['purchase_link'], 'string'];
		$rs[] = [['purchase_link'], 'required'];
		$rs[] = [['exclude_from_billing'], 'safe'];


		return $rs;
	}

	public function behaviors()
	{
    $bs = [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
			],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
			'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],
    ];

		return $bs;

	}

	public static function isExcludedFromBilling($h)
	{
		$p = self::findOne(['id' => 1]);
		$efb = $p->exclude_from_billing;
		$hs = explode("\n", $efb);
		$hs = array_map("trim", $hs);

		if (in_array($h, $hs))
		{
    	return true;
		}
		else
		{
    	return false;
		}

	}


}
