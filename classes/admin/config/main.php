<?php

$fs = [
    (__DIR__ . '/../../common/config/params.php'),
    (__DIR__ . '/../../common/config/params-local.php'),
    (__DIR__ . '/params.php'),
    (__DIR__ . '/params-local.php')
];

$params = [];

foreach($fs as $f)
{
	$ff = __DIR__.$f;
	//hr($ff);
	if (file_exists($ff))
	{
		$params = yii\helpers\ArrayHelper::merge($params, require $ff);
	}
}

$app_id = 'bi-admin';

$config = [
    'id' => $app_id,
    'basePath' => appRoot()."/classes/admin",
    'bootstrap' => ['log'],
		'defaultRoute' => 'user/login',
    'controllerNamespace' => 'admin\controllers',
    'components' => [
        'user' => [
        	'identityClass' => 'admin\models\AdminUser',
          'enableAutoLogin' => false,
					'enableSession' => true,
					'loginUrl' => ['user/login'],
					//'acceptableRedirectTypes' => ['*/*']
        ],

    'session' => [
        'name' => $app_id.'_SESS_ID',
        'cookieParams' => [
           // 'path' => $w_path,
        ],
    ],

    'request' => [
			'enableCsrfCookie' => true,
			'cookieValidationKey' => cookieValiationKey(__DIR__),
      'csrfParam' => $app_id.'_CSRF',
      'csrfCookie' => [
          'httpOnly' => true,
          //'path' => $w_path,
				],
		],

    'errorHandler' => [
    	'errorAction' => 'error/message',
    ],
	],
  'params' => $params,
];


return $config;