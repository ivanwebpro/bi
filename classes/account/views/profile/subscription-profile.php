<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\widgets\ListView;

$this->title = "Manage subscription";

?>

<h1><?=$this->title?></h1>

<?php
/*
<h2>Upgrade</h2>

<?php
	if ($shop->isPlanUnlimited())
	{
		echo "<p>You are already using unlimited plan</p>";
		//return;
	}
	else
	{
		echo "<p>";
  		echo Html::a("Upgrade", ['plan-upgrade'], ['class' => 'btn btn-primary']);
		echo "</p>";
	}
?>
*/
?>

<h2>Upgrade plan</h2>

<?php
if ($plans_msg)
{
 	echo "<div class=well>";
		echo $plans_msg;
 	echo "</div>";
}
else
{
	$cfg = [];
	$cfg['itemView'] = '_plan-profile';
	$cfg['itemOptions'] = ['class' => 'col-sm-12'];
	$cfg['options'] = ['class' => ''];
	$cfg['dataProvider'] = $plans_dp;
	$cfg['layout'] = '{items}';
	$cfg['viewParams'] = ['current_plan_code' => $current_plan_code];
	echo ListView::widget($cfg);
}

?>

<h2>Update credit card</h2>

<?php
if ($upd_card_msg)
{
 	echo "<div class=well>";
		echo $upd_card_msg;
 	echo "</div>";
}

if ($upd_card_url)
{
	echo "<p>";
		echo Html::a("Update card", $upd_card_url, ['class' => 'btn btn-primary', 'target' => '_blank']);
	echo "</p>";
}
?>
<h2>Latest Invoices</h2>

<?php

if ($invs_msg)
{
 	echo "<div class=well>";
		echo $invs_msg;
 	echo "</div>";
}
else
{
	$cs = [];

	$cs[] = [
		'label' => 'Created',
		'attribute' => 'created_at',
		'format' => 'raw',
		'value' => function($m) use ($shop)
		{
			return $shop->fTime($m['created_at']);
		}
	];

	$cs[] = [
		'label' => 'Number',
		'attribute' => 'invoice_number',
	];

	$cs[] = [
		'label' => 'Total',
		'attribute' => 'total_fmt',
	];

	$cs[] = 'state';

	echo GridView::widget([
		//'summary' => '',
		'dataProvider' => $invs_dp,
		'emptyText' => 'Nothing found',
  	'columns' => $cs,
	]);
}

?>

<br><br><br>

<p><i>Want to cancel? Email us at <a href='mailto: getmeout@whossellingwhat.com'>getmeout@whossellingwhat.com</a></i></p>
<?php

