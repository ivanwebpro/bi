<?php
	use common\helpers\Html;
	//use yii\bootstrap\Modal;
	use common\widgets\Modal;
	use common\widgets\ModalAjax;
  use common\helpers\Url;
  use common\helpers\DatesHelper;

	use common\widgets\ActiveForm;

	// $model, $key, $index
?>

<style>

</style>

<?php
	$cl = 'well';
	$cl = '';

	$id = "plan-$model->id";
?>
	<div class='<?=$cl?>' id = '<?=$id?>'>
		<?php

	$form = ActiveForm::begin();

	if ($model->stores_count_unlimited)
	{
		$stores_text = "unlimited amount of stores";
	}
	else
	{
    $stores_text = "up to $model->stores_count stores";
	}
?>


<div class='row'>
	<div class='col-sm-12'>




		<?php
			// <h1><?=$model->plan_name - <?=$stores_text</h1>
    	//<p><i><?=$model->plan_description</i></p>

			$f = 'plan_code';
			$opts = [];
			$opts['template'] = "{items}";
			$opts['name'] = "PlanChange[$f]";
			$opts['value'] = $model->plan_code;
      $lbl = "$model->plan_name - $stores_text";

			if ($current_plan_code == $opts['value'])
			{
      	//echo "<div class=well>";
					$lx = "Current plan: $lbl";
					echo Html::a($lx, '#', ['class' => 'btn btn-default']);
      	//echo "</div>";
			}
			else
			{
				echo Html::activeHiddenInput($model, $f, $opts);
				echo $form->submitButton($lbl, ['class' => 'btn btn-success']);
			}
			//echo $form->submitButton("Change");

		?>
		<br><br>
	</div>
</div>

<?php
	ActiveForm::end();



		?>
	</div>

<?php


