<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

$this->title = "Shopify integration";

?>

<h1><?=$this->title?></h1>

<?php
//  col-sm-6 col-sm-offset-3 install-form
	$form = ActiveForm::begin();
?>

<div class='alert alert-danger'>
	<p>Once you will click <b>Save</b> button - you will be logged out immediately and redirected to login page. In case of mistakes in the name of new Shopify store - you can't reverse changes. It can be done only by request to support.</p>
</div>

<div class='row'>
	<div class='col-xs-12'>

		<?php
			echo $form->errorSummary($model);
			echo $form->field($model, 'new_shopify_name', ['template' => "{label}<span class='colon'>:</span>\n<div class='input-group'>{input}<span class='input-group-addon'>.myshopify.com</span></div>\n{hint}\n{error}"]);
    ?>

		<div class="form-group">
			<?php
				$opts = [];
				$opts['class'] = 'btn btn-primary';
				$opts['data-confirm'] = "Are you sure want to switch Shopify store?";
				echo Html::submitButton('Switch', $opts);
			?>
		</div>

	</div>
</div>

<?php
	ActiveForm::end();

