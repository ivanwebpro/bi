<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\widgets\ListView;

$this->title = "Upgrade plan";



?>

<h1><?=$this->title?></h1>

<?php

if ($shop->isPlanUnlimited())
{
	echo "<p>You are already using unlimited plan</p>";
	return;
}

// plans

$cfg = [];
$cfg['itemView'] = '_plan-profile';
$cfg['itemOptions'] = ['class' => 'col-sm-12'];
$cfg['options'] = ['class' => ''];
$cfg['dataProvider'] = $dp;
$cfg['layout'] = '{items}';


echo ListView::widget($cfg);
