<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

$this->title = "Support";

?>

<h1><?=$this->title?></h1>

<h2>FAQ</h2>

<?php
	$p = \admin\models\Page::findOne(['alias' => 'faq']);
	if ($p)
	{
  	echo $p->content;
	}
	else
	{
  	echo "<div class=well>";
			echo "FAQ content can't be loaded";
  	echo "</div>";
	}
?>


<h2>Contact us</h2>

<?php

	$form = ActiveForm::begin();
	echo $form->errorSummary($contact_model);
	echo $form->field($contact_model, 'name');
	echo $form->field($contact_model, 'store_url');
	echo $form->field($contact_model, 'email');
	echo $form->field($contact_model, 'inquiry')->textArea(['rows' => 5]);
	echo $form->submitButton("Send");
	ActiveForm::end();

	/*
?>

 <br><br><br>
<h2>Cancel</h2>

<?php

if ($shop->subscription && ('active' == $shop->subscription->state))
{
	$form = ActiveForm::begin();
	echo $form->errorSummary($cancel_model);
	echo $form->field($cancel_model, 'run', ['template' => '{input}'])->hiddenInput(['value' =>1]);

	$opts = [];
	$opts['class'] = 'btn btn-danger';
	$opts['data-confirm'] = "Are you sure want to cancel your subscription?";

	echo $form->submitButton("Cancel", $opts);
	ActiveForm::end();
}
else
{
	echo "<p>";
		echo "You don't have active subscription";
	echo "</p>";
}


*/