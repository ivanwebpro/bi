<?php

use yii\helpers\Html;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;
use account\models\AppCharging;
$app_name = Yii::$app->name;
$this->title = $app_name.": Add to Your Shopify Store";
$ac = AppCharging::findForApp();
?>

<?php
	if (!empty($ac->discount_notice))
	{
		echo "<div style='border: 2px dashed red; padding: 5px'>";
  	echo "<h1 class='text-center' style='background-color: darkgreen; color: white; padding: 5px; border-radius: 4px; margin: 0px;'>";
		echo $ac->discount_notice;
  	echo "</h1>";
		echo "</div>";
	}
?>

<h1 class='text-center'><?=$this->title?></h1>

<h2 class='text-center'><?=$ac->trial_days?> days FREE trial, after &ndash;

<?php
	if (!empty($ac->compare_price))
	{
  	echo "<span style='text-decoration: line-through red;'>";
		echo $ac->compare_price;
		echo "</span>";
	}
?>

<?=$ac->monthly_price?> USD per month</h2>

<ul class='lead'>
	<li>Daily motivation to perform better and better</li>
	<li>Quickly notice reducing of orders count / average order amount</li>
	<li>See orders with biggest amount and keep more attention on it</li>
	<li>Check visitor stats from Google Analytics</li>
	<li>Unlimited email recipients (as many team member can receive reports as you want)</li>
</ul>


<?php
if (!empty($error))
{
 	echo Alert::widget([
 				'body' => $error,
       	'closeButton' => false,
       	'options' => ['class' => 'alert-danger'],
       ]);
}
?>

<?php
if (!empty($msg))
{
 	echo Alert::widget([
 				'body' => $msg,
       	'closeButton' => false,
       	'options' => ['class' => 'alert-danger'],
       ]);
}
?>


<?php include '_app_connect_form.php'; ?>


<?php


