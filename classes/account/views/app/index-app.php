<?php

use yii\helpers\Html;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;

$app_name = Yii::$app->name;
$this->title = $app_name.": Add to Your Shopify Store";
$this->menu_display = false;
//<h1><?=$this->title</h1>

?>



<?php
if (!empty($error))
{
 	echo Alert::widget([
 				'body' => $error,
       	'closeButton' => false,
       	'options' => ['class' => 'alert-danger'],
       ]);
}
?>

<?php
if (!empty($msg))
{
 	echo Alert::widget([
 				'body' => $msg,
       	'closeButton' => false,
       	'options' => ['class' => 'alert-danger'],
       ]);
}
?>

<?php include '_app_connect_form.php'; ?>

<?php


