<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
?>

<style>
.field-shopifyauthform-shop_name .help-block
{
	display: none;
}

</style>


<?php
$form = ActiveForm::begin();
$app_name = Yii::$app->name;
?>



<div class='row'>
	<div class='col-xs-12 col-sm-6 col-sm-offset-3 install-form'>

		<h2  class=text-center>Connect <?=$app_name?></h2>
		<?php

		?>
		<?= $form->errorSummary($model); ?>
		<?=$form->field($model, 'shop_name', ['template' => "{label}<span class='colon'>:</span>\n<div class='input-group'>{input}<span class='input-group-addon'>.myshopify.com</span></div>\n{hint}\n{error}"]);?>

<div class="form-group text-center">
	<?= Html::submitButton('Connect', ['class' => 'btn btn-primary']) ?>
</div>

	</div>
</div>



<?php
	ActiveForm::end();

?>


<?php
