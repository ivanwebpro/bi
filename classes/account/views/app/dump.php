<?php

if (empty($title))
	$title = "Debug Page";
	
$this->title = $title;

if (is_scalar($model))
	hr($model);
else
	var_dump($model);