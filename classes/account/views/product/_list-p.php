<?php
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\widgets\ActiveForm;
use common\widgets\SelObj;
use common\widgets\AjaxLoad;
use common\grid\BoolColumn;
use common\grid\DeltaColumn;
use common\grid\DecimalColumn;
use common\helpers\Url;
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii2mod\c3\chart\Chart as C3Chart;
use yii\web\JsExpression;
use yii\widgets\ListView;
use common\widgets\SelList;
use kop\y2sp\ScrollPager;


?>


<style>
.row.display-flex {
  display: flex;
  flex-wrap: wrap;
}
.row.display-flex > [class*='col-'] {
  display: flex;
  flex-direction: column;
}
</style>

<style>


.product-pushed h4,
.product-pushed p,
.product-pushed a
{
	opacity: 0.7 !important;
	/*color: gray !important;*/
}

.product-pushed .btn-add
{
	display: none;
}

.product-non-pushed .btn-added
{
	display: none;
}

.product-pushed.product-non-pushed  .btn-added
{
	display: block !important;
}

</style>

<?php
	// if (empty($products_index_saved)

if (!empty($stores))
{
	$p = [];
	//$p['title'] = 'Stores';
	$p['active_id'] = $store_id;
	$p['els'] = $stores;
	$p['url_fld'] = 'store_id';
	echo SelList::widget($p);
}


$cfg = [];
$cfg['dataProvider'] = $dp;

/*
<div class='row'>
	<div class='col-xs-12'>
		{summary}
  </div>
</div>

$cf

<div class='row'>
	<div class='col-xs-12'>
		{pager}
  </div>
</div>
*/

$cf = '<div class=clearfix></div>';
$cfg['layout'] = "
<div class=list-view>




<div class='row display-flex'>
	{items}
</div>

$cf

<div class='row'>
	<div class='col-xs-12'>
		{pager}
  </div>
</div>


</div>

	";

$fp_class = 'fp-item';

$cfg['itemView'] = '_product-p';
$cfg['itemOptions'] = ['class' => 'col-xs-4 col-sm-3 '.$fp_class];
$cfg['options'] = ['class' => ''];

$cfg['id'] = 'fp';

/*$cfg['pager'] = [
	'class' => ScrollPager::className(),
  'container' => '.list-view',
  'item' => '.'.$fp_class,
	'enabledExtensions'=> [
		ScrollPager::EXTENSION_TRIGGER,
		ScrollPager::EXTENSION_SPINNER,
		ScrollPager::EXTENSION_NONE_LEFT,
		ScrollPager::EXTENSION_PAGING
	],
	];*/


if (empty($_GET['d_pg']))
{
	$cfg['pager'] = [
		'class' => 'mranger\load_more_pager\LoadMorePager',
		'id' => $cfg['id'].'-pagination',
		'contentSelector' => '.list-view', //'#'.$cfg['id'],
	  'contentItemSelector' => '.'.$fp_class,
		'buttonText' => 'Load more',
		'template' => '<p class=text-center>{button}</p>',
		'loaderTemplate' => '<div class="modal-ajax-loader"></div>',
		'options' => ['class' => 'btn btn-lg btn-warning'],
	];
}


echo ListView::widget($cfg);

