<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

$ea = $this->context->entity_alias;
$en = $this->context->entity_name;
$this->title = "$product->title from ".($product->store->domain);

?>

<h1><?=$this->title?></h1>

<?php
/*
if (isDebug())
{
?>

<style>
	td {
		border: 2px solid red;
	}
</style>
<?php
}
*/


echo $product->summaryClean;