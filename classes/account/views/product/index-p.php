<?php
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\widgets\ActiveForm;
use common\widgets\SelObj;
use common\widgets\AjaxLoad;
use common\grid\BoolColumn;
use common\grid\DeltaColumn;
use common\grid\DecimalColumn;
use common\helpers\Url;
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii2mod\c3\chart\Chart as C3Chart;
use yii\web\JsExpression;
use yii\widgets\ListView;
use common\widgets\SelList;
use common\widgets\AjaxToggle;
use kop\y2sp\ScrollPager;

$enm = $this->context->entity_name_pl;
$this->title = $enm;

/*if ('saved' == Yii::$app->request->get('store_id', 'all'))
{
	$this->title = "Saved products";
} */

echo AjaxToggle::widget();

?>


<h1><?=$this->title?></h1>


<?php
  $q = $shop->getStoresQuery();
	if (!$q->exists())
	{
  	?>
			<div class='alert alert-success'>
     		<p>There is no stores in your account yet.</p>
     		<p>You can type some URL in field below or add some store from our Suggested stores:</p>
				<p>
        	<?php
          	echo Html::a("Suggested stores", ['store-suggested/index'], ['class' => 'btn btn-success']);
					?>
				</p>
			</div>

		<?php

	}
?>

<div class='row'>
<div class='col-xs-12'>
<?php

if ($shop->isPlanUnlimited())
{
	$limit_msg = "(unlimited available)";
}
else
{
	$sub = $shop->subscription;
	$limit_max = $sub->plan->stores_count;
	$current_stores_count = $shop->getStoresQuery()->count();
	$limit_msg = "($current_stores_count / $limit_max used)";
}

$form = ActiveForm::begin();
echo $form->errorSummary($store_add_form);

$inline_btn = "<span class='input-group-btn'>
        <button type='submit' class='btn btn-primary'>Add store $limit_msg</button>
      </span>";

$tpl = "{label}<span class='colon'>:</span>\n
	<div class='input-group'>\n{input}\n$inline_btn \n</div>\n{hint}"; //\n{error}

echo $form->field($store_add_form, 'url', ['template'=> $tpl]);

ActiveForm::end();

?>
</div>
</div>

<style>
	.btn-danger
	{
		background-color: #ff7d1e !important;
		border-color: #ff7d1e !important;
	}
</style>


<?php


	include '_list-p.php';




