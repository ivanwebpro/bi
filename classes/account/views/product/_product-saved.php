<?php
	use common\helpers\Html;
	//use yii\bootstrap\Modal;
	use common\widgets\Modal;
	use common\widgets\ModalAjax;
  use common\helpers\Url;
  use common\helpers\DatesHelper;
	// $model, $key, $index
?>

	<div class=well>
		<?php
			echo "<h4 class=text-center>".$model->title."</h4>";

			echo "<div style='height: 100px;'>";
				//echo Html::img($model->imageUrl, ['class' => 'img-responsive', 'style' => 'max-height: 100px; margin: 0 auto']);
				echo Html::img($model->scaledImageUrl, ['class' => 'img-responsive', 'style' => 'max-height: 100px; margin: 0 auto']);
			echo "</div>";

			if ($model->price_min || $model->price_max)
			{
				echo "<p class=text-center>";
					if ($model->price_min != $model->price_max)
					{
	        	echo "$model->price_min ... $model->price_max ";
					}
					else
					{
	          echo "$model->price_min";
					}
					echo " $model->currency";

				echo "</p>";
			}

			echo "<p class=text-center>";
				$lbl = '<span class="glyphicon glyphicon-new-window"></span>';
				echo Html::a($lbl, $model->urlNoRef, ['target' => '_blank']);
			echo "</p>";



			echo "<p class=text-center>";
				//$d = date('d-M-Y, D', strtotime($model->published_time_utc));
				$d = DatesHelper::deltaText($model->published_time_utc);
				echo $d;
			echo "</p>";

			echo "<p class=text-center><b>";
				echo $model->store->domain;
			echo "</b></p>";


			/*echo "<p class=text-center>";
				echo Html::a("Add to Shopify", ['shopify-push', 'id' => $model->id], ['class' => 'btn btn-primary']);
			echo "</p>";
      */
			//echo "<p class=text-center>";
			//				echo Html::a("Description", ['#', 'id' => $model->id], ['class' => 'btn btn-success']);
			// echo "</p>";

			/*Modal::begin([
    		'header' => "<h2>$model->title</h2>",
    		'toggleButton' => ['label' => 'Description', 'class' => 'btn btn-success'],
        'toggle_button_wrapper' => ['tag' => 'p', 'class' => 'text-center'],
			]);

				echo $model->summary;

			Modal::end();*/

echo ModalAjax::widget([
    'id' => "description-product-store-$model->id",
    'header' => "Description",
		//'mode' => ModalAjax::MODE_MULTI,
		'selector' => '.description-product-store',
    'toggleButton' => [
				'tag' => 'a',
        'label' => 'Description',
        'class' => 'btn btn-success description-product-store',
				'href' => Url::to(['description', 'id' => $model->id, 'ajax' => 1]),
    ],
		'toggle_button_wrapper' => ['tag' => 'p', 'class' => 'text-center'],
    'url' => Url::to(['description', 'id' => $model->id, 'ajax' => 1]), // Ajax view with form to load
    //'ajaxSubmit' => true, // Submit the contained form as ajax, true by default
]);



echo ModalAjax::widget([
    'id' => "add-to-shopify-product-store-$model->id",
    'header' => "Add to Shopify: result",
		'selector' => '.shopify-push-product-store',
    'toggleButton' => [
        'label' => 'Add to Shopify',
        'class' => 'btn btn-primary shopify-push-product-store',
				'href' => Url::to(['shopify-push', 'id' => $model->id, 'ajax' => 1]),
    ],
		'toggle_button_wrapper' => ['tag' => 'p', 'class' => 'text-center'],
    'url' => Url::to(['shopify-push', 'id' => $model->id, 'ajax' => 1]), // Ajax view with form to load
    //'ajaxSubmit' => true, // Submit the contained form as ajax, true by default
]);

		?>
	</div>

<?php


