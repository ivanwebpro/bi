<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

$ea = $this->context->entity_alias;
$en = $this->context->entity_name;
$this->title = "Product `$product->title` has been included in saved products";

?>

<p><?=$this->title?></p>

<?php

$opts = [
	'class' => 'btn btn-lg btn-primary',
	'data-dismiss' => "modal",
	'aria-hidden'=> "true",
];

echo "<br><br><br><p class=text-center>";
echo Html::a("OK", ['index'], $opts);
echo "</p>";
