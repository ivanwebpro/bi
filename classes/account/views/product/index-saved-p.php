<?php
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\widgets\ActiveForm;
use common\widgets\SelObj;
use common\widgets\AjaxLoad;
use common\grid\BoolColumn;
use common\grid\DeltaColumn;
use common\grid\DecimalColumn;
use common\helpers\Url;
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii2mod\c3\chart\Chart as C3Chart;
use yii\web\JsExpression;
use yii\widgets\ListView;
use common\widgets\SelList;
use kop\y2sp\ScrollPager;

$this->title = "Saved products";

?>

<h1><?=$this->title?></h1>

<?php
	$products_index_saved = true;
	include '_list-p.php';
