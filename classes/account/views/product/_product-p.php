<?php
	use common\helpers\Html;
	//use yii\bootstrap\Modal;
	use common\widgets\Modal;
	use common\widgets\ModalAjax;
  use common\helpers\Url;
  use common\helpers\DatesHelper;
	// $model, $key, $index
?>

<?php
	$cl = 'well';

	if ($model->pushed)
	{
  	$cl .= " product-pushed";
	}
	else
	{
    $cl .= " product-non-pushed";
	}

	$id = "product-$model->id";
?>
	<div class='<?=$cl?>' id = '<?=$id?>'>
		<?php


		//$save_opts = save-product btn btn-'.($model->saved ? 'warning' : 'default'),
		echo "<div style='float: left' data-ajax-toggle-wrapper>";
			echo $model->saveBtn();
		echo "</div>";



$modal_opts = [
    'id' => "add-to-shopify-product-store-$model->id",
    'header' => "Add to Shopify: result",
		'selector' => '.shopify-push-product-store',
    'toggleButton' => [
        'label' => 'Add',
        'class' => 'btn btn-primary shopify-push-product-store btn-add',
				'href' => Url::to(['shopify-push', 'id' => $model->id, 'ajax' => 1]),
    ],
    'toggleButton2' => [
        'label' => 'Added',
        'class' => 'btn btn-default shopify-push-product-store btn-added',
				'href' => Url::to(['shopify-push', 'id' => $model->id, 'ajax' => 1]),
    ],

		//'toggle_button_wrapper' => ['tag' => 'p', 'class' => 'text-center'],
		'toggle_button_wrapper' => ['tag' => 'div', 'style' => 'float: right'],
    'url' => Url::to(['shopify-push', 'id' => $model->id, 'ajax' => 1]), // Ajax view with form to load
    'ajaxSubmit' => true, // Submit the contained form as ajax, true by default
		'pjaxContainer' => "pjax-$model->id"
];


echo ModalAjax::widget($modal_opts);


			echo "<div class=clearfix></div>";
      echo "<br><br>";


			echo "<div style='height: 100px;'>";
				//echo Html::img($model->imageUrl, ['class' => 'img-responsive', 'style' => 'max-height: 100px; margin: 0 auto']);
				echo Html::img($model->scaledImageUrl, ['class' => 'img-responsive', 'style' => 'max-height: 100px; margin: 0 auto']);
			echo "</div>";

			echo "<h4 class=text-center>".$model->title."</h4>";

			if ($model->price_min || $model->price_max)
			{
				echo "<p class=text-center>";
					if ($model->price_min != $model->price_max)
					{
	        	echo "$model->price_min ... $model->price_max ";
					}
					else
					{
	          echo "$model->price_min";
					}
					echo " $model->currency";

				echo "</p>";
			}

			echo "<p class=text-center>";
				$lbl = '<span class="glyphicon glyphicon-new-window"></span>';
				echo Html::a($lbl, $model->urlNoRef, ['target' => '_blank']);
			echo "</p>";


			//
			echo "<p class=text-center>";
				$lbl = 'Aliexpress';

				// https://www.aliexpress.com/wholesale?catId=0&initiative_id=&SearchText=cd
				// 		"https://www.aliexpress.com/w/wholesale-abc.html?spm=2114.search0104.0.0.6a7e563eElWOhA&site=glo&groupsort=1&SortType=total_tranpro_desc&g=y&SearchText=abc
				$u = "https://www.aliexpress.com/wholesale?catId=0&groupsort=1&SortType=total_tranpro_desc&site=glo&initiative_id=&SearchText=".urlencode($model->title);
				echo Html::a($lbl, $u, ['target' => '_blank', 'class' => 'btn btn-info']);
			echo "</p>";


			echo "<p class=text-center>";
				//$d = date('d-M-Y, D', strtotime($model->published_time_utc));
				$d = DatesHelper::deltaText($model->published_time_utc);
				echo $d;
			echo "</p>";

			if ($model->store)
			{
				echo "<p class=text-center><b>";
					echo $model->store->domain;
				echo "</b></p>";
			}
			else if ($model->store_domain)
			{
				echo "<p class=text-center><b>";
					echo $model->store_domain;
				echo "</b></p>";
			}


			/*echo "<p class=text-center>";
				echo Html::a("Add to Shopify", ['shopify-push', 'id' => $model->id], ['class' => 'btn btn-primary']);
			echo "</p>";
      */
			//echo "<p class=text-center>";
			//				echo Html::a("Description", ['#', 'id' => $model->id], ['class' => 'btn btn-success']);
			// echo "</p>";

			/*Modal::begin([
    		'header' => "<h2>$model->title</h2>",
    		'toggleButton' => ['label' => 'Description', 'class' => 'btn btn-success'],
        'toggle_button_wrapper' => ['tag' => 'p', 'class' => 'text-center'],
			]);

				echo $model->summary;

			Modal::end();*/

echo ModalAjax::widget([
    'id' => "description-product-store-$model->id",
    'header' => "Description",
		//'mode' => ModalAjax::MODE_MULTI,
		'selector' => '.description-product-store',
    'toggleButton' => [
				'tag' => 'a',
        'label' => 'Description',
        'class' => 'btn btn-success description-product-store',
				'href' => Url::to(['description', 'id' => $model->id, 'ajax' => 1]),
    ],
		'toggle_button_wrapper' => ['tag' => 'p', 'class' => 'text-center'],
    'url' => Url::to(['description', 'id' => $model->id, 'ajax' => 1]), // Ajax view with form to load
    //'ajaxSubmit' => true, // Submit the contained form as ajax, true by default
]);





		?>
	</div>

<?php


