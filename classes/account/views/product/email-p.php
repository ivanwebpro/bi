<?php
	use common\helpers\Url;
	use common\helpers\Html;

?>

<p>
	New product `<b><?=$product->title?></b>` was published on <b><?=$product->store_domain?></b>:
</p>

<p>
	<b>Link:</b>
	<?php
		$lbl = $product->url;
		echo Html::a($lbl, $product->urlNoRef, ['target' => '_blank']);
	?>
</p>


<p>
	<b>Look in app:</b>
	<?php
		$u = appRootWeb()."/product/view?id=$product->id";
		echo Html::a($u, $u);
	?>
</p>


<p>
	<b>Who's Selling What App</b><br>
	Change email notifications settings:
	<?php
		$u = appRootWeb()."/shop/update";
		echo Html::a($u, $u);
	?>
</p>

<?php

