<?php
use yii\helpers\Html;
use common\helpers\Url;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\widgets\Pjax;
	use common\widgets\ModalAjax;


$ea = $this->context->entity_alias;
$en = $this->context->entity_name;
$this->title = "Adding product `$product->title`";// (#$model->id)";

?>

<h1><?=$this->title?></h1>


<?php
	//echo "<div class='x-pjax-`$product->id'>";



	if ($show_push_again_form)
	{
		echo "<p>";
			echo "This product had been added, do you want to add it again?";
		echo "</p>";

$modal_opts = [
    'id' => "x-add-to-shopify-product-store-$product->id",
    'header' => "Add to Shopify: result",
		'selector' => '.x-shopify-push-product-store',
    'toggleButton' => [
        'label' => 'Add again',
        'class' => 'btn btn-danger x-shopify-push-product-store',
				'href' => Url::to(['shopify-push', 'id' => $product->id, 'ajax' => 1, 'force' => 1]),
			'data-dismiss' => "modal",
			'aria-hidden'=> "true",

    ],

		//'toggle_button_wrapper' => ['tag' => 'p', 'class' => 'text-center'],
		'toggle_button_wrapper' => ['tag' => 'div'],
    'url' => Url::to(['shopify-push', 'id' => $product->id, 'ajax' => 1, 'force' => 1]), // Ajax view with form to load
    //'ajaxSubmit' => true, // Submit the contained form as ajax, true by default
		//'pjaxContainer' => "pjax-$model->id"
];


echo ModalAjax::widget($modal_opts);
return;


Pjax::begin([
'id'=>"pjax-$product->id",
'timeout' => 5000,
'enableReplaceState'=>false,
'enablePushState'=>false,
//'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
]);




		$form = ActiveForm::begin();
		echo $form->errorSummary($push_again_form_model);
		echo $form->field($push_again_form_model, 'run', ['template' => '{input}'])->hiddenInput(['value' =>1]);

		$opts = [];
		$opts['class'] = 'btn btn-danger';
		//$opts['data-confirm'] = "Are you sure want to cancel your subscription?";
		echo $form->submitButton("Add again", $opts);
		ActiveForm::end();

Pjax::end();
		return;

	}

	if ($err_msg)
	{
  	echo "<div class='alert alert-danger'>";
		echo $err_msg;
  	echo "</div>";

		if (!$ajax)
		{
			echo "<br><br><br><p class=text-center>";
			echo Html::a("Try again", '', ['class' => 'btn btn-lg btn-primary']);
			echo "</p>";
		}

	}
	else
	{
  	echo "<div class='alert alert-success'>";
			echo $msg;
  	echo "</div>";

		$opts = [
			'class' => 'btn btn-lg btn-primary',
			'data-dismiss' => "modal",
			'aria-hidden'=> "true",
		];

		echo "<br><p class=text-center>";
		echo Html::a("Add more products", ['index'], $opts);
		echo "</p>";

		$model = $product;
		$id = "product-$model->id";

		echo "
<script>
	var element, name, arr;
  element = document.getElementById('$id');
  name = 'product-pushed';
  arr = element.className.split(' ');
  if (arr.indexOf(name) == -1)
	{
  	element.className += ' ' + name;
  }
</script>
";

	}



//echo "</div>";