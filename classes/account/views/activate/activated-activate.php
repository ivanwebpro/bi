<?php

use yii\helpers\Html;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;

$app_name = Yii::$app->name;
$this->title = $app_name.": Subscription has been activated";
$this->menu_display = false;
//<h1><?=$this->title</h1>

?>



<div class='row'>
	<div class='col-xs-12 col-sm-6 col-sm-offset-3 install-form'>

		<h2 class=text-center><?=$this->title?></h2>
		<p class=text-center>

		<?php
			echo Html::a("Install app to Shopify store", $url, ['class' => 'btn btn-lg btn-primary']);
		?>
		</p>

	</div>
</div>


<?php



