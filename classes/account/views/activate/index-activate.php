<?php

use yii\helpers\Html;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;

$app_name = Yii::$app->name;
$this->title = $app_name.": Activate your subscription";
$this->menu_display = false;
//<h1><?=$this->title</h1>

?>



<?php
if (!empty($error))
{
 	echo Alert::widget([
 				'body' => $error,
       	'closeButton' => false,
       	'options' => ['class' => 'alert-danger'],
       ]);
}
?>



<?php
$form = ActiveForm::begin();
$app_name = Yii::$app->name;
?>



<div class='row'>
	<div class='col-xs-12 col-sm-6 col-sm-offset-3 install-form'>

		<h2 class=text-center><?=$app_name?><br>Activate subscription</h2>
		<?php

		?>

<?php
if (!empty($msg))
{
 	echo Alert::widget([
 				'body' => $msg,
       	'closeButton' => false,
       	'options' => ['class' => 'alert-danger'],
       ]);
}
?>
		
		<?= $form->errorSummary($model); ?>
		<?=$form->field($model, 'shopify_name', ['template' => "{label}<span class='colon'>:</span>\n<div class='input-group'>{input}<span class='input-group-addon'>.myshopify.com</span></div>\n{hint}\n{error}"]);?>
		<?=$form->field($model, 'email');?>

<div class="form-group text-center">
	<?= Html::submitButton('Activate', ['class' => 'btn btn-primary']) ?>
</div>

	</div>
</div>



<?php
	ActiveForm::end();

?>


<?php



