<?php
	use common\helpers\Url;
	use common\helpers\Html;

?>

<p>
	<b>Email:</b> <?=$c->email?>
</p>

<p>
	<?php
		$u = $c->store_url;
		echo Html::a($u, $u);
	?>
</p>

<p>
	<b>Name:</b> <?=$c->name?>
</p>

<p>
	<b>Inquire:</b><br>
	<?php
		echo str_replace("\n", "<br>", $c->inquiry);
	?>
</p>

<?php

