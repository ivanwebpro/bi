<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\datecontrol\DateControl;
use kartik\daterange\DateRangePicker;
use kartik\file\FileInput;
use yii\redactor\widgets\Redactor;

?>

<?php
	$form = ActiveForm::begin();
?>


<div class='row'>
	<div class='col-xs-12'>
		<h1><?=$this->title?></h1>

		<?= $form->errorSummary($model); ?>
		<?php


			echo $form->field($model, 'title');
			echo $form->field($model, 'price');
			echo $form->field($model, 'margin');

echo $form->field($model, $f = 'image')->widget(FileInput::classname(),
	[
    'options' => [
			'accept' => 'image/*',
      'name' => "Product[$f]",
			//'disabled' => $disabled,
			'data-file-current-url' => $model->imageUrl
		],
		'pluginOptions' => [
    	'initialPreview' => $model->imageUrl,
			'initialPreviewAsData' => true,
		]
	]);


	echo $form->field($model, 'body_html')
			->widget(Redactor::className(),
			[
      	'clientOptions' => [
					//'plugins' => false,
					'fileUpload' => false,
					'imageUpload' => false,
					//'minHeight' => '300px',
				],
			]);
			?>



			echo $form->field($model, 'description');

			echo $form->field($model, 'fb_ad_text_example');
      echo $form->field($model, 'notes');


			echo $form->submitButton("Save");
		?>
	</div>
</div>
<?php
	ActiveForm::end();

