<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

$ea = $this->context->entity_alias;
$en = $this->context->entity_name;
$this->title = "Adding product `$product->title`";// (#$model->id)";

?>

<h1><?=$this->title?></h1>

<?php
	if ($err_msg)
	{
  	echo "<div class='alert alert-danger'>";
		echo $err_msg;
  	echo "</div>";

		echo "<br><br><br><p class=text-center>";
		echo Html::a("Try again", '', ['class' => 'btn btn-lg btn-primary']);
		echo "</p>";

	}
	else
	{
  	echo "<div class='alert alert-success'>";
			echo $msg;
  	echo "</div>";

		echo "<br><br><br><p class=text-center>";
		echo Html::a("Add more products", ['index'], ['class' => 'btn btn-lg btn-primary']);
		echo "</p>";
	}
	


