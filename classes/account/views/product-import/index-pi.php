<?php
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\widgets\ActiveForm;
use common\widgets\SelObj;
use common\widgets\AjaxLoad;
use common\grid\BoolColumn;
use common\grid\DeltaColumn;
use common\grid\DecimalColumn;
use common\helpers\Url;
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii2mod\c3\chart\Chart as C3Chart;
use yii\web\JsExpression;

$enm = $this->context->entity_name_pl;
$this->title = $enm;

?>

<h1><?=$this->title?></h1>

<?php


$cs = [];

	$cs[] = [
		//'label' => 'Collection',
		'attribute' => 'title',
	];


	$cs[] = [
		'label' => 'Image',
		'contentOptions' => ['class' => 'col-xs-2'],
		'format' => 'raw',
		'value' => function($m)
		{
    	$u = $m->getImageUrl();
			return Html::img($u, ['class' => 'img-responsive']);
		},
	];

	$cs[] = [
		'label' => 'Price',
		//..'attribute' => '',
		'value' => function($m)
		{
    	return "$m->price USD";
		}
	];

	$cs[] = [
		'label' => 'Margin',
		//..'attribute' => '',
		'value' => function($m)
		{
    	return "$m->margin %";
		}
	];

	$cs[] = [
		'label' => 'Vendor',

		'format' => 'raw',
		'value' => function($m)
		{
    	$u = $m->vendor_url;
			return Html::a($u, $u, ['target' => '_blank']);
		},
	];



	$cs[] = [
		//'label' => 'Fa',
		'contentOptions' => ['class' => 'col-xs-3'],
		'format' => 'raw',
		'attribute' => 'fb_ad_text_example',
	];

	$cs[] = [
		//'label' => 'Fa',
		'contentOptions' => ['class' => 'col-xs-3'],
		'format' => 'raw',
		'attribute' => 'description',
	];

	$cs[] = [
		//'label' => 'Fa',
		//'contentOptions' => ['class' => 'col-xs-5'],
		'format' => 'raw',
		'attribute' => 'notes',
	];

	$cs[] = [
		'label' => 'Add to your store',
		'format' => 'raw',
		'value' => function($m)
		{
			$u = ['shopify-push', 'id' => $m->id];
			$a = Html::a("Add", $u, ['class' => 'btn btn-primary']);
			return $a;
		},
	];



	echo GridView::widget([
		'dataProvider' => $dp,
		'emptyText' => 'Nothing found',
  	'columns' => $cs,
	]);
