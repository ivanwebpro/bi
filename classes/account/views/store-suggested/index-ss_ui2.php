<?php
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\widgets\ActiveForm;
use common\widgets\SelObj;
use common\widgets\AjaxLoad;
use common\grid\BoolColumn;
use common\grid\DeltaColumn;
use common\grid\DecimalColumn;
use common\helpers\Url;
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii2mod\c3\chart\Chart as C3Chart;
use yii\web\JsExpression;
use common\widgets\TabsUI2 as Tabs;
use yii\data\ActiveDataProvider;

$enm = $this->context->entity_name_pl;
$this->title = $enm;

//return;

?>

<div class="main-title sug-title"><?=strtoupper($this->title)?></div>









<?php

	$cs = [];

	$cs[] = [
		'label' => '',
		'attribute' => 'domain',
		'format' => 'raw',
		'value' => function($m)
		{
			$r = '';

			$lbl = '';
      $lbl .= '<img src="'.appRootWeb().'/design2/icons-imgs/open-link.png" alt="">';
			$lbl .= $m->domain;
			$r .= Html::a($lbl, "https://$m->domain", ['target' => '_blank']);
			return $r;
		}
	];


$cs[] = [
  	'label' => '',
		'format' => 'raw',
		'value' => function($m) use ($shop)
		{
			$r = '';

			// check existence
			$q = $shop->getStoresQuery();
			$q->andWhere(['domain' => $m->domain]);
			if ($q->exists())
			{
				return "<i>This store has been already added</i>";
			}



			$u = ['add', 'suggested_store_id' => $m->id];

			$opts = ['class' => 'btn btn-primary'];
			//$opts['data-confirm'] = "Are you sure want to delete $m->url store?";
			//$opts['data-method'] = "POST";
      $r .= Html::a("Add", $u, $opts);
			return $r;
		}
	];


$tabs_prep = [];

$tab = [];
$tab['active'] = true;
$tab['label'] = "All";
$tab['stores'] = $dp->query->all();
$tabs_prep[] = $tab;

foreach($cats as $cat)
{
	$q = $cat->getStoresSuggestedQuery();

	if (!$q->exists())
	{
  	continue;
	}

	$tab = [];
	$tab['active'] = false;
	$tab['label'] = $cat->title;
	$tab['stores'] = $q->all();
	$tabs_prep[] = $tab;
}



foreach($tabs_prep as $tx)
{
	$tab = [];
	$tab['label'] = $tx['label'];
	$tab['active'] = $tx['active'];

	$c = '';
	$c .= '<div class="store-table"><span class="title-of-table">STORE</span>';

	foreach($tx['stores'] as $s)
	{


		$lbl = '';
		$lbl .= $s->domain;
		$a = Html::a($lbl, "https://$s->domain", ['target' => '_blank']);


		$c .= '<div class="row-of-table">';

		$c .= '<img src="'.appRootWeb().'/design2/icons-imgs/open-link.png" alt="">';
		$c .= $a;

		$q = $shop->getStoresQuery();
		$q->andWhere(['domain' => $s->domain]);
		if ($q->exists())
		{
			$c .= '<span class="store-already-added">This store has been already added</span>';
		}
		else
		{
			$u = ['add', 'suggested_store_id' => $s->id];
			if (!empty($_GET['ui2']))
			{
      	$u['ui2'] = 1;
			}
			$opts = ['class' => 'button-plus', 'style' => 'width: auto'];
			$opts['data_bg_on_click'] = '#f47b24';
			$opts['data_src_on_click'] = appRootWeb()."/design2/icons-imgs/noun-plus-active.png";

			$lbl = '<img src="'.appRootWeb().'/design2/icons-imgs/noun-plus.png" alt="">';
      $c .= Html::a($lbl, $u, $opts);
		}

  	$c .= '</div>';

	}

	$c .= '</div>';

	$tab['content'] = $c;

  $tabs[] = $tab;

}




$cfg = ['items' =>$tabs];


echo '<div class="border-tabs"></div>';

$cfg['ul_wrap_open'] = '<div class="container">
				<div class="row">
					<div class="col-md-12 z-index">';
$cfg['ul_wrap_close'] = "</div></div></div>";
$cfg['options'] = ['class' => 'ul-nav-tabs'];
$cfg['linkOptions'] = ['class' => 'nav-link small text-uppercase active-tabs'];

$cfg['tabs_wrap_open'] = '<div class="tabs-section">';
$cfg['tabs_wrap_close'] = '</div>';
echo Tabs::widget($cfg);




