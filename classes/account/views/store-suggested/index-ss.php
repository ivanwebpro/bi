<?php
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\widgets\ActiveForm;
use common\widgets\SelObj;
use common\widgets\AjaxLoad;
use common\grid\BoolColumn;
use common\grid\DeltaColumn;
use common\grid\DecimalColumn;
use common\helpers\Url;
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii2mod\c3\chart\Chart as C3Chart;
use yii\web\JsExpression;
use yii\bootstrap\Tabs;
use yii\data\ActiveDataProvider;

$enm = $this->context->entity_name_pl;
$this->title = $enm;

?>

<h1><?=$this->title?></h1>


<?php

	$cs = [];

	/*$cs[] = [
		'label' => 'Category',
		'attribute' => 'category.title',
	];*/

	$cs[] = [
		'label' => 'Store',
		'attribute' => 'domain',
		'format' => 'raw',
		'value' => function($m)
		{
			$r = Html::a($m->domain, "https://$m->domain", ['target' => '_blank']);
			return $r;
		}
	];

	/*$cs[] = [
		'label' => 'URL',
		'attribute' => 'url',
	];*/


$cs[] = [
  	'label' => '',
		'format' => 'raw',
		'value' => function($m) use ($shop)
		{
			$r = '';

			// check uniq
			$q = $shop->getStoresQuery();
			$q->andWhere(['domain' => $m->domain]);
			if ($q->exists())
			{
				return "<i>This store has been already added</i>";
			}



			$u = ['add', 'suggested_store_id' => $m->id];

			$opts = ['class' => 'btn btn-primary'];
			//$opts['data-confirm'] = "Are you sure want to delete $m->url store?";
			//$opts['data-method'] = "POST";
      $r .= Html::a("Add", $u, $opts);
			return $r;
		}
	];


$tabs = [];

	$tab = [];
	$tab['label'] = "All";

	$tab['content'] = GridView::widget([
		'summary' => '',
		'dataProvider' => $dp,
		'emptyText' => 'Nothing found',
  	'columns' => $cs,
	]);
  $tab['active'] = true;
	$tabs[] = $tab;


$first = true;

foreach($cats as $cat)
{
	$tab = [];
	$tab['label'] = $cat->title;

	$q = $cat->getStoresSuggestedQuery();

	if (!$q->exists())
	{
  	continue;
	}
	$dpx = new ActiveDataProvider(
			[
				'query' => $q,
				'pagination' => false,
				'sort' => false,
			]);

	$tab['content'] = GridView::widget([
		'summary' => '',
		'dataProvider' => $dpx,
		'emptyText' => 'Nothing found',
  	'columns' => $cs,
	]);

	if ($first)
	{
		//$tab['active'] = true;
	}

	$first = false;
	$tabs[] = $tab;
}

echo Tabs::widget(['items' =>$tabs]);






