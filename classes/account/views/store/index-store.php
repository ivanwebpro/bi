<?php
use common\helpers\Html;
use common\grid\GridView;
use common\grid\DeltaColumn;
use yii\grid\ActionColumn;

$this->title = "Saved stores";

echo "<h1>$this->title</h1>";

echo "<p>";
	echo Html::a("Add store", ['create'], ['class' => 'btn btn-primary']);
echo "</p>";

	$cs = [];

	if (isDebug())
	{
		$cs[] = [
  		'label' => 'ID',
			'attribute' => 'id',
		];
	}

	$cs[] = [
  	'label' => 'Store',
		'attribute' => 'domain',
		'format' => 'raw',
		'value' => function($m)
		{
			$r = Html::a($m->domain, "https://$m->domain", ['target' => '_blank']);
			return $r;
		},
	];

	$cs[] = [
  	'label' => 'Added',
		'attribute' => 'created_time',
		'value' => function($m)
		{
       $t = $m->created_time;
			$tf = date('d-M-Y, D h:i a', strtotime($t))." UTC";
			return $tf;
		}
	];

	if (!$shop->isPlanUnlimited())
	{
		$cs[] = [
  		'label' => 'Can be removed at',
			'attribute' => 'removeAllowedTime',
			'value' => function($m)
			{
        $t = $m->removeAllowedTime;
				$tf = date('d-M-Y, D h:i a', strtotime($t))." UTC";
				return $tf;
			}
		];
	}


	$cs[] = [
  	'label' => 'Last attempt to load',
		'attribute' => 'load_try_time',
		'value' => function($m)
		{
       $t = $m->load_try_time;
			$tf = date('d-M-Y, D h:i a', strtotime($t))." UTC";
			return $tf;
		}
	];

	$cs[] = [
  	'label' => '',
		'format' => 'raw',
		'value' => function($m)
		{
			$r = '';
			$u = ['product/index', 'store_id' => $m->id];

			$opts = ['class' => 'btn btn-success', 'target' => '_blank'];
      $r .= Html::a("See products", $u, $opts);
			return $r;
		}
	];



$cs[] = [
  	'label' => '',
		'format' => 'raw',
		'value' => function($m)
		{
			$r = '';
			$u = ['delete', 'id' => $m->id];

			$opts = ['class' => 'btn btn-danger'];
			$opts['data-confirm'] = "Are you sure want to delete store $m->domain?";
			$opts['data-method'] = "POST";
      $r .= Html::a("Delete", $u, $opts);
			return $r;
		}
	];



	/*$cs[] = [
		'class' => ActionColumn::className(),
		'template' => '{delete}',
	];*/

	echo GridView::widget([
			'dataProvider' => $dp,
			'emptyText' => 'No data',
			'summary' => false,
	  	'columns' => $cs,
		]);


