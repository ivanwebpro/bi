<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
use kartik\datecontrol\DateControl;

?>

<?php
	$form = ActiveForm::begin();
?>


<div class='row'>
	<div class='col-xs-12'>
		<h1><?=$this->title?></h1>

		<?= $form->errorSummary($model); ?>
		<?php
    	echo $form->field($model, 'url');
		?>
		<?php
 			echo $form->submitButton("Save");
		?>
	</div>
</div>
<br><br><br><br>
<?php
	ActiveForm::end();
