	<?php
use yii\helpers\Html;
use common\widgets\ActiveForm;
use yii\bootstrap\Alert;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

$this->title = "Settings";


?>
<?php
	$form = ActiveForm::begin();
?>


<div class='row'>
	<div class='col-xs-12'>
		<h1><?=$this->title?></h1>

		<?= $form->errorSummary($model); ?>
		<?php
			echo "<h2>Email notifications</h2>";
			echo $form->field($model, 'notify_email_enabled')->checkbox();
			echo $form->field($model, 'notify_email_target');

			echo "<h2>Desktop notifications</h2>";
			echo $form->field($model, 'notify_desktop_enabled')->checkbox(['notify_desktop_enable' => true]);


			echo "<h2>Push to Shopify</h2>";
			echo $form->field($model, 'publish_on_push_to_shopify')->checkbox();
 			echo $form->submitButton("Save");
		?>
	</div>
</div>
<br><br><br>
<?php
	ActiveForm::end();

?>
<br><br><br>
<?php
