<?php

//use yii\bootstrap4\Nav;
use common\widgets\NavBs4 as Nav;
use yii\bootstrap4\NavBar;

if (Yii::$app->user->isGuest)
{
	return;
}


NavBar::begin([
	//'brandLabel' => false,'',
	//'brandUrl' => Yii::$app->homeUrl,
	'options' => [
  	'class' => 'navbar navbar-expand-lg navbar-light bg-light my-navbar',
	],
	'containerOptions' => [
		'class' => 'collapse navbar-collapse',
		'id' => "navbarSupportedContent2",
	],
'renderInnerContainer' => false,
	//'renderInnerContainer' => true,
]);

?>
	<img src="<?=appRootWeb()?>/design2/icons-imgs/logo.png" alt="" class="logo-img">
	<img src="<?=appRootWeb()?>/design2/icons-imgs/user.png" alt="" class="user-img d-lg-none">

<?php
	include '_menu_ui2.php';

echo Nav::widget([
	'options' => ['class' => 'navbar-nav mr-auto indent-mr-auto'],
	'items' => $menuItems,
	//'renderInnerContainer' => false,
]);




NavBar::end();
