<?php

	$menuItems = [];


if (!Yii::$app->user->isGuest)
{
$shop = Yii::$app->shop;


$menuItems[] = [
	'label' => 'Products',
	'url' => ['product/index'],
];

/*$menuItems[] = [
'label' => 'Stores',
'url' => ['store/index'],
];*/

$menuItems[] = [
 	'label' => "Saved",
	'items' =>
	[
		[
			'label' => 'Saved stores',
			'url' => ['store/index'],
		],

		[
			'label' => 'Saved products',
			'url' => ['product/saved'],
		],
	]
];


$menuItems[] = [
'label' => 'Suggested stores',
'url' => ['store-suggested/index'],
];

if (!$shop->isPlanUnlimited())
{
  $menuItems[] = [
		'label' => 'Upgrade',
    'url' => ['profile/plan-upgrade'],
		'linkOptions' => ['style' => 'color: #ff7d1e'],
	];
}


	$menuItems[] = [
 		'label' => "Profile <img src='$shop->iconUrl' style='max-height: 25px; max-width: 25px'>",
		'encodeLabels' => false,
		'items' =>
		[
			[
				'label' => 'Open Shopify store (in new window)',
				'url' => $shop->url,
				'linkOptions' => ['target' => '_blank'],
			],

			[
				'label' => 'Open Shopify store admin (in new window)',
				'url' => $shop->urlAdmin,
				'linkOptions' => ['target' => '_blank'],
			],

			[
				'label' => 'Settings',
				'url' => ['shop/update'],
			],

			[
				'label' => 'Shopify integration',
				'url' => ['profile/shopify'],
			],

			[
				'label' => 'Manage subscription',
				'url' => ['profile/subscription'],
			],

			[
				'label' => 'Support',
				'url' => ['profile/support'],
			],


			[
				'label' => 'Logout',
				'url' => ['app/logout'],
				'linkOptions' => ['data-method' => 'post'],
			],

		]
  ];

}