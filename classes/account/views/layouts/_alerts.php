<?php
use common\widgets\Alert;
use yii\bootstrap\Alert as AlertOne;

?>

<div class='row'>
	<div class='col-xs-12'>


		<?php
    	if (!empty($shop) && $shop->uninstalled)
			{
      	$un = "App has been uninstalled from your Shopify store".Html::a("Click to connect", ['app/index', 'shop' => $shop->host]);

				echo AlertOne::widget([
 					'body' => $un,
       		'closeButton' => false,
       		'options' => ['class' => 'alert-danger'],
       	]);
				echo "<br><br>";

			}
		?>

		<?= Alert::widget() ?>
	</div>
</div>


<?php

