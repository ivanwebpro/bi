<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
//use account\assets\AppAsset;
use common\assets\AppAsset;
use common\widgets\Alert;
use common\helpers\Html;
use common\helpers\Url;
use account\models\Shop;
use common\widgets\LoadingAnimation;
use common\widgets\DesktopNotification;
use yii\bootstrap\Alert as AlertOne;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/icon" href="<?=appRootWeb();?>/favicon.ico" />


	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

	<?php
		$ux = Url::to(['app/custom-css-global']);
		echo Html::cssFile($ux);

		if ($this->custom_css_split_test_create)
		{
			$ux = Url::to(['app/custom-css-split-test-create']);
			echo Html::cssFile($ux);
		}

	?>
</head>
<body>
  <?php $this->beginBody() ?>
	<?php

  	echo LoadingAnimation::widget();
  	echo DesktopNotification::widget();
	?>

    <div class="wrap">
        <?php

				if ($this->menu_display)
				{

            NavBar::begin([
                'brandLabel' => Yii::$app->name,
                'brandUrl' => ['product/index'],
                'options' => [
                    'class' => 'navbar navbar-inverse',
                ],
            ]);

						$menuItems = [];


            if (!Yii::$app->user->isGuest)
						{
							$shop = Yii::$app->shop;


							$menuItems[] = [
								'label' => 'Products',
								'url' => ['product/index'],
							];

							/*$menuItems[] = [
								'label' => 'Stores',
								'url' => ['store/index'],
							];*/

        	$menuItems[] = [
              	'label' => "Saved",
								'items' =>
									[
										[
											'label' => 'Saved stores',
											'url' => ['store/index'],
										],
										[
											'label' => 'Saved products',
											'url' => ['product/saved'],
										],
									]
									];


							$menuItems[] = [
								'label' => 'Suggested stores',
								'url' => ['store-suggested/index'],
							];

							if (!$shop->isPlanUnlimited())
							{
	              $menuItems[] = [
									'label' => 'Upgrade',
	                'url' => ['profile/plan-upgrade'],
									'linkOptions' => ['style' => 'color: #ff7d1e'],
								];
							}


            	$menuItems[] = [
              	'label' => "Profile <img src='$shop->iconUrl' style='max-height: 25px; max-width: 25px'>",
								'encodeLabels' => false,
								//.(!isLocalHost() ? "" : " [$shop->id]" ),
								'items' =>
									[
										[
											'label' => 'Open Shopify store (in new window)',
											'url' => $shop->url,
											'linkOptions' => ['target' => '_blank'],
										],

										[
											'label' => 'Open Shopify store admin (in new window)',
											'url' => $shop->urlAdmin,
											'linkOptions' => ['target' => '_blank'],
										],

                  	[
											'label' => 'Settings',
                			'url' => ['shop/update'],
										],

                  	[
											'label' => 'Shopify integration',
                			'url' => ['profile/shopify'],
										],
                  	[
											'label' => 'Manage subscription',
                			'url' => ['profile/subscription'],
										],

                  	[
											'label' => 'Support',
                			'url' => ['profile/support'],
										],


										[
											'label' => 'Logout',
                			'url' => ['app/logout'],
                			'linkOptions' => ['data-method' => 'post'],
										],


									]
                ];

            }

            	echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
								'encodeLabels' => false,
            	]);

             NavBar::end();
        }

        ?>




        <div class="container">
        <?php

?>


<div class='row'>
	<div class='col-xs-12'>


		<?php
    	if (!empty($shop) && $shop->uninstalled)
			{
      	$un = "App has been uninstalled from your Shopify store".Html::a("Click to connect", ['app/index', 'shop' => $shop->host]);

				echo AlertOne::widget([
 					'body' => $un,
       		'closeButton' => false,
       		'options' => ['class' => 'alert-danger'],
       	]);
				echo "<br><br>";

			}
		?>

		<?= Alert::widget() ?>
	</div>
</div>






        <?= $content ?>
				<br><br><br>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <!--<p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>-->

				<p class=text-center>
					WHOSSELLINGWHAT COPYRIGHT © 2018 | Contact: <a href='mailto:support@whossellingwhat.com'>support@whossellingwhat.com</a>
    		</p>

				<p class='text-center' style='font-size: 80%;'>
					Disclaimer: Who's Selling What is in no way affiliated with Shopify or any of its subsidiaries.
				</p>

        </div>
    </footer>

<?php
	if (Yii::$app->shop)
	{
		$h = Yii::$app->shop->host;
  	echo "<i style='color:white'>$h</i>";
	}
?>
    <?php $this->endBody() ?>
<?php
	$app_cfg = Yii::$app->appCfg;

  //echo $app_cfg->tracking_code_all;

	/*if (Yii::$app->user->isGuest)
	{
		echo $app_cfg->tracking_code_not_logged_in;
	}
	else
	{
		echo $app_cfg->tracking_code_logged_in;
	} */

?>
</body>
</html>
<?php $this->endPage() ?>
