<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
//use account\assets\AppAsset;
use common\assets\AppAssetUI2 as AppAsset;
use common\widgets\Alert;
use common\helpers\Html;
use common\helpers\Url;
use account\models\Shop;
use common\widgets\LoadingAnimation;
use yii\bootstrap\Alert as AlertOne;

$this->bs4 = true;
$this->deny_bs3 = true;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/icon" href="<?=appRootWeb();?>/favicon.ico" />


	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

</head>

<body>

  <?php $this->beginBody() ?>

	<?php

  	echo LoadingAnimation::widget();
	?>

<div class="wrap">
<?php

	if ($this->menu_display)
	{

		NavBar::begin([
			'brandLabel' => Yii::$app->name,
			'brandUrl' => ['product/index'],
			'options' => [
				'class' => 'navbar navbar-inverse',
			],
		]);

		include '_menu_ui2.php'

		echo Nav::widget([
			'options' => ['class' => 'navbar-nav navbar-right'],
			'items' => $menuItems,
			'encodeLabels' => false,
		]);

		NavBar::end();
	}

?>




	<div class="container">
		<?php
	  	include '_alerts_ui2.php';
		?>

	  <?= $content ?>
		<br><br><br>
	</div>

</div>

<footer class="footer">
	<div class="container">
  	<!--<p class="pull-left">&copy; My Company <?= date('Y') ?></p>
    <p class="pull-right"><?= Yii::powered() ?></p>-->

		<p class=text-center>
			WHOSSELLINGWHAT COPYRIGHT © 2018 | Contact: <a href='mailto:support@whossellingwhat.com'>support@whossellingwhat.com</a>
    </p>

		<p class='text-center' style='font-size: 80%;'>
			Disclaimer: Who's Selling What is in no way affiliated with Shopify or any of its subsidiaries.
		</p>

  </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php
	$this->endPage();
