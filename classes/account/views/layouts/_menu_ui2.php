<?php

include '_svg_ui2.php';
$menuItems = [];


if (!Yii::$app->user->isGuest)
{
	$shop = Yii::$app->shop;

// Products
$menuItems[] = [
	'li_prepend' => '<span class="item-block products-link-navbar">
									<span class="">',
	'li_append' => '</span></span>',
	'linkOptions' => ['class' => 'product-link-indent'],
	'label' => '<img src="'.appRootWeb().'/design2/icons-imgs/svgs/noun_products_1375571(1).svg" alt="" class="products-icon"> Products',
	'url' => ['product/index'],
	'encode' => false,
];


// Saved
$menuItems[] = [
	'encode' => false,
	'linkOptions' => ['class' => 'my-dropdown-toggle'],
	//'li_prepend' => '<span class="item-block no-border item-block-active">
		//						<span class="internal-item-block-none-border">',
	//'li_append' => '</span></span>',

 	'label' => $svg_saved.' Saved <img src="'.appRootWeb().'/design2/icons-imgs/down-arrow.png" alt="" class="down-arrow">',
	'items' =>
	[
		[
			'label' => 'Saved stores',
			'url' => ['store/index'],
		],

		[
			'label' => 'Saved products',
			'url' => ['product/saved'],
		],
	]
];


$menuItems[] = [
'label' => $svg_suggested.' Suggested stores',
'li_prepend' => '<span class="item-block no-border item-block-active">
								<span class="internal-item-block-none-border">',
'li_append' => '</span></span>',
'encode' => false,
'linkOptions' => ['class' => 'nav-link activesugg nav-link-indents'],


'url' => ['store-suggested/index'],
];

if (!$shop->isPlanUnlimited() || isLocalHost())
{
	// UPGRADE
  $menuItems[] = [
		'encode' => false,
		'li_prepend' => '<span class="item-block no-border border-right">',
		'li_append' => '</span>',
		'label' => '<button class="menu-button">
												<img src="'.appRootWeb().'/design2/icons-imgs/svgs/noun_update_1572676.svg" alt="" class="button-top-menu-icon">
												<span>UPGRADE</span>
											</button>',
    'url' => ['profile/plan-upgrade'],
		'linkOptions' => ['style' => 'color: #ff7d1e'],
	];
}


	$profile_menu = [
 		//'label' => "Profile <img src='$shop->iconUrl' style='max-height: 25px; max-width: 25px'>",
 		'label' => '<img src="'.appRootWeb().'/design2/icons-imgs/user.png" alt="" class="user-img">',
		'encode' => false,
		'options' => ['class' => ['d-none d-lg-block d-xl-block d-xl-none', 'ni' => false, 'widget' => false]],
	];



	$profile_menu['items'] = [];
	$profile_menu['items'][] = [
				'label' => 'Open Shopify store (in new window)',
				'url' => $shop->url,
				'linkOptions' => ['target' => '_blank'],
			];

	$profile_menu['items'][] = [
				'label' => 'Open Shopify store admin (in new window)',
				'url' => $shop->urlAdmin,
				'linkOptions' => ['target' => '_blank'],
			];

	$profile_menu['items'][] = [
				'label' => 'Settings',
				'url' => ['shop/update'],
			];

	$profile_menu['items'][] = [
				'label' => 'Shopify integration',
				'url' => ['profile/shopify'],
			];

	$profile_menu['items'][] = [
				'label' => 'Manage subscription',
				'url' => ['profile/subscription'],
			];

	$profile_menu['items'][] = [
				'label' => 'Support',
				'url' => ['profile/support'],
			];

	$profile_menu['items'][] = [
				'label' => 'Logout',
				'url' => ['app/logout'],
				'linkOptions' => ['data-method' => 'post'],
			];

	$menuItems[] = $profile_menu;

}