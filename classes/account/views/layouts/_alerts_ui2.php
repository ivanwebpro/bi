<?php
use common\widgets\AlertUI2 as Alert;
use yii\bootstrap4\Alert as AlertOne;

?>

<div class='row' style='margin-top: 30px'>
	<div class='col-12'>


		<?php
    	if (!empty($shop) && $shop->uninstalled)
			{
      	$un = "App has been uninstalled from your Shopify store".Html::a("Click to connect", ['app/index', 'shop' => $shop->host]);

				echo AlertOne::widget([
 					'body' => $un,
       		'closeButton' => false,
       		'options' => ['class' => 'alert-danger'],
       	]);
				echo "<br><br>";

			}
		?>

		<?= Alert::widget() ?>
	</div>
</div>


<?php

