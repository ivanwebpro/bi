<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
//use account\assets\AppAsset;
use common\assets\AppAssetUI2 as AppAsset;
use common\widgets\Alert;
use common\helpers\Html;
use common\helpers\Url;
use account\models\Shop;
use common\widgets\LoadingAnimation;
use yii\bootstrap\Alert as AlertOne;

$this->bs4 = true;
$this->deny_bs3 = true;
\rmrevin\yii\fontawesome\AssetBundle::register($this);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" type="image/icon" href="<?=appRootWeb();?>/favicon.ico" />


	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

</head>

<body>
<?php $this->beginBody() ?>
<?php
	//echo LoadingAnimation::widget();
?>

<div class="container">
	<div class="row">
		<?php
			if ($this->menu_display)
			{
				include '_nav_bar_ui2.php';
		  }
		?>
	</div>
</div>

	<div class="container-fluid">
		<div class="row">
			<div class="bottom-border"></div>
		</div>
	</div>

<div class="container">
	<div class="row">
		<div class="col-12">

		<?php
			include '_alerts_ui2.php';
		?>

		<?= $content ?>
		<br><br><br>
		</div>
	</div>
</div>


	<div class="container-fluid">
		<div class="row">
			<span class="disclaimer">Disclaimer: Who's Selling What is in no way affiliated with Shopify or any of its subsidiaries.</span>
		</div>

		<div class="row copyright-indent">
			<div class="col-12 col-sm-6">
				<span class="copyright">WHOSSELLINGWHAT COPYRIGHT © 2018</span>
			</div>

			<div class="col-12 col-sm-6">
				<img src="<?=appRootWeb()?>/design2/icons-imgs/contact.png" alt="">
				<span class="contact-website">Contact:
					<a href="#">support@whossellingwhat.com</a>
				</span>
			</div>
		</div>
	</div>

<?php $this->endBody() ?>
</body>
</html>
<?php
	$this->endPage();
