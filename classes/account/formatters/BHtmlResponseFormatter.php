<?php

namespace common\formatters;


use yii\base\Component;

/**
 * HtmlResponseFormatter formats the given data into an HTML response content.
 *
 * It is used by [[Response]] to format response data.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BHtmlResponseFormatter extends Component implements \yii\web\ResponseFormatterInterface
{
    /**
     * @var string the Content-Type header for the response
     */
    public $contentType = 'text/html';


    /**
     * Formats the specified response.
     * @param Response $response the response to be formatted.
     */
    public function format($response)
    {
        /*if (stripos($this->contentType, 'charset') === false) {
            $this->contentType .= '; charset=' . $response->charset;
        }
        $response->getHeaders()->set('Content-Type', $this->contentType);*/

        if ($response->data !== null)
				{
        	$d = $response->data;

					if ($response->statusCode == 200)
					{
					$indenter = new \Gajus\Dindent\Indenter();
					$d = $indenter->indent($d);


					//$d = str_ireplace("<", "\n<", $d);
/*
$config = array(
           'indent'         => true,
           'output-xhtml'   => true,
           'wrap'           => 200);


$tidy = new \Tidy;
$tidy->parseString($d, $config, 'utf8');
$tidy->cleanRepair();
$d = (string)$tidy;
                */
								// http://www.bioinformatics.org/phplabware/internal_utilities/htmLawed/
								// https://github.com/gajus/dindent

					$d = str_ireplace("><", ">\n<", $d);
					}
					
					$response->content = $d;


					/*$dom = new \DOMDocument();
					$dom->preserveWhiteSpace = false;
					@$dom->loadHTML($response->data,LIBXML_HTML_NOIMPLIED);
					$dom->formatOutput = true;
					$response->content = $dom->saveXML($dom->documentElement);
          */
        }
    }
}
