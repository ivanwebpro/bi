<?php

namespace account\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use common\helpers\Url;
use yii\helpers\Html;
use yii\db\Expression;
use yii\data\Sort;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use account\models\Shop;
use account\models\Store;
use account\models\LogStore;
use api\models\Order;
use account\models\Product;
use account\segments\Segment;
use account\reports\Report;
use account\models\StoreAddForm;
use account\models\ProductVariantCostImport;

use common\helpers\ArrayHelper;
use account\helpers\CronHelper;
use api\helpers\OrderHelper;

class StoreController extends base\Controller
{



	public function actionParseLogStore($lf_id)
	{
 		$lf = LogStore::findOne($lf_id);
		$lf->parse();
		hr("OK");
	}

	public function actionIndex()
	{
    $shop = $this->shop;

		$r = [];
		$r['shop'] = $shop;

		$cfg = [];
		$cfg['query'] = $shop->getStoresQuery();

		$cfg['pagination'] = false;
		$r['dp'] = new ActiveDataProvider($cfg);

		return $this->render('index-store', $r);


	}



	/*public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save())
		{
			Yii::$app->session->setFlash('success', 'Data saved');
			return $this->redirect(['index']);
		}
		else
		{
			$r = [];
			$r['model'] = $model;
			return $this->render("update-store", $r);
		}
	}    */

	public function actionCreate()
	{
		$shop = $this->shop;
  	$store_add_form = new StoreAddForm;
		$store_add_form->shop = $this->shop;

		if ($store_add_form->load(Yii::$app->request->post()))
		{
    	if ($store_add_form->validate())
			{
				$store_add_form->saveNewStore();
				Yii::$app->session->addFlash('success', 'Store has been added');
      	return $this->redirect(['index']);
			}
		}


		$r = [];
		$r['model'] = $store_add_form;
		return $this->render("create-store", $r);
	}



	public $entity_name = 'store';

	protected function findModel($id)
	{
		if (($model = $this->shop->getStoreById($id)))
		{
			return $model;
		}
		else
		{
			throw new NotFoundHttpException("The requested store does not exist");
		}
	}

	public function actionDelete($id)
	{
  	$m = $this->findModel($id);

		if (!$this->shop->isPlanUnlimited())
		{
			$rat = $m->removeAllowedTime;

			if ($m->created_time < $rat)
			{
	      Yii::$app->session->setFlash('warning', "This store can't be deleted before $rat");

				if (!isDebug())
				{
					return $this->redirect(['index']);
				}
			}
		}

		$m->delete();
		Yii::$app->session->setFlash('success', "Store has been deleted");

		return $this->redirect(['index']);
	}
}
