<?php

namespace account\controllers;

use Yii;
use yii\data\ArrayDataProvider;
use yii\base\UserException;


use account\models\ActivateForm;
use common\helpers\Url;
use yii\helpers\Html;
use yii\db\Expression;
use admin\models\AppCfg;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use common\helpers\ArrayHelper;

class ActivateController extends base\Controller
{

	public function behaviors()
	{
  	$bs = parent::behaviors();

		$r = [
    	'allow' => true,
			'actions' => [
				'index',
				'activated',
				],
      ];

		$bs['access']['rules'][] = $r;

		return $bs;
	}



 	public function actionActivated($shop)
	{
		$h = $shop;

		$sub = \admin\models\Subscription::getActiveForHost($shop);

		if (!$sub)
		{
	  	$msg = "Can't find active subscription for store `$shop_host`, please activate";
			$u = ['activate/index', 'msg' => $msg];
			Yii::$app->response->redirect($u);
			Yii::$app->end();
		}


		$r = [];
		$r['url'] = ['app/index', 'shop' => $shop];
		return $this->render('activated-activate', $r);
	}

 	public function actionIndex($msg='')
	{
		$model = new ActivateForm;

		if ($model->load(Yii::$app->request->post()))
		{
    	if ($model->validate())
			{
				$shop_host = $model->shopHost;
				$u = ['activated', 'shop' => $shop_host];
				return $this->redirect($u);
			}
		}

		$r = [];
		$r['model'] = $model;
		$r['msg'] = $msg;

		return $this->render('index-activate', $r);
	}


	public function beforeAction($action)
	{
  	$this->enableCsrfValidation = false;
    return parent::beforeAction($action);
	}
}
