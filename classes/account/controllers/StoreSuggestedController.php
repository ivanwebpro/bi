<?php

namespace account\controllers;

use Yii;
use common\apis\TelegramApi;

use account\models\Account;
use account\models\StorePreset;
use yii\data\ActiveDataProvider;


use yii\data\Sort;
use yii\data\ArrayDataProvider;

use yii\base\UserException;

use account\models\Contact;
use account\models\StoreSuggested;
use account\models\Alert;
use account\models\User;
use account\models\LogAppUninstall;
use common\helpers\Url;
use common\helpers\DatesHelper;
use common\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\db\Expression;
use account\models\Shop;
use account\models\ShopifyAuthForm;
use account\models\LogAlert;
use admin\models\Category;
use yii\db\Query;
use admin\models\AppCfg;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use account\helpers\CronHelper;
use account\models\StoreAddForm;

class StoreSuggestedController extends base\Controller
{
  public $defaultAction = 'index';



	public function actionAdd($suggested_store_id)
	{
		$sp = StoreSuggested::findOne($suggested_store_id);

		if (!$sp)
		{
    	throw new NotFoundHttpException("The requested suggested store #$suggested_store_id does not exist");
		}

		$url = $sp->url;

		$shop = $this->shop;
  	$store_add_form = new StoreAddForm;
		$store_add_form->shop = $this->shop;
		$store_add_form->url = $url;
		$store_add_form->validate_store_http = false;

		$ret_url = ['index'];

		if (!empty($_GET['ui2']))
		{
    	$ret_url['ui2'] = 1;
		}

    if ($store_add_form->validate())
		{
			$store_add_form->saveNewStore();
			Yii::$app->session->addFlash('success', 'Store has been added');
      return $this->redirect($ret_url);
		}

    $errs = Html::errorSummary($store_add_form);
		Yii::$app->session->addFlash('error', $errs);
		return $this->redirect($ret_url);
	}

	public function actionIndex()
	{
		$ec = $this->entity_class;
		$q = $ec::find();

		$tn = Category::tableName();
		$q->innerJoin($tn, "category_id = $tn.id");
		$q->orderBy(["$tn.title" => SORT_ASC, "domain" => SORT_ASC]);

		$ks = ['id', 'title'];
    foreach($ks as $k)
		{
			$s_attrs[$k] = ['default' => SORT_ASC];
		}
		$sort = new Sort([
    	'attributes' => $s_attrs,
			'defaultOrder' => [$ks[0] => SORT_DESC],
		]);


		$dp = new ActiveDataProvider(
			[
				'query' => $q,
				'pagination' => false,
				'sort' => $sort,
			]);

		$r = [];
    $r['dp'] = $dp;
		$r['shop'] = $this->shop;

		$cats = Category::find()->orderBy(['title' => SORT_ASC])->all();
		$r['cats'] = $cats;

		/*if (empty($_GET['ui2']))
		{
			return $this->render("index-$this->entity_alias", $r);
		} */

		$this->layout = 'account_layout_ui2';
			Yii::$app->response->formatters = [
        Yii::$app->response::FORMAT_HTML => [
            'class' => 'common\formatters\BHtmlResponseFormatter',
        ],
    	];


		return $this->render("index-$this->entity_alias"."_ui2", $r);
	}



	public $entity_name = 'Suggested store';
	public $entity_name_pl = 'Suggested stores';
	public $entity_alias = 'ss';
	public $entity_class = "account\\models\\StoreSuggested";

	protected function findModel($id)
	{
		$ec = $this->entity_class;

		if ($model = $ec::findOne($id))
		{
			return $model;
		}
		else
		{
			throw new NotFoundHttpException("The requested $this->entity_name does not exist");
		}
  }

}
