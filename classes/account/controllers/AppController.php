<?php

namespace account\controllers;

use Yii;
use yii\data\ArrayDataProvider;
use yii\base\UserException;
use account\models\AuthRequest;
use account\models\Contact;
use account\models\AppActivateForm;
use account\models\LogAppUninstall;
use common\helpers\Url;
use yii\helpers\Html;
use yii\db\Expression;
use account\models\Shop;
use account\models\ShopifyAuthForm;
use yii\db\Query;
use admin\models\AppCfg;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use common\helpers\ArrayHelper;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;


class AppController extends base\Controller
{
  public $defaultAction = 'index';

  public function actionShopIcon()
	{
		$u = $this->shop->iconUrl;

		$r = '';
		$r .= "$u";
		$r.= "<img src='$u'>";
		return $r;
	}

  public function actionCustomCssGlobal()
	{
  	$ac = Yii::$app->appCfg;

		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
    //$this->layout = "no_layout";
		$hrs = Yii::$app->response->getHeaders();
		$hrs->set('Content-Type', 'text/css; charset=UTF-8');

		return '';
	}

  public function actionCustomCssSplitTestCreate()
	{
  	$ac = Yii::$app->appCfg;

		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
    //$this->layout = "no_layout";
		$hrs = Yii::$app->response->getHeaders();
		$hrs->set('Content-Type', 'text/css; charset=UTF-8');

		return '';
	}


	public function actionRefreshSchema()
	{
		//https://github.com/yiisoft/yii2/issues/421
		//
  	Yii::$app->db->schema->refresh();
		print "OK";
	}

	public function behaviors()
	{
  	$bs = parent::behaviors();

		$r = [
    	'allow' => true,
			'actions' => ['index', 'on-uninstall',
				'help', 'login', 'refresh-schema',
				'error', 'setup-complete', 'charges-activated',
				'custom-css',
				'custom-css-global',
				'custom-css-split-test',
				'gr',
				],
      ];

		$bs['access']['rules'][] = $r;

		return $bs;
	}

	public function actions()
	{
		return [
			'error' => [
					'class' => 'yii\web\ErrorAction',
				],
			'index' => [
					'class' => 'common\actions\ShopifyConnectAction',
				],
			'charges-activated' => [
					'class' => 'common\actions\ShopifyChargesActivated',
				],

			];
	}

 	public function actionOnUninstall()
	{
		$l = new LogAppUninstall();
		$b = Yii::$app->request->getRawBody();

		$l->data = $b;//file_get_contents('php://input');

		$a = json_decode($b, true);
		$l->shop_host = $a['myshopify_domain'];
		$l->saveWithException();

		if ($s = Shop::findByHost($l->shop_host))
		{
			$s->activated = 0;
    	$s->uninstalled = 1;
    	$s->uninstalled_time = new Expression("NOW()");
			// reset in case of multiple installing / uninstalling
    	$s->uninstalled_data_deleted_time = null;
    	$s->uninstalled_data_deleted = 0;
			$s->saveWithException();
			$s->delete();
		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    return [
    	'message' => 'OK',
      'code' => 200,
    ];
	}

 	public function actionLogout()
	{
		Yii::$app->user->logout();
		return $this->goHome();
	}


 	public function actionLogin()
	{
		return $this->redirect(['index']);
	}


	public function beforeAction($action)
	{
    if ($action->id == 'on-uninstall')
		{
    	$this->enableCsrfValidation = false;
    }

    return parent::beforeAction($action);
	}

	public function actionDesktopNotificationsJson($test=0)
	{
		$shop = $this->shop;

		$res = [];
		$res['enabled'] = $shop->notify_desktop_enabled;
		$res['message'] = null;
		$res['url'] = null;

		if ($shop->notify_desktop_enabled)
		{
			$q = $shop->getProductsQuery()->orderBy('id ASC');


			if (!$test)
			{
				$q->andWhere(['>', 'published_time_utc', $shop->notify_desktop_enabled_time]);
				$q->andWhere(['notify_desktop' => 0]);
			}
			else
			{
				$res['test'] = 1;
				//$res['sql'] = $q;
			}
			$q->limit(1);
			$p = $q->one();

			if ($p)
			{
				$url = appRootWeb()."/product/view?id=$p->id";

				$res['message'] = "New product `$p->title was published on $p->store_domain";
				$res['url'] = $url;

				$p->notify_desktop = 1;
				$p->notify_desktop_time = new Expression("NOW()");
				$p->saveEx();
			}
		}

		return $this->asJson($res);

	}

	public function actionTestEmail2()
  {
		$shop = $this->shop;
		$q = $shop->getProductsQuery()->orderBy('id DESC');
		$q->limit(1);

		$p = $q->one();
		$r = $p->sendEmailNotification($to_email='ivan@ivanpro.com');

		hr("r: $r");
	}

	public function actionTestEmail()
	{
		$app_name = Yii::$app->name;
		//$to_email = 'support@ecomfuel.io';
		$to_email = 'ivan@ivanpro.com';

		//$from_email = 'no-reply@ecomfuel.io';

		$from_email = 'no-reply@whossellingwhat.io';
		$from_name = $app_name;

    $c = \account\models\Contact::findOne(24);

		$res = Yii::$app->mailer
			->compose('@account/views/contact/email', ['c' => $c])
			//->disableAsync()
    	->setFrom([$from_email => $from_name])
    	->setTo($to_email)
    	//->setSubject($app_name.": contact form")
    	->send();

		//hr("R: $res");

		$r = [];
		$r['msg'] = "Send res: $res";
		$r['title'] = "Test send";

		$v = '@account/views/cron/cron.php';
		return $this->render($v, $r);
	}
}
