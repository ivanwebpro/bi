<?php

namespace account\controllers;

use Yii;
use common\apis\TelegramApi;

use account\models\Account;
use yii\data\ActiveDataProvider;


use yii\data\Sort;
use yii\data\ArrayDataProvider;

use yii\base\UserException;

use account\models\Contact;
use account\models\AppActivateForm;
use account\models\Symbol;
use account\models\Tick;
use account\models\Alert;
use account\models\User;
use account\models\Candle;
use account\models\LogAppUninstall;
use common\helpers\Url;
use common\helpers\DatesHelper;
use common\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\db\Expression;
use account\models\Shop;
use account\models\ShopifyAuthForm;
use account\models\LogAlert;
use yii\db\Query;
use admin\models\AppCfg;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use account\helpers\CronHelper;

class ProductImportController extends base\Controller
{
  public $defaultAction = 'index';



	public function actionIndex()
	{
		$ec = $this->entity_class;
		$q = $ec::find();

		$dp = new ActiveDataProvider(
			[
				'query' => $q,
				'pagination' => false,
			]);

		$r = [];
    $r['dp'] = $dp;
		return $this->render("index-$this->entity_alias", $r);
	}


	public function actionShopifyPush($id)
	{
		$product = $this->findModel($id);

		$shop = $this->shop;
    $err_msg = '';
    $msg = '';

		$li = $product->shopifyPush($shop);

		if ($li->success)
		{
			$u = $li->shopifyUrl;
			$a = Html::a($u, $u, ['target' => '_blank']);
	    $msg = "Product has been added to your Shopify store. You can look it here: $a";
    }
		else
		{
			$err_msg = $li->err_msg;
		}


		$r = [];
		$r['product'] = $product;
		$r['msg'] = $msg;
		$r['err_msg'] = $err_msg;

		//hre($r);

		return $this->render("shopify-push-$this->entity_alias", $r);

	}


	public $entity_name = 'Product';
	public $entity_name_pl = 'Products';
	public $entity_alias = 'pi';
	public $entity_class = "admin\\models\\Product";

	protected function findModel($id)
	{
		$ec = $this->entity_class;

		if ($model = $ec::findOne($id))
		{
			return $model;
		}
		else
		{
			throw new NotFoundHttpException("The requested $this->entity_name does not exist");
		}
  }

}
