<?php

namespace account\controllers;

use Yii;
use common\apis\TelegramApi;

use account\models\Account;
use yii\data\ActiveDataProvider;


use yii\data\Sort;
use yii\data\ArrayDataProvider;

use yii\base\UserException;

use account\models\Contact;
use account\models\AppActivateForm;
use account\models\StoreAddForm;
use account\models\Alert;
use account\models\User;
use account\models\Candle;
use account\models\LogAppUninstall;
use common\helpers\Url;
use common\helpers\DatesHelper;
use common\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\db\Expression;
use account\models\Shop;
use account\models\ShopifyAuthForm;
use account\models\LogAlert;
use yii\db\Query;
use admin\models\AppCfg;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use account\helpers\CronHelper;

class ProfileController extends base\Controller
{
  public $defaultAction = 'index';

	public $entity_alias = 'profile';

	public function actionShopify()
	{
		$r = [];

		$shop = $this->shop;

		$model = new \account\models\ShopifySwitch;
		$model->shop_id = $shop->id;
		$model->old_shop_host = $shop->host;

		$sn = str_ireplace(".myshopify.com", "", $shop->host);
		$model->new_shopify_name = $sn;

		if ($model->load(Yii::$app->request->post()))
		{
    	if ($model->save())
			{
      	if ($model->run())
				{
					return $this->redirect(appRootWeb());
				}
				else
				{
        	Yii::$app->session->addFlash('error', "Error on switch Shopify store: $model->err");
					return $this->refresh();
				}
			}
		}

		$r = [];
		$r['model'] = $model;
		return $this->render("shopify-$this->entity_alias", $r);
	}

	public function actionSupport()
	{
		$shop = $this->shop;

		$contact_model = new \account\models\Contact;
		$contact_model->shop_id = $shop->id;
		$contact_model->shop_host = $shop->host;

		if ($contact_model->load(Yii::$app->request->post()))
		{
    	if ($contact_model->validate())
			{
      	if ($contact_model->save())
				{
        	$contact_model->sendMail();
					Yii::$app->session->addFlash('success', "Thank you! Your message has been sent");
					return $this->refresh();
				}
			}
		}

		$cancel_model = new \account\models\PlanCancelForm;

		if ($cancel_model->load(Yii::$app->request->post()))
		{
			if ($cancel_model->cancel())
			{
      	Yii::$app->session->addFlash('success', "Subscription has been cancelled");
			}
			else
			{
        Yii::$app->session->addFlash('error', "Error on cancelling subscription: $cancel_model->err");
			}
			return $this->refresh();
		}




		$r = [];
		$r['contact_model'] = $contact_model;
		$r['cancel_model'] = $cancel_model;
		$r['shop'] = $shop;
		return $this->render("support-$this->entity_alias", $r);
	}

	public function actionSubscription()
	{

  	$model = new \account\models\PlanChange;
		$model->shop = $this->shop;

		if ($model->load(Yii::$app->request->post()))
		{
    	if ($model->changePlan())
			{
				Yii::$app->session->addFlash('success', "Plan has been changed");
			}
			else
			{
        Yii::$app->session->addFlash('error', "Error on changing plan: $model->err");
			}

			return $this->refresh();
		}


		$r = [];
		$r['shop'] = $this->shop;
		$shop = $this->shop;

		$acc_id = null;
		$sub_id = null;

		if (!$shop->isExcludedFromBilling())
		{
			$sub =  $shop->subscription;
    	$acc_id = $sub->account_code;
    	$sub_id = $sub->recurly_id;

			if ('prt04.myshopify.com' == $shop->host)
			{
      	//$acc_id = '06d68ee1ad254e349da3a8fd7d7ab1d3';
      	//$sub_id = '468f3ca037e4c637da49a44f82bbd963';
			}
		}


		if ($shop->isExcludedFromBilling())
		{
    	$plans_msg = "Your store is excluded from billing, you don't have any plan";
			//$plans = [];
			$plans_dp = null;
		}
		else
		{
			$plans_msg = '';

			$sub = $shop->subscription;
			$plan = $sub->plan;

			$q = \admin\models\Plan::find();
			$q->andWhere(['app_access' => 1]);

			if ($shop->host != 'prt04.myshopify.com')
			{
				$q->andWhere(['dev' => 0]);
			}
			//$q->andWhere(['NOT', ['plan_code' => $plan->plan_code]]);
			$q->orderBy(['stores_count_unlimited' => SORT_DESC, 'stores_count' => SORT_DESC]);
			$plans = $q->all();

      $r['current_plan_code'] = $plan->plan_code;

			//foreach($plans as $k=>$p)
			//{
      	//if ($p->plan_code == )
			//}

			$plans_dp = new ArrayDataProvider(
				[
					'allModels' => $plans,
					'pagination' => false,
					//'sort' => $sort,
				]);
		}

		if ($shop->isExcludedFromBilling())
		{
    	$invs = [];
    	$invs_msg = "Your store is excluded from billing, no invoices available";
		}
		else
		{
			$invs_msg = '';
			$invs = Yii::$app->recurlyApi->invoices($acc_id, $sub_id);
			//hr($invs); exit;
 		}

		if ($shop->isExcludedFromBilling())
		{
    	$upd_card_url = '';
    	$upd_card_msg = "Your store is excluded from billing, card is not using";
		}
		else
		{
			$upd_card_url = "https://ecomfuel.recurly.com/accounts/$acc_id/edit";
			$upd_card_msg = '';
		}


		$r['plans_dp'] = $plans_dp;
		$r['plans_msg'] = $plans_msg;

		$r['invs_dp'] = new ArrayDataProvider(['allModels' => $invs]);
		$r['invs_msg'] = $invs_msg;

		$r['upd_card_url'] = $upd_card_url;
		$r['upd_card_msg'] = $upd_card_msg;

		return $this->render("subscription-$this->entity_alias", $r);
	}

	// last_plan_downgrade_time

	public function actionPlanUpgrade()
	{
    $model = new \account\models\PlanChange;
		$model->shop = $this->shop;

		if ($model->load(Yii::$app->request->post()))
		{
    	if ($model->changePlan())
			{
				Yii::$app->session->addFlash('success', "Plan has been changed");
			}
			else
			{
        Yii::$app->session->addFlash('error', "Error on changing plan: $model->err");
			}

			return $this->refresh();
		}

		$shop = $this->shop;

		if ($shop->isPlanUnlimited())
		{
			$limit_max = 0;
			$plans = [];
			$dp = null;
		}
		else
		{
			$sub = $shop->subscription;
			$limit_max = $sub->plan->stores_count;

			$q = \admin\models\Plan::find();
			$q->andWhere(['app_access' => 1]);
			$q->andWhere(['dev' => 0]);
			$q->andWhere("stores_count > :lim OR stores_count_unlimited = 1", [':lim' => $limit_max]);
			$q->orderBy(['stores_count_unlimited' => SORT_DESC, 'stores_count' => SORT_DESC]);
			$plans = $q->all();


			$dp = new ActiveDataProvider(
				[
					'query' => $q,
					'pagination' => false,
					//'sort' => $sort,
				]);

    }

		$r = [];
		$r['shop'] = $shop;
		$r['plans'] = $plans;
		$r['dp'] = $dp;

		return $this->render("plan-upgrade-$this->entity_alias", $r);
	}

}


