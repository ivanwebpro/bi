<?php

namespace account\controllers;

use Yii;
use common\apis\TelegramApi;

use account\models\Account;
use yii\data\ActiveDataProvider;


use yii\data\Sort;
use yii\data\ArrayDataProvider;

use yii\base\UserException;

use account\models\Contact;
use account\models\AppActivateForm;
use account\models\StoreAddForm;
use account\models\Alert;
use account\models\User;
use account\models\Candle;
use account\models\LogAppUninstall;
use common\helpers\Url;
use common\helpers\DatesHelper;
use common\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\db\Expression;
use account\models\Shop;
use account\models\ShopifyAuthForm;
use account\models\LogAlert;
use yii\db\Query;
use admin\models\AppCfg;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use account\helpers\CronHelper;

class ProductController extends base\Controller
{
  public $defaultAction = 'index';



	public function actionIndex($store_id='all')
	{
		$shop = $this->shop;
  	$store_add_form = new StoreAddForm;
		$store_add_form->shop = $this->shop;
		$store_add_form->use_proxy = false;

		if ($store_add_form->load(Yii::$app->request->post()))
		{
    	if ($store_add_form->validate())
			{
				$store_add_form->saveNewStore();
				Yii::$app->session->addFlash('success', 'Store has been added');
      	return $this->redirect(['index']);
			}
      else
			{
      	if ($store_add_form->show_upgrade_link)
				{
        	Yii::$app->session->addFlash('warning', $store_add_form->show_upgrade_text);
					return $this->redirect(['index']);
				}
			}

		}

		$q = $this->shop->getProductsQuery();

		if ($store_id != 'all')
		{
			if ($store_id != 'saved')
			{
      	$q->andWhere(['store_id' => $store_id]);
			}
			else
			{
        $q->andWhere(['saved' => 1]);
			}
		}

		//


		if ($store_id != 'saved')
		{
			$q->andWhere(['NOT', ['store_id' => null]]);
   		$q->orderBy(['published_time_utc' => SORT_DESC]);
		}
		else
		{
      $q->orderBy(['saved_time' => SORT_DESC]);
		}

		$dp = new ActiveDataProvider(
			[
				'query' => $q,
				'pagination' => [
        	'pageSize' => 12,
				],
			]);

		$r = [];
    $r['dp'] = $dp;
    $r['store_add_form'] = $store_add_form;

		$r['store_id'] = $store_id;
		$stores = $shop->getStoresQuery()->all();
		$stores = ArrayHelper::map($stores, 'id', 'domain');
		asort($stores);
		$stores = ArrayHelper::prepend($stores, 'all', 'All');
		$stores = ArrayHelper::prepend($stores, 'saved', 'Saved');


		$r['stores'] = $stores;
		$r['shop'] = $shop;
		return $this->render("index-$this->entity_alias", $r);
	}

	public function actionSaved($store_id='all')
	{
		$shop = $this->shop;


		$q = $this->shop->getProductsQuery();
		$q->andWhere(['saved' => 1]);

		if ($store_id != 'all')
		{
    	$q->andWhere(['store_id' => $store_id]);
		}

		//


    $q->orderBy(['saved_time' => SORT_DESC]);

		$dp = new ActiveDataProvider(
			[
				'query' => $q,
				'pagination' => [
        	'pageSize' => 12,
				],
			]);

		$r = [];
    $r['dp'] = $dp;

		$r['store_id'] = $store_id;
		$stores = $shop->getStoresQuery()->all();
		$stores = ArrayHelper::map($stores, 'id', 'domain');
		asort($stores);
		$stores = ArrayHelper::prepend($stores, 'all', 'All');
		//$stores = ArrayHelper::prepend($stores, 'saved', 'Saved');


		$r['stores'] = $stores;
		$r['shop'] = $shop;
		return $this->render("saved-$this->entity_alias", $r);
	}

	public function actionView($id)
	{
		$product = $this->findModel($id);
		$shop = $this->shop;

		$q = $this->shop->getProductsQuery();
		$q->andWhere(['id' => $id]);

		$dp = new ActiveDataProvider(
			[
				'query' => $q,
				'pagination' => [
        	'pageSize' => 12,
				],
			]);

		$r = [];
    $r['dp'] = $dp;
		$r['shop'] = $shop;
		$r['product'] = $product;
		return $this->render("view-$this->entity_alias", $r);
	}

	/*public function actionSaved($store_domain='all')
	{
		$shop = $this->shop;

		$q = $this->shop->getProductsQuery();

		if ($store_domain != 'all')
		{
      $q->andWhere(['store_domain' => $store_domain]);
		}

		$q->andWhere(['saved' => 1]);

   	$q->orderBy(['saved_time' => SORT_DESC]);

		$dp = new ActiveDataProvider(
			[
				'query' => $q,
				'pagination' => [
        	'pageSize' => 12,
				],
			]);

		$r = [];
    $r['dp'] = $dp;

		$r['store_id'] = $store_domain;
		$stores = $shop->getStoresQuery()->all();
		$stores = ArrayHelper::map($stores, 'domain', 'domain');
		asort($stores);
		$stores = ArrayHelper::prepend($stores, 'all', 'All');


		$r['stores'] = $stores;
		$r['shop'] = $shop;
		return $this->render("index-saved-$this->entity_alias", $r);
	}     */


	public function actionDescription($id, $ajax=0)
	{
    $product = $this->findModel($id);

		$r = [];
		$r['product'] = $product;
 		if ($ajax)
		{
      return $this->renderAjax("description-$this->entity_alias", $r);
		}
		else
		{
			return $this->render("description-$this->entity_alias", $r);
		}


	}

	public function actionShopifyPush($id, $ajax=0, $force=0)
	{
		///if ($ajax) return "OK";

		$product = $this->findModel($id);

		$push_again_form_model = new \account\models\PushAgainForm;

		if ($product->pushed && !$force)
		{
			if (!$push_again_form_model->load(Yii::$app->request->post()))
			{
				$r = [];
				$r['push_again_form_model'] = $push_again_form_model;
				$r['show_push_again_form'] = true;
				$r['product'] = $product;
		   	if ($ajax)
				{
		      return $this->renderAjax("shopify-push-$this->entity_alias", $r);
				}
				else
				{
					return $this->render("shopify-push-$this->entity_alias", $r);
				}
			}
		}

		$shop = $this->shop;
    $err_msg = '';
    $msg = '';

		$li = $product->shopifyPush($shop);

		if ($li->success)
		{
			$u = $li->shopifyAdminUrl;
			$a = Html::a($u, $u, ['target' => '_blank']);
	    $msg = "Product has been added to your Shopify store. You can edit it here: $a";
    }
		else
		{
			$err_msg = $li->err_msg;
		}


		$r = [];
		$r['show_push_again_form'] = false;
		$r['product'] = $product;
		$r['msg'] = $msg;
		$r['err_msg'] = $err_msg;
    $r['ajax'] = $ajax;

		//hre($r);
   	if ($ajax)
		{
      return $this->renderAjax("shopify-push-$this->entity_alias", $r);
		}
		else
		{
			return $this->render("shopify-push-$this->entity_alias", $r);
		}

	}


	public $entity_name = 'Product';
	public $entity_name_pl = 'Products';
	public $entity_alias = 'p';
	public $entity_class = "admin\\models\\Product";

	protected function findModel($id)
	{
		$ec = $this->entity_class;

		if ($model = $this->shop->getProductById($id))
		{
			return $model;
		}
		else
		{
			throw new NotFoundHttpException("The requested $this->entity_name does not exist");
		}
  }


	public function actionSavedInclude($id, $ajax=0)
	{
  	$product = $this->findModel($id);
		$product->saved = 1;
		$product->saved_time = new Expression("NOW()");
    $product->saveEx();


		if (!$product->store_domain)
		{
			$product->store_domain = $product->store->domain;
			$product->saveEx();
		}


		$r = [];
		$r['product'] = $product;
		$view = "include-saved-$this->entity_alias";
 		if ($ajax)
		{
      return $this->renderAjax($view, $r);
		}
		else
		{
			return $this->render($view, $r);
		}
	}

	public function actionSavedExclude($id, $ajax=0)
	{
  	$product = $this->findModel($id);
		$product->saved = 0;
		$product->saveEx();

		Yii::$app->session->addFlash("Product `$product->title` has been excluded from saved");
		return $this->redirect(['saved']);
	}

	public function actionSavedToggle($id, $ajax=0, $store_id='all')
	{
  	$product = $this->findModel($id);

		if ($product->saved)
		{
			$product->saved = 0;
		}
		else
		{
			$product->saved = 1;
			$product->saved_time = new Expression("NOW()");

			if (!$product->store_domain)
			{
				$product->store_domain = $product->store->domain;
			}
		}

		$product->saveEx();

		if ($ajax)
		{
    	return $product->saveBtn();
		}


		if ($product->saved)
		{
      Yii::$app->session->addFlash("Product `$product->title` has been saved");
		}
		else
		{
			Yii::$app->session->addFlash("Product `$product->title` has been excluded from saved");
		}

		return $this->redirect(['index', 'store_id' => $store_id]);
	}
}


/*
$r = [];
		$r['product'] = $product;
		$view = "saved-exclude-$this->entity_alias";
 		if ($ajax)
		{
      return $this->renderAjax($view, $r);
		}
		else
		{
			return $this->render($view, $r);
		}
*/