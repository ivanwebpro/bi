<?php

namespace account\controllers;

use Yii;
use yii\data\ArrayDataProvider;
use yii\base\UserException;
use account\models\AuthRequest;
use account\models\Contact;
use account\models\AppActivateForm;
use account\models\LogAppUninstall;
use common\helpers\Url;
use yii\helpers\Html;
use yii\db\Expression;
use account\models\Shop;
use account\models\ShopifyAuthForm;
use yii\db\Query;
use admin\models\AppCfg;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use common\helpers\ArrayHelper;

class ShopController extends base\Controller
{
  public $defaultAction = 'index';

	public function actionUpdate()
	{
    $model = $this->shop;

		if ($model->load(Yii::$app->request->post()))
		{
			$prev_email = $model->notify_email_enabled;
			$prev_desktop = $model->notify_desktop_enabled;

    	if ($model->save())
			{
				if ($model->notify_email_enabled && (!$prev_email || !$model->notify_email_enabled_time))
				{
        	$model->notify_email_enabled_time = new Expression("NOW()");
          $model->saveEx();
				}

				if ($model->notify_desktop_enabled && (!$prev_desktop || !$model->notify_desktop_enabled_time))
				{
        	$model->notify_desktop_enabled_time = new Expression("NOW()");
          $model->saveEx();
				}

				Yii::$app->session->addFlash('success', 'Data saved');
    		return $this->refresh();
			}
		}

		$r = [];
		$r['model'] = $model;
		return $this->render('update-shop', $r);

	}


}
