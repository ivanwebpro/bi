<?php

namespace account\controllers\base;

use Yii;
use yii\web\Controller as YiiController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\helpers\Url;
use common\models\logs\PageLog;
use admin\models\AppCfg;
use account\models\Shop;
use common\apis\ShopifyApi;

class Controller extends YiiController
{
	//header('P3P: CP="Your P3P policy here"');

	public $layout = 'account_layout';
	public $display_top_menu = true;

	public function redirectNoPassParams($url, $statusCode = 302)
	{
    return parent::redirect($url, $statusCode);
	}

	public function handleChargingCheck0($shop)
	{
		//hre(Yii::$app->requestedRoute);

		$app_cfg = Yii::$app->appCfg;
		$ch = $app_cfg->isShouldBeCharged($shop);

		//hr('$ch');
		//hre($ch);

		if ($this->shop->isVerbose())
		{
			Yii::warning("ch: $ch", __METHOD__);
		}

		if ($ch)
		{
			$shop->charging_enabled = 0;
			$shop->saveWithException();

			$ret_url = Url::absolute(["app/charges-activated", 'shop' => $shop->host]);
			$confirm_url = $app_cfg->urlForConfirmCharging($shop, $ret_url);
			$app = Yii::$app;
      Yii::$app->session->set($app::SESSION_VAR_CHARGE, $shop->host);
      //return $this->redirect($confirm_url);

			Yii::$app->response->redirect($confirm_url);
			Yii::$app->end();
		}

	}


	public function handleChargingCheck($shop)
	{
		if ($shop->exclude_from_billing)
		{
			return true;
		}

		$ex = \admin\models\Setting::isExcludedFromBilling($shop->host);
		if ($ex)
		{
    	return true;
		}

		$sub = \admin\models\Subscription::getActiveForShopifyName($shop->shopifySubDomain);

		if (!$sub)
		{
	  	$msg = "Can't find active subscription for store `$shop->host`, please activate";
			$u = ['activate/index', 'msg' => $msg];
			Yii::$app->response->redirect($u);
			Yii::$app->end();
		}

		if (!$sub->plan->app_access)
		{
	  	$msg = "You have active subscription on plan `$sub->plan_name`, but this plan does not allow to access to app";
			$u = ['activate/index', 'msg' => $msg];
			Yii::$app->response->redirect($u);
			Yii::$app->end();
		}
	}

 	public function getShop()
	{
  	return Yii::$app->shop;
	}

 	public function behaviors()
  {
      return [
          'access' => [
              'class' => AccessControl::className(),
              'rules' => [
                	// allow authenticated users
                	[
                    'allow' => true,
                    'roles' => ['@'],
                	],
              ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
      ];
  }



	public function redirect($url, $statusCode = 302)
	{
		//hre(__METHOD__);
		/*if (isset($_COOKIE['XDEBUG_SESSION']))

		//if (isLocalHost())
		{
			$url_s = Url::to($url);
			$msg = "<A href='$url_s'>Manual Redirect</A>";
			return $this->render('@account/views/app/cron', ['msg' => $msg]);
		} */

		$url = Url::passParams($url);
    return parent::redirect($url, $statusCode);
	}




	public function beforeAction($action)
	{
		if ($this->id != 'cron')
		{
			PageLog::log();
		}

		//hre($action);
		if (!parent::beforeAction($action))
		{
			return false;
		}

		if ($this->shop)
		{
			//if ($action->id != 'charges-activated'
				//	&& $action->id != 'shopify-connect'
					//)
			//{
				//hre($this->shop);
    		$this->handleChargingCheck($this->shop);
			//}
		}


		$hrs = Yii::$app->response->getHeaders();
		$hrs->set('P3P', 'CP="Your P3P policy here"');

		return true;

	}

}