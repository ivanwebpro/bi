<?php

namespace account\controllers;

use Yii;
use common\helpers\Url;
use yii\helpers\Html;
use common\models\auth\User;
use account\models\Shop;
use account\models\Order;
use account\models\LogShopifyApi;
use account\models\LogSync;
use account\models\LogStockxApi;
use account\helpers\CronHelper;
use yii\db\Expression;
use yii\web\Response;
use common\helpers\ArrayHelper;
use yii\base\ModelEvent;

//localhost/a/cron/run
class CronController extends base\Controller
{
	public $layout = '@account/views/layouts/cron_layout';
  public $defaultAction = 'run';

 	public function returnGif()
	{
  	// return just gif
		//header('Content-Type: image/gif');
		$response = Yii::$app->getResponse();
		$response->headers->set('Content-Type', 'image/gif');
		$response->format = Response::FORMAT_RAW;
		//				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return base64_decode('R0lGODlhAQABAJAAAP8AAAAAACH5BAUQAAAA'.
				'LAAAAAABAAEAAAICBAEAOw==');
	}

	public function actionSummary()
	{
		$q = Shop::find()
     	->andWhere("token IS NOT NULL AND token <> '' ")
     	->andWhere(['uninstalled' => 0])
     	->orderBy("last_cron_time_start ASC")
			;

	}

	public function actionProductsLoading($shop='', $gif = 0)
	{
		$ex = [];
		$ex['behaviors'] = ['product'];
		return $this->actionRun($auto_reload = false, $gif, $shop);
	}

	public function actionRun($auto_reload = false, $gif=0, $shop_host = '', $shop_id = '', $manual = '', $ex = [])
	{
		$op = Yii::$app->request->get('op', '');
		$cron = new CronHelper(__METHOD__."-op-$op");
		$r = mt_rand(1,100)/100;
		usleep(1000*1000*$r);
		//Yii::warning("sleep $r sec");

		if ($shop_id)
		{
    	$cron = new CronHelper(__METHOD__."_$shop_id");
		}

		if ($manual)
		{
    	$cron = new CronHelper(__METHOD__."_manual");
		}

		if (true)
		{
			if ($gif)
			{
				if (!$cron->tryLock($exit = false))
				{
	    		$this->returnGif();
				}
			}
			else
			{
				$cron->tryLock();
			}
		}

    Yii::$app->cron = $cron;

		try
		{
			$ps = [];
			$ps['shop_host'] = $shop_host;
			$ps['shop_id'] = $shop_id;
			$ps['ex'] = $ex;
  		$this->runCron($ps);

			if ($gif)
			{
				$this->returnGif();
			}

			if ($auto_reload)
			{
				?>
					<h1>Wait reloading &hellip; (30 seconds)</h1>
					<script>
						setTimeout(function() { location.reload();}, 30*1000);
					</script>
				<?php
			}

			$cron = Yii::$app->cron;

			//hre($cron);
			$cron->cleanLock();
			$cron->addMsg("Cron is finished");

			$m = '';
			$m .= "<table class='table bordered striped' style='margin: 30px 10px;'>";
			foreach($cron->msgs as $mx)
			{
				$m .= "<tr>";
				$m .= "<td>$mx[time]</td>";
				$m .= "<td>$mx[msg]</td>";
				$m .= "</tr>";
			}
			$m .= "</table>";

			if (!isDebug())
			{
				//$m = '';
			}

			return $this->render('cron', ['msg' => $m]);
		}
		catch(\Exception $e)
		{
			//Yii::warning($e->message, $cat);
			//Yii::warning($e->getTraceAsString(), $cat);

			$cron->cleanLock();
			throw $e;
		}
	}

	public function runCron($ps)
	{
		//return;
    $shop_host = '';
		if (!empty($ps['shop_host']))
		{
			$shop_host = $ps['shop_host'];
		}
    $shop_id = '';
		if (!empty($ps['shop_id']))
		{
			$shop_id = $ps['shop_id'];
		}


		if (empty($_GET['op']))
		{
			LogShopifyApi::deleteAll("time < DATE_SUB(NOW(), INTERVAL 1 DAY)");

			$rnd = mt_rand(1,100);

			Yii::$app->cron->addMsg("rnd: $rnd");

			if ($rnd < 20)
			{
				Yii::$app->cron->addMsg("Recurly data refresh start");

				\admin\models\Subscription::refreshApiSafe();
				//\admin\models\Plan::refreshApiSafe();
      	Yii::$app->cron->addMsg("Recurly data has been refreshed");
			}
			else
			{
      	Yii::$app->cron->addMsg("Recurly data refresh skip");
			}

		}



		if ($shop_host)
		{
			$q = Shop::find()->andWhere(['host' => $shop_host]);
		}
		else if ($shop_id)
		{
			$q = Shop::find()->andWhere(['id' => $shop_id]);
		}
		else
		{
			$q = Shop::find()
      	->andWhere("token IS NOT NULL AND token <> '' ")
      	->andWhere(['uninstalled' => 0])
      	->orderBy("last_cron_time_start ASC")
				;
    }

		// LU
		$last_cron_time_start = $q->min('last_cron_time_start');
		$last_cron_time_end = $q->min('last_cron_time_end');

		Yii::$app->cron->addMsg("last_cron_time_start: $last_cron_time_start, last_cron_time_end: $last_cron_time_end");

		$shops = $q->all();
		shuffle($shops);

		//hre($shops);

		Yii::$app->cron->addMsg("Shops: <pre>".json_encode(ArrayHelper::map($shops, 'id', 'host'), JSON_PRETTY_PRINT)."</pre>");


		foreach($shops as $shop)
		{
			if (!Yii::$app->cron->canRun())
			{
				Yii::$app->cron->addMsg("Time expired, exit cron");
				break;
			}


			if ($shop->uninstalled && !$shop->uninstalled_data_deleted)
			{
				//$shop->cachedDataClean();
				$shop->charging_activated_response = '';
				$shop->uninstalled_data_deleted = 1;
				$shop->uninstalled_data_deleted_time = new Expression("NOW()");
				$shop->saveWithException();
			}



			if ($shop->uninstalled)
				continue;

			if ($shop->api_fails_count > 5)
				continue;

			Yii::$app->cron->addMsg("<b style='color: darkblue'>Start processing shop $shop->host</b>");

			try
			{
				$shop->last_cron_time_start = new Expression("NOW()");
				$shop->saveWithException();

      	$event = new ModelEvent;

				if (!empty($ex['behaviors']))
				{
        	$event->data = [];
        	$event->data['behaviors'] = $ex['behaviors'];
				}

    		$shop->trigger(CRON_EVENT, $event);
				$shop->api_fails_count = 0;

				$shop->last_cron_time_end = new Expression("NOW()");
				$shop->saveWithException();

			}
		catch(\GuzzleHttp\Exception\ConnectException $e)
			{
      	$err_msg = $e->getMessage();
  			$timeout_msg = "cURL error 28: Operation timed out after";
				$host_msg = "cURL error 6: Could not resolve host";

        if (stristr($err_msg, $timeout_msg))
				{
					Yii::$app->cron->addMsg("$shop->host error: $err_msg");
				}
        else if (stristr($err_msg, $host_msg))
				{
          Yii::$app->cron->addMsg("$shop->host error: $err_msg");
				}
				else
				{
        	throw $e;
				}

			}
			catch(\GuzzleHttp\Exception\ServerException $e)
			{
				// server error - just skip, wait next attempt
				$m = $e->getMessage();
        Yii::$app->cron->addMsg("$shop->host error: $m");
			}
			catch(\GuzzleHttp\Exception\ClientException $e)
			{
				$m = $e->getMessage();
				Yii::$app->cron->addMsg("$shop->host error: $m");
				//Yii::warning($m, __METHOD__);

				//$m = $e->getMessage();
				//hr($m);
				$rs = $e->getResponse();

				switch($rs->getStatusCode())
				{
        	case 401:
						// 401 Unauthorized
					case 403:
						// 403 Forbidden
					case 420:


	        	$shop->api_fails_count++;
						$shop->saveEx();
						break;



        	case 400:
						//Error 400 (Bad Request)

					case 402:
						// 402 Payment Required

					case 404:
						// 404 Not Found
						// skip, just wait

						// ? - add pause before next requests

						if (!empty($_GET['throw']))
						{
            	throw $e;
						}
						break;

					default:
						// add extra data
            $stx = "Error: ".$rs->getStatusCode()." ".$rs->getReasonPhrase();
						Yii::warning($stx, __METHOD__);

						Yii::warning($m, __METHOD__); // log first, if next req will fail
						$shop_data = $shop->shopifyApi->shopGet();
						$shop_data = json_encode($shop_data, JSON_PRETTY_PRINT);
						Yii::warning($shop_data, __METHOD__);
						throw $e;
				}
			}
		}
	}


	public function behaviors()
	{
  	$bs = parent::behaviors();

		$r = [
    	'allow' => true,
			'actions' => ['run', 'products-loading'],
      ];

		$bs['access']['rules'][] = $r;

		return $bs;
	}

}
