<?php

namespace account\components;

use common\components\Application as CommonApplication;
use common\helpers\Url;
use account\models\Shop;
use common\models\Order;
use admin\models\AppCfg;
use Yii;

class Application extends CommonApplication
{

	public $shopify_scope;
	public $redirect_after_login;

	public $debug = false;
	public $cron = 0;
	public $guid = '';
	public $shopify_proxy_slug = '';
	public $shopify_params = [];

	public function redirectAfterLogin()
	{
		$u = $this->redirect_after_login;

		$u = Yii::$app->user->getReturnUrl($def=$u);


		Yii::$app->response->redirect($u);
		Yii::$app->end();
	}

	public function getShop()
	{
  	$shop = Yii::$app->user->identity;

		if ($shop && $shop->app_admin && !empty($_GET['shop_j8k']))
		{
    	$shop_x = Shop::findOne($_GET['shop_j8k']);

			if ($shop_x)
			{
      	$shop = $shop_x;
			}


		}
		return $shop;
	}

}