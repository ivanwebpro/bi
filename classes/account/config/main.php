<?php

$fs = [
    ('/../../common/config/params.php'),
    ('/../../common/config/params-local.php'),
    ('/params.php'),
    ('/params-local.php')
];

$params = [];

foreach($fs as $f)
{
	$ff = __DIR__.$f;

	if (file_exists($ff))
	{
		$params = yii\helpers\ArrayHelper::merge($params, require $ff);
	}
}


$app_id = 'bi';

$config = [
    'id' => $app_id,
    'basePath' => appRoot().'/classes/account',
    'bootstrap' => ['log'],
		'defaultRoute' => 'app',
    'controllerNamespace' => 'account\controllers',
    'components' => [
			'assetManager' => [
				//'class' => 'yii\web\AssetManager',
				//'basePath' => '@app_root/account/assets',
				//'baseUrl' => '@web/account/assets',
			],

     'user' => [
        'identityClass' => 'account\models\Shop',
        'enableAutoLogin' => false,
				'enableSession' => true,
				'loginUrl' => ['app/login'],
      ],
    	'request' => [
				'enableCsrfCookie' => true,
				'cookieValidationKey' => cookieValiationKey(__DIR__),
				'csrfParam' => $app_id.'_CSRF',
			],
    	'session' => [
        'name' => $app_id.'_SESS_ID',
      ],
    	'errorHandler' => [
    		'errorAction' => 'app/error',
    	],
		],

  'params' => $params,
];


return $config;