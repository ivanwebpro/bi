<?php

namespace account\helpers;
use Yii;
use account\models\CronLog;
use account\models\CronLock;
use yii\db\Expression;


class CronHelper
{

	public $msgs = [];
	public $timeStart = 0;
	public $log_tbl = 'cron_log';
	public $lock_tbl = 'cron_lock';
	public $log_id;
  public $timeStartAr = array();
	public $maxExecTime = 25;
	public $cron_name = '';

	public function addMsg($msg, $lim = true)
	{
		if (!is_scalar($msg))
		{
			ob_start();
			var_dump($msg, true);
			$d = ob_get_clean();

			if ($lim)
			{
				$d = substr($d, 0, 700);
			}
    	$msg = "<pre>".$d."</pre>";
		}

  	$this->msgs[] = ['time' => date('c'), 'msg' => $msg];
	}

	public function __construct($cron_name)
	{
		$this->cron_name = $cron_name;
		CronLog::deleteAll("time_start < DATE_SUB(NOW(), INTERVAL 3 DAY)");
	}


	public function tryLock($exit = true)
	{
		if ($this->isLock())
		{
			if ($exit)
			{
				hr(date('c').": cron parrallel run protection!");
				exit;
			}
			else
			{
				return false;
			}
		}

		$this->addMsg("Cron job $this->cron_name locked OK");
		$this->setLock();
		return true;
  }

	public function verifyStop()
	{
  	if (!$this->canRun())
		{
			$this->cleanLock();
			hr("Cron Timeout, exit");
			exit;
		}
	}

	public function timeUsed()
	{
  	return microtime(true) - $this->timeStart;
	}

	public function canRun()
	{
  	if ($this->timeUsed() > $this->maxExecTime)
			return false;
		else
			return true;
	}

	public function setLock()
	{
		$this->timeStart = microtime(true);
		$this->changeLock(1);

    $log = new CronLog();
		$log->time_start = new Expression('NOW()');
		$log->name = $this->cron_name;
		$log->saveWithException();
		$this->log = $log;
	}


	public function cleanLock()
	{
		$this->changeLock(0);
		$this->log->time_end = new Expression('NOW()');
		$this->log->delta = microtime(true) - $this->timeStart;
		$this->log->saveWithException();
		$this->addMsg("Cron job $this->cron_name lock cleaned OK");
	}

	public function resetLock()
	{
    $data = array();
		$data['lock'] = 0;
		$data['time_end'] = new Expression('NOW()');
		$data['delta'] = -1;


    $dt = "DATE_SUB(NOW(), INTERVAL 5 MINUTE)";
		$conditions = "time_start < $dt";
		//$params = array(':late_time_start' => new CDbExpression($dt));

		Yii::$app->db
			->createCommand()
			->update(CronLock::tableName(), $data, $conditions)
			->execute();
	}


	public function isLock()
	{
    $this->resetLock();

		if (!empty($_GET['unlock']))
			return false;

    $lo = $this->getLockObj();
		return (!$lo || $lo->lock > 0);
	}

	protected function getLockObj()
	{
		$lock = CronLock::find()
				->andWhere(['name' => $this->cron_name])
				->one();

		if (!$lock)
		{
			$lock = new CronLock();
			$lock->name = $this->cron_name;
			$lock->lock = 0;
			$lock->saveWithException();
		}

		return $lock;
	}

	public function changeLock($lock)
	{
		$lo = $this->getLockObj();
		$lo->lock = $lock;

		if (1 == $lock)
			$lo->time_start = new Expression('NOW()');
		else
		{
			$lo->time_end = new Expression('NOW()');
			$lo->delta = microtime(true) - $this->timeStart;
		}

		$lo->saveWithException();
	}

	public function msgs()
	{
		$this->addMsg("Part of job is finished");

		$m = '';
		$m .= "<table class='table bordered striped' style='margin: 30px 10px;'>";
		foreach($this->msgs as $mx)
		{
			$m .= "<tr>";
			$m .= "<td>$mx[time]</td>";
			$m .= "<td>$mx[msg]</td>";
			$m .= "</tr>";
		}
		$m .= "</table>";

		return $m;

	}

}
