<?php

namespace account\widgets\base;
use common\widgets\base\Widget as WidgetCommon;

class Widget extends WidgetCommon
{
	public function getWidgetFolderFile()
	{
		$r = dirname(__DIR__)."/views/".$this->getWidgetViewsFolderName();
		//hr($r);
		return $r;

  	//return dirname(__DIR__)."/views/".basename(get_called_class());
	}

	public function getWidgetFolderWeb()
	{
		//return Yii::getAlias($css_alias);
		$dr = $_SERVER['DOCUMENT_ROOT'];
		$dx = $this->getWidgetFolderFile();
		$dr = str_ireplace('\\', '/', $dr);
		$dx = str_ireplace('\\', '/', $dx);
		return str_ireplace($dr, '', $dx);
	}
}