<?php

namespace account\widgets;

use yii\bootstrap\Tabs as BsTabs;

class Tabs extends BsTabs
{
	public function init()
	{
  	parent::init();

		foreach($this->items as $k => $v)
		{
			$c = $v['content'];
			if ($c instanceof Closure)
			{
      	$this->items[$k]['content'] = call_user_func($c);
      }
		}
	}
}