<?php

namespace account\models;

use Yii;
use account\models\candles\CandleD1;
use common\helpers\StrHelper;

class Proxy extends  \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return '{{%proxy}}';
	}

	public static function random()
	{
  	$p = self::find()
			->andWhere('error_count < 2')
			->orderBy("RAND() ASC")->limit(1)->one();
		return $p;
	}

	public static function addNewGetProxyList()
	{
		$u = "https://api.getproxylist.com/proxy?anonymity[]=high%20anonymity&allowsHttps=true";
		$d = file_get_contents($u);
		$a = json_decode($d, true);

		$p = new Proxy;
		$p->source = $u;
		$p->ip = $a['ip'];
		$p->port = $a['port'];
		$p->protocol = $a['protocol'];
		$p->saveEx();

	}

	public static function addNewGimmeProxy()
	{
		$u = "https://gimmeproxy.com/api/getProxy?anonymityLevel=1&supportsHttps=true&get=true";
		$d = file_get_contents($u);
		$a = json_decode($d, true);

		$p = new Proxy;
		$p->source = $u;
		$p->ip = $a['ip'];
		$p->port = $a['port'];
		$p->protocol = $a['protocol'];
		$p->saveEx();

	}

	public static $added = false;

	public static function log($m)
	{
  	if (Yii::$app->cron)
		{
    	Yii::$app->cron->addmsg($m);
		}
	}

	public static function addNew()
	{
		if (self::$added)
		{
    	return;
		}

		$ck = [__METHOD__];
		$delta = 1200;

		if (Yii::$app->cache->get($ck))
		{
      self::log("Last proxy has beed added in less $delta seconds");
    	return;
		}

		try
		{
			Yii::$app->cache->set($ck, true, $delta);

			//Yii::warning("addNewGimmeProxy - START", __METHOD__);
			self::addNewGimmeProxy();
			//Yii::warning("addNewGimmeProxy - END", __METHOD__);
			self::$added = true;
		}
		catch(\Exception $e)
		{
    	Yii::warning($e->getMessage(), 'data');
		}
	}

	public function behaviors()
	{
		return [
			'ExceptionOnSave' => [
				'class' => 'common\behaviors\ExceptionOnSave',
			],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
				'class' => 'common\behaviors\DateTimeOnCreate',
			],
			'DateTimeOnCreate' => [
				'attr' => 'updated_time',
				'class' => 'common\behaviors\DateTimeOnCreate',
			],
		];
	}

	public function loadUrlByProxy($url, $p)
	{

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);

		if ($p)
		{
			curl_setopt($ch, CURLOPT_PROXY, "$p->protocol://$p->ip:$p->port");
		}

		//curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15); // seconds

		$t0 = microtime(true);
		$r = curl_exec($ch);
		$this->proxy_delta = microtime(true) - $t0;

		$curl_err = curl_error($ch);
		if ($curl_err)
		{
			throw new \Exception( "CURL error: $curl_err" );
		}
		$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE );
    if ($http_status >=400)
		{
      throw new \Exception( "CURL HTTP STATUS: $http_status");
		}

		curl_close($ch);

		if (empty($r))
		{
			throw new \Exception("Blank response received");
		}

		return $r;


		$aContext = array(
    'http' => array(
        'proxy' => $this->getProxy(), //'tcp://192.168.0.2:3128',
        'request_fulluri' => true,
    ),
		);

		hr($aContext);



		$cxContext = stream_context_create($aContext);

		return file_get_contents($url, false, $cxContext);
	}

public function loadUrlByProxySafe($url)
	{
		Proxy::addNew();
		$p = Proxy::random();
		//$p = null;

		if (!$p)
		{
			$e_msg = "No proxy, direct request";
    	Yii::warning($e_msg, __METHOD__);
			Yii::$app->cron->addMsg($e_msg);
		}
		else
		{
			$p->try_time = new Expression("NOW()");
			$p->saveEx();
		}

		try
		{
			$r = $this->loadUrlByProxy($url, $p);

			if ($p)
			{
				$p->used_time = new Expression("NOW()");
				$p->saveEx();
			}

			return $r;
		}
		catch(\Exception $e)
		{

			if (!empty($_GET['t']))
			{
      	throw $e;
			}


			if ($p)
			{
				$p->error_time = new Expression("NOW()");
				$p->error_msg = get_class($e).": ".$e->getMessage();
				$p->saveEx();
			}

			Yii::$app->cron->addMsg($p->error_msg);

			if ($p)
			{
				$p->error_count++;
				$p->saveEx();
			}

  		throw $e;
		}
	}
}
