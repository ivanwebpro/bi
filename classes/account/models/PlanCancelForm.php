<?php

namespace account\models;

use Yii;
use yii\base\Model;
use common\helpers\HttpHelper;
use common\helpers\Url;
use common\helpers\Html;

class PlanCancelForm extends Model
{
	public $shop;
	public $run;
	public $err = '';


  public function cancel()
	{
		$sub_id = $this->shop->subscription->recurly_id;

		$api = Yii::$app->recurlyApi;
		$this->err = '';

		if ($api->cancelSafe($sub_id))
		{
			\admin\models\Subscription::refreshApiSafe();
    	return true;
		}

		$this->err = $api->err;
		return false;
	}

  public function rules()
  {
		$rs = [];
		$rs[] = [['run'], 'number'];
		$rs[] = [['run'], 'required'];
		return $rs;
	}
}
