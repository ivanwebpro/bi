<?php

namespace account\models;

use Yii;
use yii\db\Expression;
use yii\db\Query;
use common\helpers\Url;
use common\helpers\ArrayHelper;
use common\traits\St;

/*
shop_id	int(11)
created_time	datetime NULL
old_host	varchar(255)
new_shopify_name

*/
class ShopifySwitch extends \yii\db\ActiveRecord
{
 	use St;

	public static function tableName()
	{
		return '{{%shopify_switch}}';
	}


	public function getShop()
  {
  	return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
  }

	public function rules()
	{
  	$rs = [];
		$rs[] = ['new_shopify_name', 'string'];
		$rs[] = ['new_shopify_name', 'required'];
		$rs[] = ['new_shopify_name', 'checkNewShopiyfName'];
		return $rs;
	}

	public function checkNewShopiyfName($a)
	{
		$sn = $this->new_shopify_name;
		$h = "$sn.myshopify.com";

		if ($this->shop->host == $h)
		{
    	$this->addError($a, "You are already using this store `$h`");
		}
	}

  public function attributeHints()
	{
		$ahs = parent::attributeHints();
		$ahs['new_shopify_name'] = "Enter part before <i>.myshopify.com</i>";
		return $ahs;
	}

  public function attributeLabels()
	{
		$als = parent::attributeLabels();
		$als['new_shopify_name'] = "Shopify subdomain of another store";
		return $als;
	}


	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
			],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
    ];
	}

	public $err = '';

	public function run()
	{
  	$transaction = Yii::$app->db->beginTransaction();

		try
		{
			if ($this->_run())
			{
    		$transaction->commit();
				return true;
			}
			else
			{
      	return false;
			}
		}
		catch(Exception $e)
		{
			$transaction->rollBack();

			if (isDebug())
			{
      	throw $e;
			}

			$e_cl = get_class($e);
			$e_msg = "$e_cl: ".$e->getMessage();
			$this->err = $e_msg;

			return false;
		}
	}

	public function _run()
	{
		//hr("Switching logics is not fully implemented");
		//exit;

  	// check new shop exists
		$sn = $this->new_shopify_name;
		$h = "$sn.myshopify.com";

		$old_shop = $this->shop;
		$new_shop = Shop::findOne(['host' => $h]);

		if (!$new_shop)
		{
			$this->old_shop_token = $old_shop->token;
			$this->old_shop_host = $old_shop->host;
			$this->new_shop_host = $h;
			$this->saveEx();

			$old_shop->host = $h;
			$old_shop->token = null;
			$old_shop->saveEx();
		}

		$sub = $old_shop->subscription;

		if ($sub)
		{
			$sub->shopify_name = $old_shop->shopifySubDomain;
			$sub->saveEx();
		}

		Yii::$app->user->logout();
		return true;
	}
}
