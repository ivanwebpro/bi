<?php

namespace account\models;

use Yii;

use common\helpers\ArrayHelper;
use common\helpers\StrHelper;
use common\helpers\Url;
use yii\db\Expression;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;
use common\traits\St;

use yii\imagine\Image as ImagineImage;
use Imagine\Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use Imagine\Image\ManipulatorInterface;



class Image extends  \yii\db\ActiveRecord
{
  use St;

  public static function logError($e)
	{
		Yii::warning("can't load $url ".get_class($e).": ".$e->getMessage(), __METHOD__);
		return false;
	}

  public static function addSafe($url)
	{
		try
		{
			return self::add($url);
		}
		catch(\GuzzleHttp\Exception\RequestException $e)
		{
			return self::logError($e);
		}
		catch(\GuzzleHttp\Exception\ConnectException $e)
		{
			return self::logError($e);
		}
		catch(\GuzzleHttp\Exception\ServerException $e)
		{
       return self::logError($e);
		}
		catch(\GuzzleHttp\Exception\ClientException $e)
		{
			return self::logError($e);
		}
	}

  public static function add($url)
	{
		if ($img = self::findOne(['url' => $url]))
		{
    	return $img;
		}

		$client = new GuzzleClient([
    	'base_uri' => $url,
    	'timeout'  => 10.0,
			//'handler' => $stack,
		]);

		$hs = [];
		//$hs['Content-Type'] = "application/json";
		$rqx = [];
    $rqx['headers'] = $hs;

		$response = $client->request($method='get', $url_x='', $rqx);
		$b = $response->getBody();
    $b = (string)$b;

		$filename_from_url = parse_url($url);
		$ext = pathinfo($filename_from_url['path'], PATHINFO_EXTENSION);

		$file_name = md5($url).".$ext";

		$file_path = self::getFilePath($file_name);
		file_put_contents($file_path, $b);
		$fi = new static;
		$fi->url = $url;
		$fi->file = $file_name;
		$fi->saveEx();
		return $fi;
	}

	public static function getFilePath($file_name)
	{
		$file_path = appRoot()."/cache-image/$file_name";
		return $file_path;
	}

	public function getLocalUrl()
	{
		$file_path = $this->getFilePath($this->file);
		return Url::filePathToWebPath($file_path);
	}

	public static function tableName()
	{
		return '{{%image}}';
	}




	public function rules()
	{
  	$rs = [];
		return $rs;
	}

	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
	    ],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
			'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],
			'ReplaceBy' => [
				'attrs' => ['url'],
				'class' => 'common\behaviors\ReplaceBy',
			],
    ];
	}

	public function getImageUrlScaled()
	{
		$ps = [];
		$ps['v'] = 1;
		$ps['path'] = $this->getFilePath($this->file);
    //$ps['width'] = 200;
    $ps['height'] = 200;


		$src_image = $ps['path'];

    $size = getimagesize($src_image);

		if (!is_array($size) || empty($size[1]))
		{
			//Yii::warning("Can't get imagesize data from $src_image");
			return '';
		}

		if (0 == $size[1])
		{
    	$size[1] = 1;
		}

		$ratio = $size[0]/$size[1]; // width/height

		if (empty($ps['width']))
		{
			$ps['width'] = round($ps['height'] * $ratio);
		}

		if (empty($ps['height']))
		{
			if (0 == $ratio)
			{
         	$ratio = 1;
			}

			$ps['height'] = round($ps['width'] / $ratio);
		}


		$ext = pathinfo($src_image, PATHINFO_EXTENSION);
    $scaled_image_basename = md5(serialize($ps)).".".$ext;
		//$scaled_images_dir_alias = "@store/runtime/image-resize-cache";
		$scaled_images_dir = appRoot()."/cache-image-thumb";
		//FileHelper::createDirectory($scaled_images_dir);
		$scaled_image = $scaled_images_dir."/".$scaled_image_basename;

		if (file_exists($scaled_image))
		{
    	return Url::filePathToWebPath($scaled_image);
		}

		try
		{
			$img = ImagineImage::thumbnail(
						$src_image, $ps['width'], $ps['height'],
							ManipulatorInterface::THUMBNAIL_INSET);
			$img->save($scaled_image, ['quality' => 90]);
    	return Url::filePathToWebPath($scaled_image);
		}
		catch(\Expression $e)
		{
			Yii::warning($e->getMessage());
      return '';
		}

		//Image::getImagine()->open()->thumbnail(new Box($ps['width'], $ps['height']))->save($scaled_image , ['quality' => 90]);
    //hr($scaled_image);
		//hr("Scaled_image: $scaled_image");


	}
}