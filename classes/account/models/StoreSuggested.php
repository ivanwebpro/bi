<?php

namespace account\models;

use Yii;

use common\helpers\ArrayHelper;
use common\helpers\StrHelper;
use common\helpers\Url;
use yii\db\Expression;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;

use admin\models\Category;


class StoreSuggested extends  \yii\db\ActiveRecord
{
	public $category_new;

	public function attributeLabels()
	{
  	$als = [];
		$als['category_id'] = "Category";
		$als['url'] = "URL";
		$als['category_new'] = "Add new category";
		return $als;
	}

	public function ddCategories()
	{
		$cats = Category::find()
      ->orderBy('title ASC')
			->all();

		$res = ArrayHelper::map($cats, 'id', 'title');
		$res = ArrayHelper::prepend($res, '', '- not set -');
		return $res;
	}

	public static function tableName()
	{
		return '{{%store_suggested}}';
	}

	public function rules()
	{
  	$rs = [];
		$rs[] = ['url', 'string'];
		$rs[] = ['url', 'required'];
		//$rs[] = ['url', 'url'];
		//$rs[] = ['url', 'checkStore'];
		$rs[] = ['category_id', 'number'];
		$rs[] = ['category_new', 'string'];
		$rs[] = ['category_new', 'chkCategoryNew'];
		return $rs;
	}

	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
	    ],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
			'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],

    ];
	}

	public function chkCategoryNew($a)
	{
		if ($this->hasErrors())
		{
    	return;
		}

		$cat = $this->$a;
		$cx = Category::findOne(['title' => $cat]);

		if (!$cx)
		{
			$cx = new Category;
			$cx->title = $cat;
			$cx->saveEx();
		}

		$this->category_id = $cx->id;
	}



	public $test_content;

	public function beforeSave($insert)
	{
  	if (!parent::beforeSave($insert))
		{
    	return false;
		}

		$this->domain = $this->extractDomain();
		return true;
	}

	public function extractDomain()
	{
		return Url::extractDomain($this->url);
	}

 
	public function getCategory()
	{
    return $this->hasOne(Category::className(), ['id' => 'category_id']);
	}

}