<?php

namespace account\models;

use Yii;

use common\helpers\ArrayHelper;
use common\helpers\StrHelper;
use common\helpers\Url;
use yii\db\Expression;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;
use common\traits\St;

use yii\imagine\Image as ImagineImage;
use Imagine\Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use Imagine\Image\ManipulatorInterface;



class ImageExclude extends  \yii\db\ActiveRecord
{
  use St;


	public static function tableName()
	{
		return '{{%image_exclude}}';
	}


	public function rules()
	{
  	$rs = [];
		return $rs;
	}

	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
	    ],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
			'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],
			'ReplaceBy' => [
				'attrs' => ['url'],
				'class' => 'common\behaviors\ReplaceBy',
			],
    ];
	}


}