<?php

namespace account\models;

use Yii;

class AuthRequest extends base\AuthRequest
{
  public function getShop()
  {
  	return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
  }

	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
	    ],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
      'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],
      'Slug' => [
				'attr' => 'state',
        'class' => 'common\behaviors\Slug',
      ],
    ];
	}
}