<?php

namespace account\models;

use Yii;

class LogShopifyApi extends base\LogShopifyApi
{
	public function log($rq, $opts, $rs)
	{
		$l = new static;
		$l->shop_id = $this->shop_id;
		$l->method = $rq->getMethod();
		$l->uri = (string)$rq->getUri();//getRequestTarget();
		$l->request_body = (string)$rq->getBody();
		$l->headers = json_encode($rq->getHeaders(), JSON_PRETTY_PRINT);
		$l->saveWithException();

		$rs->then
		(
    	function ($response) use ($l)
			{
				//hre($response);
      	//$message = $formatter->format($request, $response);
        //$logger->log($logLevel, $message);
				$l->response_code = (string)$response->getStatusCode();
				$l->response_body = (string)$response->getBody();
				$l->saveWithException();
        //hre($l);

        return $response;
      },
			function ($reason) use ($l)
			{
      	$response = $reason instanceof RequestException
                            ? $reason->getResponse()
                            : null;

				if ($response)
				{
					$l->response_code = (string)$response->getStatusCode();
					$l->response_body = (string)$response->getBody();
        	$l->saveWithException();
				}

        //$message = $formatter->format($request, $response, $reason);
        //$logger->notice($message);
        return \GuzzleHttp\Promise\rejection_for($reason);
      }
		);

		//hre(func_get_args());

	}

	public function log_f()
	{
		return function (callable $handler)
		{
			return function ($request, array $options) use ($handler)
			{
				hre($request);
      	//return $handler($fn($request), $options);
      };

    };
		//
		//hre($request);
		//hr($options);
		//exit;
		//hre(func_get_args());

	}

  public function getShop()
  {
  	return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
  }

	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
	    ],
			'DateTimeOnCreate' => [
				'attr' => 'time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
    ];
	}
}