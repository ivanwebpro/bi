<?php

namespace account\models;

use Yii;
use yii\db\Expression;
use common\helpers\Url;
use common\helpers\ArrayHelper;
use account\models\AppCharging;
use common\traits\St;
use mdm\upload\FileModel;

class AppCfg extends \yii\db\ActiveRecord
{
 	use St;

	public static function tableName()
	{
		return '{{%app_cfg}}';
	}

	public function isHostNoCharged($shop)
	{
		return true;
	}

	public function isShouldBeCharged($shop)
	{
  	return false;
	}

	public function isShouldBeChargedQuickCheck($shop)
	{
		return false;
	}


	public function attributeHints()
	{
		$ahs = parent::attributeHints();
   	$ahs['map_random_refresh'] = 'If checked - all positions will be refresh accordingly to new radius. Otherwise new setting will be used only for new customers';

		return $ahs;
	}

	public function attributeLabels()
	{
		$als = parent::attributeLabels();
    $als['map_random_refresh'] = 'Refresh random positions';
    $als['map_city_random_radius_km'] = 'Radius around city for random positions';
		return $als;
	}



	public function rules()
	{
  	$rs = parent::rules();
		return $rs;
	}

	public static function factory()
	{
  	$obj = static::find()->one();

		if (!$obj)
		{
    	$obj = new static;
			$obj->saveEx();
		}

		return $obj;
	}

	public function behaviors()
	{
    $bs = [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
	    ],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
      'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],
    ];

		$bs['upl_logo'] = [
			'class' => 'mdm\upload\UploadBehavior',
			'attribute' => 'logo_file', // required, use to receive input file
			'savedAttribute' => 'logo_file_id', // optional, use to link model with saved file.
			'uploadPath' => '@admin/uploads/logo', // saved directory. default to '@runtime/upload'
      'autoSave' => true, // when true then uploaded file will be save before ActiveRecord::save()
      'autoDelete' => false, // when true then uploaded file will deleted before ActiveRecord::delete()
		];
		return $bs;
	}

	public function getLogoUrl()
	{
  	return '';
		//$this->getFileUrl('logo_file_id');
	}

	public function getFileUrl($fld)
	{
		//hr($fld);
		$f_id = $this->$fld;
		//hr($f_id);

    $fm = FileModel::findOne($f_id);
    //hre($fm);

		if ($fm && file_exists($fm->filename))
		{
    	return Url::filePathToWebPath($fm->filename);
		}
		return null;
	}

}