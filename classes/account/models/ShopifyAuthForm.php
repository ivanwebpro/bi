<?php

namespace account\models;

use Yii;
use yii\base\Model;
use common\helpers\HttpHelper;
use common\helpers\Url;

class ShopifyAuthForm extends Model
{

	public $client_id;
	public $client_secret;
	public $refresh_token;
	public $scope;
	public $redirect_uri;
	public $state;
	protected $shop_full;
	protected $code;

	public $shop_name = '';
	public $shop_id;
	public $tap_click_id;


  public function getShop()
	{
  	return Shop::findOne($this->shop_id);
	}

  public function authTokenSave()
	{
		$ar = $this->authRequest();
		$this->shop_id = $ar->shop_id;

  	$client = $this->shop->shopifyApi;

		$q = [];
		$q['client_id'] = $this->client_id;
		$q['client_secret'] = $this->client_secret;
		$q['code'] = $this->code;
    $ba = $client->call("POST", "/admin/oauth/access_token", $q);
   	$b = json_encode($ba, JSON_PRETTY_PRINT);

		if (true)//$ba = json_decode($b, true))
		{
			if (!is_array($ba) || !isset($ba['access_token'])
						|| !isset($ba['scope']))
			{
				throw new \Exception("Error on decoding response ($b)");
			}
		}

		$ar = $this->authRequest();
		$ar->response = $b;
		$ar->saveWithException();

    $shop = $this->shop;
    $shop->token = $ba['access_token'];
    $shop->api_fails_count = 0;

    $shop->uninstalled = 0;
    $shop->uninstalled_time = null;

    $shop->uninstalled_data_deleted = 0;
    $shop->uninstalled_data_deleted_time = null;

		$shop->saveWithException();
		$shop->onAuthorized();
	}

  public function authValidate($ps)
	{
  	//hre($ps);
		$app = Yii::$app;
		if (!isset($ps['state'])
					&& ($sv = Yii::$app->session->get($app::SESSION_VAR_AUTH_STATE)))
		{
			$ps['state'] = $sv;
		}

		$rq = ['code', 'shop', 'timestamp', 'hmac', 'state'];
		$up = [];


		foreach($rq as $f)
		{
    	//if ( ($f != 'state') && !isset($ps[$f]) )
    	if (!isset($ps[$f]))
				throw new \Exception("$f parameter should be specified");

			if ($f != 'signature' && $f != 'hmac')
				$up[$f] = $ps[$f];
		}

		// not take state into account for HMAC calculation if wasn't provided in URL parameters
		if (!isset($ps['state']))
		{
			unset($up['state']);
		}
		//unset($up['state']);

		$seconds_in_a_day = 24 * 60 * 60;
		$older_than_a_day = $ps['timestamp'] < (time() - $seconds_in_a_day);
		if ($older_than_a_day)
		{
			throw new \Exception("Timestamp is older than 24 hours");
		}

		ksort($up);
		$s = http_build_query($up);

		$up_no_state = $up;
    unset($up_no_state['state']);
		$s_no_state = http_build_query($up_no_state);

		//hr(htmlspecialchars($s));
		//hr($this);
		$hmac_correct = hash_hmac('sha256', $s, $this->client_secret);
		$hmac_correct_no_state = hash_hmac('sha256', $s_no_state, $this->client_secret);

		//hre("Check hmac: PASSED $ps[hmac] == CORRECT $hmac_correct)");

		if ($ps['hmac'] != $hmac_correct
					&& $ps['hmac'] != $hmac_correct_no_state)
		{
			//hre("hmac is not match ($ps[hmac] != $hmac_correct)");
			throw new \Exception("hmac $ps[hmac] is not match for params ".json_encode($up, JSON_PRETTY_PRINT).", _GET: ".json_encode($_GET, JSON_PRETTY_PRINT));
		}

		if (!$this->authRequest($ps['state']))
		{
			throw new \Exception("Cannot find AuthRequest");
		}

		$this->state = $ps['state'];
		$this->shop_full = $ps['shop'];
		$this->code = $ps['code'];

		return true;
	}

  public function authRequest($state = '')
	{
		if (!$state)
			$state = $this->state;

    return AuthRequest::findOne(['state' => $state]);
	}

  public function authUrl()
	{
	 	$this->redirect_uri = Url::noTrailingSlash($this->redirect_uri);
		$ps = [];
		$vs = ['client_id', 'scope', 'redirect_uri', 'state'];
		foreach($vs as $v)
		{
			$ps[$v] = $this->$v;
		}

  	$url = "https://{$this->shop->host}/admin/oauth/authorize";
    $url .= "?";
    $url .= http_build_query($ps);
		return $url;
	}

  public function attributeHints()
	{
		$ahs = parent::attributeHints();
		$ahs['shop_name'] = "Enter part before <i>.myshopify.com</i>";
		return $ahs;
	}

  public function attributeLabels()
  {
		$als = parent::attributeLabels();
  	return $als;
	}

  public function checkFormat()
	{
  	$sn = $this->shop_name;
		if(!preg_match('/^[a-z0-9-]+$/i', $sn))
		{
    	$this->addError('shop_name', "Shop name can contain only alphanumeric characters and hyphens");
    }
	}

  public function checkHttp()
	{
  	$sn = $this->shop_name;
		$url = "https://$sn.myshopify.com/admin";
		$rc = HttpHelper::getHttpResponseCode($url);
    if($rc >= 400)
		{
			$m = "Url $url can't be reached ($rc response code), please check name or try again";
			Yii::warning($m, __METHOD__);
			$this->addError('shop_name', $m);
		}

	}

  public function rules()
  {
		$rs = parent::rules();

		$rs[] = [['shop_name'], 'required'];
		$rs[] = [['tap_click_id'], 'safe'];
    $rs[] = [['shop_name'], 'string', 'max' => 255];
    $rs[] = [['shop_name'], 'checkFormat'];
    $rs[] = [['shop_name'], 'checkHttp'];

		return $rs;
  }
}