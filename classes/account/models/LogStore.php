<?php

namespace account\models;

use Yii;

use common\helpers\ArrayHelper;
use common\helpers\StrHelper;
use yii\db\Expression;


class LogStore extends  \yii\db\ActiveRecord
{
  public function getStore()
  {
  	return $this->hasOne(Store::className(), ['id' => 'store_id']);
  }

	public function cronMsg($m)
	{
  	if (Yii::$app->cron)
		{
			Yii::$app->cron->addMsg($m);
		}
	}

	public function parse()
	{
		$d = $this->store->domain;

		try
		{
			$this->parseInternal();
			$this->cronMsg("Log Store LOG#$this->id [$d] has been parsed");

			$this->parsed = 1;
			$this->parsed_time = new Expression("NOW()");
			$this->saveEx();


		}
		catch(\Exception $e)
		{
			$this->cronMsg("Log Store LOG#$this->id [$d] - parse error");


      $e_msg = get_class($e).": ".$e->getMessage();
			if (stristr($e_msg, 'simplexml_load_string'))
			{
				Yii::warning("Error on parse store $d, LOG#$this->id, $e_msg: $this->content", 'data');
			}

			throw $e;
      //hr(get_class()$e->getMessage());

			$this->broken = 1;
			$this->broken_time = new Expression("NOW()");
			$this->saveEx();
		}
	}

	public function parseInternal()
	{
		//
		$r = [];
		$r["<s:variant"] = "<s_variant";
		$r["</s:variant>"] = "</s_variant>";
		$r["<s:price"] = "<s_price";
		$r["</s:price>"] = "</s_price>";
		$r["<s:sku"] = "<s_sku";
		$r["</s:sku>"] = "</s_sku>";
		$r["<s:grams"] = "<s_grams";
		$r["</s:grams>"] = "</s_grams>";

		$cx = $this->content;
		foreach($r as $k=>$v)
		{
			$cx = str_ireplace($k, $v, $cx);
		}

		if (!empty($_GET['d_cx']))
		{
      print "<pre>";
				print htmlspecialchars($cx);
      print "</pre>";
			exit;
		}



		$x = simplexml_load_string($cx);
		//hr($x); exit;

		foreach ($x->entry as $entry)
    {
			if (!empty($_GET['d_e']))
			{
				hr('$entry');
				hr($entry);
			}
			//exit;

			$d = [];

			$d['guid'] = $entry->id;
			$d['guid'] = substr($d['guid'], 0, 250);

			$d['shop_id'] = $this->shop_id;
			$d['store_id'] = $this->store_id;
			$d['store_domain'] = $this->store->domain;

			$d['published_time_str'] = (string)$entry->published;
			$d['published_time_utc'] = gmdate('c', strtotime($d['published_time_str']));

			$d['updated_time_str'] = (string)$entry->updated;
			$d['updated_time_utc'] = gmdate('c', strtotime($d['updated_time_str']));

			$d['url'] = (string) $entry->link->attributes()->href;
      $d['title'] = (string) $entry->title;
      $d['summary'] = (string) $entry->summary;

			$vars = $entry->s_variant;

			/*if (

				'https://www.trendingvip.com/products/1386610622525' == $d['guid']
				|| 'https://www.trendingvip.com/products/1384441086013' == $d['guid']
				)
			{
				hr('$entry');
				hr($entry);

      	hr('$vars');
      	hr($vars);
				exit;
			}
			if (!is_array($vars))
			{
      	$vars = [$vars];
			} */

			$vars_data = [];
			$prices = [];
			foreach($entry->s_variant as $var)
			{
				if (!empty($_GET['d_v']))
				{
        	hr('$var');
        	hr($var);
				}
				//hr('va');
				//hr($var->s_price->attributes()->currency);
				$d['currency'] = (string)$var->s_price->attributes()->currency;

      	$el = [];
				$el['title'] = (string)$var->title;
				$el['price'] = (string)$var->s_price;
				$el['sku'] = (string)$var->s_sku;
				$prices[] = (float)$el['price'];
				$vars_data[] = $el;
			}

			$d['price_min'] = min($prices);
			$d['price_max'] = max($prices);
      $d['variants'] = json_encode($vars_data, JSON_PRETTY_PRINT);

			//hr($d);

			$dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($d['summary']);
      $imgs = $dom->find('img');
			//hr('$imgs');
			//hr($imgs[0]->src);
			if (!empty($imgs))
			{
				foreach($imgs as $img)
				{
					$src = $img->src;
					$src = trim($src);

					if (!stristr($src, 'products'))
					{
						continue;
					}

					if ($src && $src != '#')
					{
						if (ImageExclude::findOne(['url' => $src]))
						{
            	continue;
						}

      			$d['external_image_url'] = $src;
						if (Image::addSafe($d['external_image_url']))
						{
							break;
						}
						else
						{
            	$ie = new ImageExclude;
							$ie->url = $d['external_image_url'];
							$ie->saveEx();
						}
					}
				}
			}




			if (!empty($_GET['d_d']))
			{
      	print "<pre>";
					print htmlspecialchars(json_encode($d, JSON_PRETTY_PRINT));
      	print "</pre>";
				//exit;
			}

    	$p = Product::st()->replaceBy($d);
			$p->getImageUrl();
			$p->getScaledImageUrl();
    }
	}



	public static function tableName()
	{
		return '{{%log_store}}';
	}

	public function getDateFmt()
	{
			$d = $this->date_eff;

			if (!$d)
			{
      	return null;
			}

			$ds = explode(" ", $d);
			$date = $ds[0];
			$date_fmt = date('d-M-Y, D', strtotime($date));
			return $date_fmt;

	}

	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
	    ],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
			'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],

    ];
	}
}