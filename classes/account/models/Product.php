<?php

namespace account\models;

use Yii;

use common\helpers\ArrayHelper;
use common\helpers\Url;
use common\helpers\Html;
use common\helpers\StrHelper;
use yii\db\Expression;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;
use common\traits\St;


class Product extends \yii\db\ActiveRecord
{
  use St;

	public static function tableName()
	{
		return '{{%product}}';
	}

	public function sendEmailNotification($to_email)
	{
		$app_name = Yii::$app->name;
		$from_email = 'no-reply@whossellingwhat.io';
		$from_name = $app_name;

		$res = Yii::$app->mailer
			->compose('@account/views/product/email-p', ['product' => $this])
			//->disableAsync()
    	->setFrom([$from_email => $from_name])
    	->setTo($to_email)
    	->setSubject("$this->title was published")
    	->send();

		return $res;

	}

	public function saveBtn()
	{
  	$cl = 'save-product btn btn-'.($this->saved ? 'warning' : 'default');
		$opts = [];
		$opts['class'] = $cl;
		$opts['data-ajax-toggle'] = 1;

		$lbl = "<span class='glyphicon glyphicon-bookmark'></span> Save".($this->saved ? 'd' : '');

		$url = ['product/saved-toggle', 'id' => $this->id, 'ajax' => 1];

		$ks = ['ajax', 'store_id'];
		foreach($ks as $k)
		{
    	if (!empty($_GET[$k]))
			{
      	$url[$k] = $_GET[$k];
			}
		}

		return Html::a($lbl, $url, $opts);
	}

	public function getScaledImageUrl()
	{
		$no_image_path = appRoot()."/common/image.svg";
		$no_image_url = Url::filePathToWebPath($no_image_path);

		if (isLocalHost())
		{
    	return $no_image_url;
		}

		$def_url = $this->getImageUrl();

    $fi = Image::findOne(['url'=>$this->external_image_url]);
		if ($fi)
		{
  		$sc_url =$fi->getImageUrlScaled();
			return $sc_url;
		}
		else
		{
			return $def_url;
		}
	}

	public function getImageUrl()
	{
  	$url = $this->external_image_url;

		$no_image_path = appRoot()."/common/image.svg";
		$no_image_url = Url::filePathToWebPath($no_image_path);

		$url = trim($url);

		if (!$url || '#' == $url)
		{
			return $no_image_url;
		}


		if (substr($url, 0, 5) == '//cdn')
		{
    	$url = "https:$url";
		}

    $fi = Image::findOne(['url'=>$url]);

		if (!$fi)
		{
			try
			{
    		$fi = Image::add($url);
			}
			catch(\GuzzleHttp\Exception\RequestException $e)
			{
				Yii::warning("can't load $url ".get_class($e).": ".$e->getMessage(), __METHOD__);
        //if (isDebug()) throw $e;
				return $no_image_url;
			}

			catch(\GuzzleHttp\Exception\ConnectException $e)
			{
				Yii::warning("can't load $url ".get_class($e).": ".$e->getMessage(), __METHOD__);
				//if (isDebug()) throw $e;
      	return $no_image_url;
			}
			catch(\GuzzleHttp\Exception\ServerException $e)
			{
        Yii::warning("can't load $url ".get_class($e).": ".$e->getMessage(), __METHOD__);
				//if (isDebug()) throw $e;
				return $no_image_url;
			}
 			catch(\GuzzleHttp\Exception\ClientException $e)
			{
				Yii::warning("can't load $url ".get_class($e).": ".$e->getMessage(), __METHOD__);
				//if (isDebug()) throw $e;
				return $no_image_url;
			}

		}



    return $fi->localUrl;
	}

	public function rules()
	{
  	$rs = [];
		return $rs;
	}

	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
	    ],
			'DateTimeOnCreate' => [
				'attr' => 'row_created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
			'DateTimeOnUpdate' => [
				'attr' => 'row_updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],
			'ReplaceBy' => [
				'attrs' => ['shop_id', 'store_id', 'guid'],
				'class' => 'common\behaviors\ReplaceBy',
			],
    ];
	}

  public function shopifyPush($shop)
	{
		$li = new LogImport;
		$li->shop_id = $shop->id;
		$li->product_id = $this->id;
		$li->product_guid = $this->guid;
		$li->store_url = $this->store->url;
		$li->store_product_url = $this->url;


		$li->saveEx();

		$api = $shop->shopifyApi;

		$t = $this->title;
		$t = preg_replace('/[^\x{20}-\x{7F}]/u','', $t);
		//hr($t); exit;

		$ps = [];
		$ps['title'] = $t; //$this->title;
		$ps['body_html'] = $this->summaryClean;

		if (!$shop->publish_on_push_to_shopify)
		{
			$ps['published_scope'] = null;//web
    	$ps['published_at'] = null;
		}

		$ps['variants'] = [];
		$ps['variants'][] = ['price' => $this->price_min];

    $ps['images'] = [];
		$h = $_SERVER["HTTP_HOST"];
    $ps['images'][] = ['src' => "https://$h".$this->imageUrl];


    $li->title = $t;
    $li->price = $this->price_min;
		$li->request = json_encode($ps, JSON_PRETTY_PRINT);
		$li->saveEx();

		try
		{
			$resp = $api->productPost($ps);

			$li->response  = json_encode($resp, JSON_PRETTY_PRINT);
			$li->success = 1;
			$li->completed = 1;
			$li->saveEx();
			$li->handle = $resp['product']['handle'];
			$li->shopify_id = (string)$resp['product']['id'];

			$this->pushed = 1;
			$this->pushed_time = new Expression("NOW()");
			$this->saveEx();


			return $li;
		}
		catch(\GuzzleHttp\Exception\ServerException $e)
		{
			$li->response = (string)$e->getResponse()->getBody();
			$li->error = 1;
      $li->error_msg = $e->getMessage();
			$li->saveEx();
			return $li;
		}
 		catch(\GuzzleHttp\Exception\ClientException $e)
		{
			$err_msg = (string)$e->getResponse()->getBody();
			$li->response = (string)$e->getResponse()->getBody();
			$li->error = 1;
      $li->error_msg = $e->getMessage();
			$li->saveEx();
			return $li;
		}
	}


	public function getShop()
  {
  	return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
  }

	public function getStore()
  {
  	return $this->hasOne(Store::className(), ['id' => 'store_id']);
  }

	public function getUrlNoRef()
	{
   	return "https://href.li/?$this->url";
	}

  public function getSummaryClean()
	{
		$html = $this->summary;

		//$html = "<style>b{color: red}</style> <script> console.log('test'); </script> <iframe src=''></iframe> $html";
		//return $html;

		$dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($html);

		$as = ['style', 'width'];

		foreach($as as $a)
		{
			foreach($dom->find('['.$a.']') as $el)
			{
				foreach ($el->getAllAttributes() as $attr => $val)
				{
					if ($a == $attr)
					{
		  			$el->removeAttribute($attr);
					}
		  	}
			}
		}

		foreach($dom->find('img') as $el)
		{
			//$el->class = 'img-responsive';
			$el->outertext = '';
		}

		$remove_tags = ['iframe', 'script', 'style'];

		foreach($remove_tags as $rt)
		{
			foreach($dom->find($rt) as $el)
			{
				$el->outertext = '';
				//unset($el);
				//delete $el;
				//$el = NULL;
			}
		}

		$html = $dom->innertext;


		/*
<td valign="bottom">
      <p>
        <strong>Vendor: </strong>Trending Vip<br />
        <strong>Type: </strong><br />
        <strong>Price: </strong>
            0.00
      </p>
    </td>*/
		///

    $v_pos = stripos($html, 'vendor:');

		if ($v_pos !== false)
		{
			$before = substr($html, 0, $v_pos);
      $start = strripos($before, "<td");
			$end = stripos($html, "</td>", $v_pos);

			$html = substr($html, 0, $start).substr($html, $end + 5);

		}

		//strip_tags($html, "<strong><b><li><p>");


		/*
		Class yii\helpers\HtmlPurifier
$config->set('HTML.AllowedElements', array('p', 'div', 'a', 'br', 'table', 'thead', 'tbody', 'tr', 'th', 'td', 'ul', 'ol', 'li', 'b', 'i'));
		*/

		$html = \yii\helpers\HtmlPurifier::process($html, [
			'HTML.ForbiddenElements' => ['table', 'tr', 'th', 'thead', 'tbody']
    	//'Attr.EnableID' => true,
		]);

		return $html;


	}

	public function removeDomNodes($html, $xpathString)
	{
    $dom = new \DOMDocument;
    $dom->loadHtml($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    $xpath = new \DOMXPath($dom);
    while ($node = $xpath->query($xpathString)->item(0)) {
        $node->parentNode->removeChild($node);
    }
    return $dom->saveHTML();
	}

	public function strip_selected_tags($text, $tags = array())
	{
		foreach ($tags as $tag)
		{
			$re = '#<'.$tag.'[^>]*>(.*)<#'.$tag.'>/iU';
			//hr(htmlspecialchars($re)); continue;

			if(preg_match_all($re, $text, $found))
			{
				$text = str_replace($found[0],$found[1],$text);
			}
		}

		return $text;
	}

}


		//$html = preg_replace("/<iframe.*?\/iframe>/i", '', $html);
		//$html = preg_replace("/<script.*?\/script>/i", '', $html);
		//$html = preg_replace("/<iframe.*>/i", '', $html);
		//$html = preg_replace("/<script.*>/i", '', $html);


		//$html = $this->removeDomNodes($html, '//script');
		//$html = $this->removeDomNodes($html, '//style');
		//$html = $this->removeDomNodes($html, '//iframe');

		//$html = $this->strip_selected_tags($html, ['script', 'style', 'iframe']);


		//return $html;

		//return $dom->innertext;
