<?php

namespace account\models;

use Yii;
use yii\base\Model;
use common\helpers\HttpHelper;
use common\helpers\Url;

class ActivateForm extends Model
{
	public $shopify_name;
	public $email;

  public function getHost()
	{
  	return "$this->shopify_name.myshopify.com";
	}

  public function rules()
  {
		$rs = [];

		$rs[] = [['shopify_name'], 'string'];
		$rs[] = [['shopify_name'], 'required'];

		$rs[] = [['email'], 'email'];
		$rs[] = [['email'], 'required'];
		$rs[] = [['email'], 'activate'];

		return $rs;
	}

  public function attributeLabels()
  {
		$als = [];
		$als['shopify_name'] = 'Shopify name';
		$als['email'] = 'Email used on purchasing subscription';
		return $als;
	}

  public function getShopHost()
	{
  	return "$this->shopify_name.myshopify.com";
	}

  public function attributeHints()
  {
		$als = [];
		$als['shopify_name'] = 'For example, if you have Shopify store abc.shopify.com - enter just abc';
		return $als;
	}

  public function activate($a)
	{
		if ($this->hasErrors())
		{
    	return;
		}

  	\admin\models\Subscription::refreshApiSafe();
		//\admin\models\Plan::refreshApiSafe();

		$subs = \admin\models\Subscription::getActiveForEmailAll($this->email);


		if (empty($subs))
		{
    	$this->addError('email', "There are no active subscriptions for email `$this->email`");
		}

		foreach($subs as $sub)
		{
			if (!$sub->plan)
			{
				hr("Can't get plan for plan code $sub->plan_code"); exit;

			}

    	if ($sub->plan->app_access)
			{
				if ($sub->shopify_name == $this->shopify_name)
				{
      		return true;
				}

				if (!$sub->shopify_name)
				{
					$sub->shopify_name = $this->shopify_name;
					$sub->saveEx();
          return true;
				}
				else
				{
					$this->addError('email', "There are active subscriptions for email `$this->email`, but it is already associated with another store `$sub->shopify_name`");
				}
			}

			//$this->addError('email', "There are some active subscriptions for email `$this->email`, but none of them allow access to the app");
			//return false;

		}

		$this->addError('email', "There are some active subscriptions for email `$this->email`, but none of them allos access to the app");
		return false;

	}


}