<?php

namespace account\models;

use Yii;
use account\models\candles\CandleD1;
use account\models\candles\CandleM5;
use common\helpers\StrHelper;
use common\helpers\DatesHelper;
use common\traits\St;


class LogProxy extends  \yii\db\ActiveRecord
{
	use St;

	public function getProxy()
	{
		return $this->hasOne(Proxy::className(), ['id' => 'proxy_id']);
	}


	public static function tableName()
	{
		return '{{%log_proxy}}';
	}


	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
	    ],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
			'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],
    ];
	}
}
