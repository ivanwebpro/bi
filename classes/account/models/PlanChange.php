<?php

namespace account\models;

use Yii;
use yii\base\Model;
use common\helpers\HttpHelper;
use common\helpers\Url;
use common\helpers\Html;

class PlanChange extends Model
{
	public $plan_code;
	public $shop;
	public $err = '';


  public function changePlan()
	{
		$sub_id = $this->shop->subscription->recurly_id;


		$shop = $this->shop;
		$sub = $shop->subscription;
		$prev_plan = $sub->plan;
    $new_plan = \admin\models\Plan::findOne(['plan_code' => $this->plan_code]);



		//hr("Change sub `$sub_id` to $this->plan_code"); exit;

		$api = Yii::$app->recurlyApi;
		$this->err = '';

		if ($api->changePlan($sub_id, $this->plan_code))
		{
			\admin\models\Subscription::refreshApiSafe();

			// is it downgrading ?
			if (!$new_plan->stores_count_unlimited)
			{
				if ($prev_plan->stores_count_unlimited || $prev_plan->stores_count > $new_plan->stores_count)
				{
        	$this->last_plan_downgrade_time = new Expression("NOW()");
					$this->saveEx();
				}
			}

    	return true;
		}

		$this->err = $api->err;
		return false;
	}

  public function rules()
  {
		$rs = [];
		$rs[] = [['plan_code'], 'string'];
		$rs[] = [['plan_code'], 'required'];
		//$rs[] = [['plan_code'], 'checkDowngrade'];
		return $rs;
	}

  public function checkDowngrade()
	{
		if ($this->hasErrors())
		{
    	return;
		}

		// is it downgrading ?
		$shop = $this->shop;
		$sub = $shop->subscription;
		$current_plan = $sub->plan;
    $new_plan = \admin\models\Plan::findOne(['plan_code' => $this->plan_code]);

		if (!$new_plan->stores_count_unlimited)
		{
    	$current_stores_count = $shop->getStoresQuery()->count();

			if ($current_stores_count > $new_plan->stores_count)
			{
      	$delta = $current_stores_count - $new_plan->stores_count;
				$this->addError("plan_code", "You have $current_stores_count store, while new plan is allowing only $new_plan->stores_count. Please remove extra $delta stores before downgrading");
				return;
			}

			if ($this->last_plan_downgrade_time)
			{
				$ts = strtotime($this->last_plan_downgrade_time);
				$now = time();
				$delta = $now - $ts;
				$dx = 30*24*60*60;

				if ($delta < $dx)
				{
					$av = date('d-M-Y, D h:i a', $ts + $dx);
      		$this->addError("plan_code", "You have downgraded plan less than 30 days. Please wait till $av for downgrading");
					return;
				}
      	//$dt
			}
		}

	}
  public function attributeLabels()
  {
		$als = [];
		$als['plan_code'] = 'Plan';
		return $als;
	}

}
