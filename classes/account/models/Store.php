<?php

namespace account\models;

use Yii;

use common\helpers\ArrayHelper;
use common\helpers\StrHelper;
use common\helpers\Url;
use yii\db\Expression;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;



class Store extends  \yii\db\ActiveRecord
{
	public static $refresh_http_timeout_minutes = 15;
	public static $refresh_db_timeout_minutes = 12;


	public function cronMsg($m)
	{
  	if (Yii::$app->cron)
		{
			Yii::$app->cron->addMsg($m);
		}
	}

	public function beforeSave($insert)
	{
  	if (!parent::beforeSave($insert))
		{
    	return false;
		}

		$this->domain = $this->extractDomain();
		return true;
	}

	public function extractDomain()
	{
		return Url::extractDomain($this->url);
	}

	public static function loadFromUrlInternal($url, $use_proxy = true)
	{
  	$lsh = new LogStoreHttp;
		$lsh->url = $url;
    $lsh->saveEx();

		$proxy = null;
		$proxy_url = null;

		if ($use_proxy)
		{
	    Proxy::addNew();
			$proxy = Proxy::random();

			if ($proxy)
			{
				$proxy_url = "$proxy->protocol://$proxy->ip:$proxy->port";
				$lsh->proxy_url = $proxy_url;
				$lsh->saveEx();
			}
		}

		try
		{
			$res = self::_loadFromUrlInternal($url, $proxy_url);

			$lsh->success = 1;
    	$lsh->saveEx();
			return $res;

		}
		catch(\Exception $e)
		{
			$e_msg = get_class($e).": ".$e->getMessage();
			$lsh->error = 1;
			$lsh->error_message = $e_msg;
			$lsh->saveEx();

			throw $e;
		}

	}


	public static function _loadFromUrlInternal($url, $proxy_url)
	{

		$cfg = [
    	'base_uri' => $url,
    	'timeout'  => 10.0,
			//'handler' => $stack,
		];

		if ($proxy_url)
		{
			$cfg['proxy'] = $proxy_url;
		}

		$client = new GuzzleClient($cfg);

		$hs = [];
		//$hs['Content-Type'] = "application/json";
		$rqx = [];
    $rqx['headers'] = $hs;

		$response = $client->request($method='get', $url='', $rqx);
		$b = $response->getBody();
    $b = (string)$b;
		//hr('body'); hr($b); exit;

		return $b;



	}

	public function loadFromUrl($content='')
	{
		// try to load last fresh data


		if (!$content)
		{
			$q = LogStore::find();
			$q->andWhere(['store_domain' => $this->domain]);
			$q->andWhere(['parsed' => 1]);
			$ms = intval(self::$refresh_db_timeout_minutes);
			$q->andWhere("created_time > DATE_SUB(NOW(), INTERVAL $ms MINUTE)");
			$q->limit(1);
			$q->orderBy(['created_time' => SORT_DESC]);
			$lx = $q->one();

			if ($lx && $lx->content)
			{
      	$content = $lx->content;
			}
		}


  	$lf = new LogStore;
		$lf->shop_id = $this->shop_id;
		$lf->store_id = $this->id;
		$lf->store_domain = $this->domain;
		$lf->url = $this->url;
		$lf->url_real = $this->url_real;


		if (!$lf->url_real)
		{
    	$lf->url_real = $lf->url;
		}

		try
		{
			if ($content)
			{
      	$lf->content = $content;
			}
			else
			{
				$url_real = $this->url;
				if ($this->url_real)
				{
        	$url_real = $this->url_real;
				}

				$lf->content = self::loadFromUrlInternal($url_real);
			}

			$lf->saveEx();
			$lf->parse();

			//$this->load_end = 1;
			if (!$this->load_try_time)
			{
				$this->load_try_time = new Expression("NOW()");
			}

			$this->load_end_time = new Expression("NOW()");

			// send data to all stores
			//$this->sendAll()
		}
		/*catch(\GuzzleHttp\Exception\ClientException)
		{
		} */
		catch(\Exception $e)
		{
			if (!empty($_GET['d']) || !stristr('guzzle', get_class($e)))
			{
      	throw $e;
			}


			$this->cronMsg($e_msg);

			$lf->load_error = 1;
			$lf->load_error_msg = $e_msg;
			$lf->saveEx();

			$this->last_error_msg = $e_msg;
			$this->last_error_time = new Expression("NOW()");
			$this->saveEx();
		}

	}


	public static function cron($shop)
	{
		// load data

		$q = $shop->getStoresQuery();
		$q->andWhere(['load_try_time' => null]);

		if (0 == $q->count())
		{
			$q = $shop->getStoresQuery();

			if (empty($_GET['run_all']))
			{
				$ms = intval(self::$refresh_http_timeout_minutes);
				$q->andWhere("load_try_time < DATE_SUB(NOW(), INTERVAL $ms MINUTE)");
			}

			$q->orderBy('load_try_time ASC');
		}

		$q->limit(5);

		$fs = $q->all();

		if (!empty($_GET['store_id']))
		{
    	$fs = [Store::findOne($_GET['store_id'])];
		}
		foreach($fs as $f)
		{
			$f->load_try_time = new Expression("NOW()");
			$f->saveEx();
    	$f->loadFromUrl();
		}

		// cleanup
		$logs = LogStore::find()
					->andWhere(['OR',
						['parsed' => 1],
						['broken' => 1],
						])
					->andWhere("created_time < DATE_SUB(NOW(), INTERVAL 36 HOUR)")
					->orderBy('id ASC')
          ->limit(10)
					->all();

		foreach($logs as $log)
		{
    	$log->delete();
		}

	}






	public static function tableName()
	{
		return '{{%store}}';
	}



	public function rules()
	{
  	$rs = [];
		$rs[] = ['url', 'string'];
		$rs[] = ['url', 'required'];
		return $rs;
	}

	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
	    ],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
			'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],

    ];
	}

	public function getRemoveAllowedTime()
	{
		$ct = $this->created_time;
		$rat = date(DT_FMT, strtotime($ct) + 30*24*60*60);
		return $rat;
	}

}