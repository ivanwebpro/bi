<?php

namespace account\models;

use Yii;

use common\helpers\ArrayHelper;
use common\helpers\StrHelper;
use yii\db\Expression;


class LogStoreHttp extends  \yii\db\ActiveRecord
{
  public function getStore()
  {
  	return $this->hasOne(Store::className(), ['id' => 'store_id']);
  }


	public static function tableName()
	{
		return '{{%log_store_http}}';
	}


	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
	    ],
			'DateTimeOnCreate' => [
				'attr' => 'time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],

    ];
	}
}