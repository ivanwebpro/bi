<?php

namespace account\models\base;

use Yii;

/**
 * This is the model class for table "{{%app_cfg}}".
 *
 * @property integer $id
 * @property string $created_time
 * @property string $updated_time
 * @property integer $rec_charges_enabled
 * @property string $rec_charges_name
 * @property string $rec_charges_price
 * @property integer $rec_charges_trial_days
 * @property integer $rec_charges_name_on_param_enabled
 * @property string $rec_charges_name_on_param
 * @property string $rec_charges_trial_days_on_param_name
 * @property string $rec_charges_trial_days_on_param_value
 * @property integer $rec_charges_trial_days_on_param
 * @property string $hosts_no_charge
 * @property integer $coming_soon_enabled
 * @property string $coming_soon_skip_for_hosts
 * @property integer $coming_soon_url_enabled
 * @property string $coming_soon_url
 * @property string $coming_soon_page_title
 * @property string $coming_soon_page_content
 * @property integer $affiliate_code_enabled
 * @property string $affiliate_code_common
 * @property string $affiliate_code_landing
 * @property string $affiliate_code_conversion
 * @property string $affiliate_code_logged_in
 * @property string $contact_notification_emails
 */
class AppCfg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%app_cfg}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_time', 'updated_time'], 'safe'],
            [['rec_charges_enabled', 'rec_charges_trial_days', 'rec_charges_name_on_param_enabled', 'rec_charges_trial_days_on_param', 'coming_soon_enabled', 'coming_soon_url_enabled', 'affiliate_code_enabled'], 'integer'],
            [['rec_charges_price'], 'number'],
            [['hosts_no_charge', 'coming_soon_skip_for_hosts', 'coming_soon_page_content', 'affiliate_code_common', 'affiliate_code_landing', 'affiliate_code_conversion', 'affiliate_code_logged_in', 'contact_notification_emails'], 'string'],
            [['rec_charges_name', 'rec_charges_name_on_param', 'rec_charges_trial_days_on_param_name', 'rec_charges_trial_days_on_param_value', 'coming_soon_url', 'coming_soon_page_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'rec_charges_enabled' => 'Rec Charges Enabled',
            'rec_charges_name' => 'Rec Charges Name',
            'rec_charges_price' => 'Rec Charges Price',
            'rec_charges_trial_days' => 'Rec Charges Trial Days',
            'rec_charges_name_on_param_enabled' => 'Rec Charges Name On Param Enabled',
            'rec_charges_name_on_param' => 'Rec Charges Name On Param',
            'rec_charges_trial_days_on_param_name' => 'Rec Charges Trial Days On Param Name',
            'rec_charges_trial_days_on_param_value' => 'Rec Charges Trial Days On Param Value',
            'rec_charges_trial_days_on_param' => 'Rec Charges Trial Days On Param',
            'hosts_no_charge' => 'Hosts No Charge',
            'coming_soon_enabled' => 'Coming Soon Enabled',
            'coming_soon_skip_for_hosts' => 'Coming Soon Skip For Hosts',
            'coming_soon_url_enabled' => 'Coming Soon Url Enabled',
            'coming_soon_url' => 'Coming Soon Url',
            'coming_soon_page_title' => 'Coming Soon Page Title',
            'coming_soon_page_content' => 'Coming Soon Page Content',
            'affiliate_code_enabled' => 'Affiliate Code Enabled',
            'affiliate_code_common' => 'Affiliate Code Common',
            'affiliate_code_landing' => 'Affiliate Code Landing',
            'affiliate_code_conversion' => 'Affiliate Code Conversion',
            'affiliate_code_logged_in' => 'Affiliate Code Logged In',
            'contact_notification_emails' => 'Contact Notification Emails',
        ];
    }
}
