<?php

namespace account\models\base;

use Yii;

/**
 * This is the model class for table "{{%shop}}".
 *
 * @property integer $id
 * @property string $created_time
 * @property string $updated_time
 * @property string $uninstalled_time
 * @property integer $uninstalled
 * @property string $uninstalled_data_deleted_time
 * @property integer $uninstalled_data_deleted
 * @property string $host
 * @property string $token
 * @property string $data
 * @property integer $orders_count
 * @property string $orders_last_update_attempt_time
 * @property string $orders_last_updated_time
 * @property integer $orders_last_page_loaded
 * @property string $cached_data
 * @property integer $api_fails_count
 */
class Shop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_time', 'updated_time', 'uninstalled_time', 'uninstalled_data_deleted_time', 'orders_last_update_attempt_time', 'orders_last_updated_time'], 'safe'],
            [['uninstalled', 'uninstalled_data_deleted', 'orders_count', 'orders_last_page_loaded', 'api_fails_count'], 'integer'],
            [['data', 'cached_data'], 'string'],
            [['host', 'token'], 'string', 'max' => 255],
            [['host'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'uninstalled_time' => 'Uninstalled Time',
            'uninstalled' => 'Uninstalled',
            'uninstalled_data_deleted_time' => 'Uninstalled Data Deleted Time',
            'uninstalled_data_deleted' => 'Uninstalled Data Deleted',
            'host' => 'Host',
            'token' => 'Token',
            'data' => 'Data',
            'orders_count' => 'Orders Count',
            'orders_last_update_attempt_time' => 'Orders Last Update Attempt Time',
            'orders_last_updated_time' => 'Orders Last Updated Time',
            'orders_last_page_loaded' => 'Orders Last Page Loaded',
            'cached_data' => 'Cached Data',
            'api_fails_count' => 'Api Fails Count',
        ];
    }
}
