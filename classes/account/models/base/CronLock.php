<?php

namespace account\models\base;

use Yii;

/**
 * This is the model class for table "cron_lock".
 *
 * @property integer $id
 * @property string $name
 * @property integer $lock
 * @property string $time_start
 * @property string $time_end
 * @property string $delta
 */
class CronLock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cron_lock}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['lock'], 'integer'],
            [['time_start', 'time_end'], 'safe'],
            [['delta'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'lock' => 'Lock',
            'time_start' => 'Time Start',
            'time_end' => 'Time End',
            'delta' => 'Delta',
        ];
    }
}
