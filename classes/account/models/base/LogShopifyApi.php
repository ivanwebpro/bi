<?php

namespace account\models\base;

use Yii;

/**
 * This is the model class for table "voc_log_shopify_api".
 *
 * @property string $id
 * @property string $time
 * @property integer $shop_id
 * @property string $method
 * @property string $uri
 * @property string $request_body
 * @property integer $response_code
 * @property string $response_body
 */
class LogShopifyApi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%log_shopify_api}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time'], 'safe'],
            [['shop_id', 'response_code'], 'integer'],
            [['method', 'uri'], 'required'],
            [['request_body', 'response_body'], 'string'],
            [['method', 'uri'], 'string', 'max' => 255],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shop::className(), 'targetAttribute' => ['shop_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'shop_id' => 'Shop ID',
            'method' => 'Method',
            'uri' => 'Uri',
            'request_body' => 'Request Body',
            'response_code' => 'Response Code',
            'response_body' => 'Response Body',
        ];
    }
}
