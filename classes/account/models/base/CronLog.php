<?php

namespace account\models\base;

use Yii;

/**
 * This is the model class for table "cron_log".
 *
 * @property integer $id
 * @property string $name
 * @property string $preload
 * @property string $time_start
 * @property string $time_end
 * @property string $delta
 */
class CronLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cron_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['preload', 'delta'], 'number'],
            [['time_start', 'time_end'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'preload' => 'Preload',
            'time_start' => 'Time Start',
            'time_end' => 'Time End',
            'delta' => 'Delta',
        ];
    }
}
