<?php

namespace account\models\base;

use Yii;

/**
 * This is the model class for table "auth_request".
 *
 * @property integer $id
 * @property string $created_time
 * @property string $updated_time
 * @property string $state
 */
class AuthRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auth_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_time', 'updated_time'], 'safe'],
            [['state'], 'required'],
            [['state'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'state' => 'State',
        ];
    }
}
