<?php

namespace account\models\base;

use Yii;

/**
 * This is the model class for table "{{%log_app_uninstall}}".
 *
 * @property integer $id
 * @property string $time
 * @property string $shop_host
 * @property integer $shop_id
 * @property string $data
 */
class LogAppUninstall extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%log_app_uninstall}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time'], 'safe'],
            [['shop_host', 'data'], 'required'],
            [['shop_id'], 'integer'],
            [['data'], 'string'],
            [['shop_host'], 'string', 'max' => 255],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shop::className(), 'targetAttribute' => ['shop_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'shop_host' => 'Shop Host',
            'shop_id' => 'Shop ID',
            'data' => 'Data',
        ];
    }
}
