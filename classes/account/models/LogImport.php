<?php

namespace account\models;

use Yii;
use yii\db\Expression;
use yii\db\Query;
use common\helpers\Url;
use common\helpers\ArrayHelper;
use common\traits\St;

class LogImport extends \yii\db\ActiveRecord
{
 	use St;

	public static function tableName()
	{
		return '{{%log_import}}';
	}

	public function getShopifyAdminUrl()
	{
		$h = $this->shop->host;
  	//https://prt04.myshopify.com/admin/products/1408591003709
		return "https://$h/admin/products/$this->shopify_id";
	}
	public function getShopifyUrl()
	{
		$h = $this->shop->host;
  	return "https://$h/products/$this->handle";
	}
	
	public function getShop()
  {
  	return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
  }

	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
			],
			'DateTimeOnCreate' => [
				'attr' => 'time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
    ];
	}
}
