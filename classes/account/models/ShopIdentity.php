<?php
namespace account\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use common\helpers\Url;
use common\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use account\models\Preferences;
use common\apis\ShopifyApi;
use yii\db\Expression;

class ShopIdentity extends base\Shop implements IdentityInterface
{

  public function isRecurringApplicationChargeActiveQuickCheck($shop)
	{
  	return $this->charging_activated_response;
	}


  public function isRecurringApplicationChargeActive($shop)
	{
		try
		{
			$charges = $shop->shopifyApi->recurringApplicationChargesGet();

			//hre($charges);
		}
		catch(\GuzzleHttp\Exception\ClientException $e)
		{
			$m = $e->getMessage();
			Yii::warning("$shop->host error: $m", __METHOD__);
			$rs = $e->getResponse();

			switch($rs->getStatusCode())
			{
      	case 401:
						// 401 Unauthorized
	        	Yii::$app->user->logout();
						Yii::$app->response->redirect(['/']);
						Yii::$app->end();
					case 402:
						// 402 Payment Required
					case 403:
						// 403 Forbidden
					case 404:
						// 404 Not Found
						// shop has been deleted, logout
	        	Yii::$app->user->logout();
						Yii::$app->response->redirect(['/']);
						Yii::$app->end();

						// skip, just wait

						// TODO - add pause before next requests
						break;

					default:
						throw $e;
			}

			throw $e;
		}

		//$charges = json_decode($charges, true);
		$charges = $charges['recurring_application_charges'];

		foreach ($charges as $c)
		{
    	if ($c['status'] == 'active')
			{
				if (!$this->charging_activated_response)
				{
					$ca = ['recurring_application_charge' => $c];
					$cj = json_encode($ca, JSON_PRETTY_PRINT);
        	$this->charging_activated_response = $cj;
        	$this->charging_confirmed = 1;
        	$this->charging_activated = 1;
					$this->saveWithException();
				}

				return true;
			}
		}

		return false;
	}


  public function getShopifyPlan()
	{
    return $this->getShopifyData('plan_name');
	}

  public function getShopifyEmail()
	{
  	return $this->getShopifyData('email');
	}

  public function getShopifyDomain()
	{
  	$d = $this->getShopifyData('domain');
		return $d;
	}

	public function getShopifyId()
	{
  	return $this->getShopifyData('id');
	}

	public function getShopifyMoneyFormat()
	{
    return $this->getShopifyData('money_format');//money_with_currency_format
	}


	public function getShopifyName()
	{
  	return $this->getShopifyData('name');
	}

	protected function getShopifyData($f)
	{
  	$a = json_decode($this->data, true);

		if (is_array($a) && isset($a['shop'])
					&& isset($a['shop'][$f]))
		{
    	return $a['shop'][$f];
		}
		else
		{
    	return '';
		}
	}



  public function getShopifyApi()
	{
  	$client = new ShopifyApi;
		$client->shop_host = $this->host;
		$client->shop_id = $this->id;
		$client->token = $this->token;
		return $client;
	}

  public function onAuthorized()
	{
		$api = $this->shopifyApi;
    $shop_data = $api->shopGet();
		$this->data = json_encode($shop_data, JSON_PRETTY_PRINT);
		//if ($e = $this->shopifyEmail())
			//$this->shopify_email = $e;
		$this->saveWithException();
	}


  public function getTimezone()
	{
		return $this->getShopifyData('iana_timezone');
	}



  public function getPreferences()
	{
  	return $this->hasOne(Preferences::className(),
						['shop_id' => 'id']);

	}

  public function getJsUrl()
	{
		return appRootWeb().'/frontend/script.js';//?shop='.urlencode($this->host);
	}

	public function checkScripts()
	{
		$d = [];
		$d["event"] = "onload";
    $d["src"] = $this->jsUrl;
		$d["src"] = str_ireplace("http://", "https://", $d["src"]);
		$d['display_scope'] = 'online_store';
		//hr($d);
		//Specifies where the file should be included. "online_store" means only web storefront, "order_status" means only the order status page, while "all" means both.
    //$this->scriptTagsDelete();

 		$tags = $this->shopifyApi->scriptTagGet();
		///hre($tags);

		$found = false;
		foreach($tags['script_tags'] as $t)
		{
			if (($d['src'] == $t['src'])
						&& ($d['display_scope'] == $t['display_scope']))
			{
				$found = true;
      	//return true;
			}
      else
			{
				$r = $this->shopifyApi->scriptTagDelete($t['id']);
				//hr("scriptTagDelete($t[id])"); //hr($r);
			}
		}

		if ($found)
		{
    	return true;
		}


		$r = $this->shopifyApi->scriptTagPost(['script_tag' => $d]);
		$this->script_tag_add_response = json_encode($r, JSON_PRETTY_PRINT);
		$this->saveWithException();
	}


	public function scriptTagsDelete()
	{
	}

 	protected static $shop_emulated;
	public static function emulateShop($s)
	{
		static::$shop_emulated = $u;
	}

  public static function getCurrentShop()
	{
		//  	return Shop::findOne(1);
		if (isset(static::$shop_emulated))
			return static::$shop_emulated;

  	return Yii::$app->user->identity;
	}



  public function rules()
  {
		return []; // no access from user's side
  }

  public static function findIdentity($id)
  {
  	return static::findOne(['id' => $id]);
  }

  public static function findIdentityByAccessToken($token, $type = null)
  {
  	throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
  }

	public static function findByUsername($username)
	{
		return static::findOne(['id' => $username, ]);
	}

	public static function findByPasswordResetToken($token)
  {
  	throw new NotSupportedException('"'.__METHOD__.'" is not implemented.');
  }

  public static function isPasswordResetTokenValid($token)
  {
  	throw new NotSupportedException('"'.__METHOD__.'" is not implemented.');
	}

	public function getId()
	{
		return $this->getPrimaryKey();
	}

	  public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
	    ],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
      'DateTimeOnUpdate' => [
				'attr' => 'updated_time',
        'class' => 'common\behaviors\DateTimeOnUpdate',
      ],
      'TimeDelta' => [
				'class' => 'common\behaviors\TimeDelta',
			],

    ];
	}

	public function checkHooks()
	{
		$api = $this->shopifyApi;
		//hre($h_url);
		$dbg = isset($_GET['dbg']);
    $dbg = true;
		if ($dbg)
		{
			//hr('hooks');
		}

		$whs = $api->webhookGet();

		if ($dbg)
		{
			//hre($whs);
			$jwhs = json_encode($whs);
			///hre($jwhs);
			Yii::$app->cron->addMsg("Current hooks: $jwhs");
		}

		if (!is_array($whs))
		{
			hr($whs);
    	hre($this);
		}
		// clean hooks
		$whs = $whs['webhooks'];

		$force_https = true;
		$host_overload = '';

		if (isLocalHost())
		{
    	$host_overload = 'apps.ivanwebdev.com';
		}

		// data hooks
		$hs = [
			'products/create',
			'products/delete',
			'products/update',
			'collections/create',
			'collections/delete',
			'collections/update',
			'orders/create',
			'orders/delete',
			'orders/updated',
		];

		foreach($hs as $tk => $h)
		{
			Yii::$app->cron->addMsg("<b>Chk Hook $h</b>");
			
			$h_url = Url::absolute(["hook/index", 'topic' => $h, 'shop_host' => $this->host], $force_https, $host_overload);


			$h_url_dec = str_ireplace("%2F", "/", $h_url);

			$wh0 = ArrayHelper::find($whs, 'topic', $h);

			if ($wh0)
			{
			 	if ($h_url == $wh0['address']
							|| $h_url_dec == $wh0['address'])
				{
					Yii::$app->cron->addMsg("Hook $h already installed for $this->host, skipping");
  	    	continue;
				}
				else
				{
					Yii::$app->cron->addMsg("Hook $h already installed for $this->host with another URL ($wh0[address]) while $h_url is correct; deleting");
					$api->webhookDelete($wh0['id']);
				}
			}

			$wh = ["webhook" => [
				"topic" => $h,
				"address" => $h_url,
				"format" => "json",
			]];
			//hre($wh);
			$res = $api->webhookPost($wh);
			Yii::$app->cron->addMsg("Hook ".json_encode($wh, JSON_PRETTY_PRINT)." has been installed $this->host");
		}

		$this->hooks_installed = 1;
		$this->hooks_checked_time = new Expression("NOW()");
		$this->saveWithException();
	}

}
