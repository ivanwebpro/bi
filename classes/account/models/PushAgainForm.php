<?php

namespace account\models;

use Yii;
use yii\base\Model;
use common\helpers\HttpHelper;
use common\helpers\Url;
use common\helpers\Html;

class PushAgainForm extends Model
{
	public $run;

  public function rules()
  {
		$rs = [];
		$rs[] = [['run'], 'number'];
		$rs[] = [['run'], 'required'];
		return $rs;
	}
}
