<?php

namespace account\models;

use Yii;
use yii\base\Model;
use common\helpers\HttpHelper;
use common\helpers\Url;
use common\helpers\Html;

class StoreAddForm extends Model
{
	public $url;
	public $url_real;
	public $validate_store_http = true;
	public $use_proxy = true;

  public function saveNewStore()
	{
  	$f = new Store;
		$f->shop_id = $this->shop->id;
		$f->url = $this->url;
		$f->url_real = $this->url_real;
		$f->saveEx();

		if ($this->test_content)
		{
			$f->loadFromUrl($content=$this->test_content);
		}
	}

  public function rules()
  {
		$rs = [];
		$rs[] = [['url'], 'string'];
		$rs[] = [['url'], 'required'];
		//$rs[] = [['url'], 'checkSymbols'];
		$rs[] = [['url'], 'checkDomain'];

		//$rs[] = [['url'], 'url'];
		$rs[] = [['url'], 'checkStore'];
		return $rs;
	}

  public function attributeLabels()
  {
		$als = [];
		$als['url'] = 'URL';
		return $als;
	}


	public $shop;
	public $test_content;

	public function extractDomain()
	{
		return Url::extractDomain($this->url);
	}

	public $show_upgrade_link = false;
	public $show_upgrade_text = '';

	public $url_proc = '';

	public function checkDomain($a)
	{
		if ($this->hasErrors())
		{
    	return;
		}

		$d = $this->$a;
		$t = '.com';
		if (substr($d, strlen($t)) != $t)
		{
			if (!strstr($d, "."))
			{
      	$d .= $t;
			}
		}

		$this->$a = $d;

		/*$url = "https://$d";
		$rc = \common\helpers\HttpHelper::getHttpResponseCode($url);
    if($rc >= 400)
		{
			$m = "Url $url can't be reached ($rc response code), please check name or try again";
			Yii::warning($m, __METHOD__);
			$this->addError($a, $m);
		} */


	}
	public function checkStore($a)
	{
		if ($this->hasErrors())
		{
    	return;
		}

		// check count
		$shop = $this->shop;
		if (!$shop->isPlanUnlimited())
		{
			$sub = $shop->subscription;
			$limit_max = $sub->plan->stores_count;
			$current_stores_count = $shop->getStoresQuery()->count();
			if ($current_stores_count >= $limit_max)
			{
				$pn = $sub->plan->plan_name;
				$err = "You have reached limit of stores ($limit_max) on your plan `$pn`";

				$this->addError($a, $err);
				$this->show_upgrade_link = true;

				$btn = Html::a("Upgrade", ['profile/plan-upgrade'], ['class' => 'btn btn-danger']);
				$this->show_upgrade_text = "<p>$err. You can upgrade you plan here:</p> $btn";
			}
		}


		$url = $this->$a;
		$domain = $this->extractDomain();
		$url = $domain; //$this->extractDomain();

		$url_real = "http://$url/collections/all.atom";
		$this->url_real = $url_real;

		//return $h;



		// check uniq
		$q = $this->shop->getStoresQuery();
		$q->andWhere(['domain' => $this->extractDomain()]);

		/*if ($this->id)
		{
    	$q->andWhere(['NOT', ['id'=>$this->id]]);
		} */

		if ($q->count())
		{
    	$this->addError($a, "URL `$url` has been added already");
			return;
		}

		if (!$this->validate_store_http)
		{
    	return;
		}

		try
		{
			$this->test_content = Store::loadFromUrlInternal($url_real, $this->use_proxy);
			$this->test_content = trim($this->test_content);

			$xml_head = '<?xml version="1.0" encoding="UTF-8"?>';
			if (!stristr($this->test_content, $xml_head))
			{
    		//$this->addError($a, "Content on URL `$url` does not have XML header");
    		$this->addError($a, "Wrong response, perhaps this is not Shopify store");
				return;
			}

			simplexml_load_string($this->test_content);
		}
		catch(\Exception $e)
		{
    	$e_msg = get_class($e);
			$e_msg .= ": ".$e->getMessage();
			$e_msg = str_ireplace("collections/all.atom", '...', $e_msg);

			if (stristr($e_msg, 'Could not resolve host:'))
			{
				$this->addError($a, "Can't find domain $domain, please check name and zone (.com, .net, ...");
			}
			else if (stristr($e_msg, '403 Forbidden'))
			{
        $this->addError($a, "Can't access domain $domain, please check name");
			}
			else
			{
				$this->addError($a, $e_msg);
			}
		}



	}


}