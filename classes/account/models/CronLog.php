<?php

namespace account\models;

use Yii;


class CronLog extends base\CronLog
{
	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
			],
    ];
	}
}
