<?php

namespace account\models;

use Yii;
use yii\base\Model;
use common\helpers\HttpHelper;
use common\helpers\Url;
use common\helpers\Html;
use yii\db\Expression;

class Contact extends   \yii\db\ActiveRecord
{

	//public $name;
	//public $store_url;
	//public $email;
	//public $inquiry;


  public function rules()
  {
		$rs = [];

		$rs[] = ['name', 'string'];
		$rs[] = ['name', 'required'];

		$rs[] = ['store_url', 'string'];
		$rs[] = ['store_url', 'required'];

		$rs[] = ['email', 'string'];
		$rs[] = ['email', 'email'];
		$rs[] = ['email', 'required'];

		$rs[] = ['inquiry', 'required'];

		return $rs;
	}

  public function attributeLabels()
  {
		$als = [];
		$als['store_url'] = 'Store URL';
		return $als;
	}


	public function behaviors()
	{
    return [
	  	'ExceptionOnSave' => [
	    	'class' => 'common\behaviors\ExceptionOnSave',
	    ],
			'DateTimeOnCreate' => [
				'attr' => 'created_time',
        'class' => 'common\behaviors\DateTimeOnCreate',
      ],
    ];
	}

	public static function tableName()
	{
		return '{{%contact}}';
	}

	public function sendMail()
	{
		$app_name = Yii::$app->name;
		$to_email = 'support@ecomfuel.io';
		//$to_email = 'ivan@ivanpro.com';

		$from_email = 'no-reply@ecomfuel.io';
		$from_name = $app_name;


		/*$res = Yii::$app->mailer
			->compose('@account/views/contact/email', ['c' => $this])
    	->setFrom([$from_email => $from_name])
    	->setTo($to_email)
    	->setSubject($app_name.": contact form")
    	->send();

		$this->send_res = $res;
		$this->saveEx();*/

		// https://github.com/freshdesk/fresh-samples/blob/master/PHP/create_ticket.php
		// use \Freshdesk\Api;
		$fd_key = 'oS1ccddIuEaDKthMyi1p';
		$fd_domain = 'whossellingwhat';
		$api = new \Freshdesk\Api($fd_key, $fd_domain);
		// $me = $api->agents->current();


		$fd_data = [];
		$fd_data['status'] = 2; //Open
		$fd_data['priority'] = 2; //Medium
		$fd_data['email'] = $this->email;
		$fd_data['subject'] = "$this->name from $this->store_url";
		$fd_data['description'] = $this->inquiry;

		try
		{
			$new = $api->tickets->create($fd_data);
		}
		catch(\Freshdesk\Exceptions\ApiException $e)
		{
			//Yii::warning(var_export($e, true), __METHOD__);
			if (isDebug())
			{

				echo "<pre>";
					//echo (string)$e->response->getBody();
				//echo "</pre>";
    		var_dump($e);
				echo "</pre>";
				exit;
			}

			throw $e;
		}
		//return;

		// save resp in db;
		$this->freshdesk_sent = 1;
		$this->freshdesk_sent_time = new Expression("NOW()");
		$this->freshdesk_response = json_encode($new, JSON_PRETTY_PRINT);
		$this->saveEx();

	}

}
