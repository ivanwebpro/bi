<?php
namespace account\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use common\helpers\Url;
use common\behaviors\LoadPagedData;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;
use common\apis\ShopifyApi;
use common\helpers\ArrayHelper;
use common\helpers\DatesHelper;
use yii\db\Query;
use admin\models\AppCfg;

use api\helpers\OrderHelper;
use api\helpers\ProductHelper;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;


class Shop extends ShopIdentity
{



	public function attributeHints()
	{
		$ahs = [];
		return $ahs;
	}

	public function attributeLabels()
	{
  	$als = [];
  	$als['exclude_from_billing'] = "Exclude from billing";
  	$als['publish_on_push_to_shopify'] = "Automatically activate products on storefront when importing";
  	$als['notify_email_enabled'] = "Email notifications enabled";
  	$als['notify_desktop_enabled'] = "Desktop notifications enabled";
  	$als['notify_email_target'] = "Email address for notifications";
		return $als;
	}

 	public function rules()
	{
  	$rs = [];
		$rs[] = ['exclude_from_billing', 'number'];
		$rs[] = ['host', 'string'];
		$rs[] = ['host', 'required'];
		$rs[] = ['publish_on_push_to_shopify', 'number'];
		$rs[] = ['notify_email_enabled', 'number'];
		$rs[] = ['notify_desktop_enabled', 'number'];
		$rs[] = ['notify_email_target', 'string'];
		return $rs;
	}

 	public function cronRunHooks($env)
	{
    // run hooks
		$lhs = LogHook::find()
			->andWhere(['shop_id' => $this->id])
			->andWhere(['handled' => 0])
			->andWhere([
				'IN',
				'topic',
				$env['topics'],
			])
			->orderBy('id ASC')
			->all();

    if (!empty($_GET['shop_id']))
		{
    	$lhs = [];
		}

		shuffle($lhs);
		foreach($lhs as $lh)
		{
			$a = json_decode($lh->data, true);

			if (!$a['id'])
			{
				//Yii::$app->cron->addMsg("Bad a[id], checking cron log: ".$lh->id);
				$e_msg = "Bad a[id], checking cron log: ".$lh->id;
				Yii::$app->cron->addMsg($e_msg);
				Yii::warning($e_msg, __METHOD__);
				continue;
				/*if (!empty($_GET['json_hook']))
				{
	      	hr($a);
				} */
			}

			if (!$a['id'])
			{
      	//continue;
			}

			$method_get = $env['method_get'];

			$params = [$a['id']];
			$call = [$this->shopifyApi, $method_get];
			$sh_obj = $this->shopifyApi->getSafe($call, $params);

			if ($sh_obj)
			{
				//Yii::warning($sh_obj, __METHOD__);
				$hc = $env['helper_class'];
				$oh = new $hc;
				$oh->shop = $this;
				$oh->convertObject($sh_obj[$env['field']]);

				$lh->handled = 1;
				$lh->handled_time = new Expression("NOW()");
				$lh->saveWithException();
			}
		}

	}

 	public function cronRunHooksAll()
	{
		$envs = [];

		$env = [];
 		$env['topics'] = ['orders/updated', 'orders/create'];
 		$env['method_get'] = 'orderGetOne';
 		$env['helper_class'] = 'api\\helpers\\OrderHelper';
 		$env['field'] = 'order';
		$envs[] = $env;


		$env = [];
 		$env['topics'] = ['products/update', 'product/create'];
 		$env['method_get'] = 'productGetOne';
 		$env['helper_class'] = 'api\\helpers\\ProductHelper';
 		$env['field'] = 'product';
		$envs[] = $env;

		/*
		$env = [];
 		$env['topics'] = ['collections/update', 'collections/create'];
 		$env['method_get'] = 'smartCollectionGetOne';
 		$env['helper_class'] = 'api\\helpers\\SmartCollectionHelper';
 		$env['field'] = 'smart_collection';
		$envs[] = $env;

		$env = [];
 		$env['topics'] = ['collections/update', 'collections/create'];
 		$env['method_get'] = 'customCollectionGetOne';
 		$env['helper_class'] = 'api\\helpers\\CustomCollectionHelper';
 		$env['field'] = 'custom_collection';
		$envs[] = $env;
    */
		
		shuffle($envs);

		foreach($envs as $env)
		{
    	$this->cronRunHooks($env);
		}
	}



 	public function cronNotifications($event)
	{
		if (!$this->notify_email_enabled)
		{
			$this->cronMsg("Email notifications is diabled");
			return;
		}

		$q = $this->getProductsQuery()->orderBy('id ASC');
		$q->andWhere(['>', 'published_time_utc', $this->notify_email_enabled_time]);
		$q->andWhere(['notify_email' => 0]);
		$q->limit(1);

		$p = $q->one();

		if ($p)
		{
			$r = $p->sendEmailNotification($to_email=$this->notify_email_target);

			$p->notify_email = 1;
			$p->notify_email_time = new Expression("NOW()");
			$p->notify_email_success = $r;
			$p->notify_email_response = Yii::$app->mailer->getLastTransaction();
			$p->saveEx();

			//hr("r: $r");
			$this->cronMsg("Message has been sent");
		}

		$this->cronMsg("No new products / no notifications");

	}

 	public function cronMsg($msg)
	{
  	if (Yii::$app->cron)
		{
    	Yii::$app->cron->addMsg($msg);
		}
	}

 	public function cron($event)
	{
		$fs = [];
		$fs['cronStoresLoad'] = 'cronStoresLoad';
		$fs["getIconUrl"] = 'getIconUrl';
		$fs["cronNotifications"] = 'cronNotifications';

		if (!empty($_GET['cr']))
		{
			$cr = $_GET['cr'];
    	$fs = [$cr=>$fs[$cr]];
		}


		shuffle($fs);

		foreach($fs as $f)
		{
    	$this->$f($event);
			Yii::$app->cron->addMsg("$f - OK");
		}
	}


	public function cronStoresLoad($event)
	{
		Store::cron($this);
	}




  public function getEntityNames()
	{
 		$ens = [
			//'product',
			'order',
			'customer',
			];

			if (!empty($_GET['en']))
			{
      	return [$_GET['en']];
			}


		return $ens;
	}


  public function entityToBehaviorName($en)
	{
  	return "load_{$en}s";
	}

 public function getStatus($bool = false)
	{
		$ens = $this->entityNames;

		foreach($ens as $en)
		{
			$bn = $this->entityToBehaviorName($en);

    	if ($this->getBehavior($bn)->getCompleted())
			{
      	continue;
			}
			else
			{
      	if ($bool)
				{
        	return false;
				}
			}

			$cnt = $this->getBehavior($bn)->getCountCached();

			$en_m = $en."s";

			if (is_null($cnt))
			{
      	return "Counting $en_m";
			}

			$b = $this->getBehavior($bn);

     	//hr('$cnt');
     	//hr($cnt);
     	//hr($b->limit);
			//hr($cnt/$b->limit);

			$last_page = $b->getLastPage();

			if (0 == $last_page)
			{
				$last_page = 1;
			}

			if ($bool)
			{
				return false;
			}

			$total_pages = ceil($cnt/$b->limit)+1;
			return "Loading $en_m ($last_page page of $total_pages)";
		}

		if ($bool)
		{
			return true;
		}

		return "Preloading completed";
	}

  public function init()
	{
		$this->on(CRON_EVENT, [$this, 'cron']);
  	parent::init();
	}


  public function behaviors()
	{
   	$bs = parent::behaviors();

		return $bs;
	}


	public function getCustomerByShopifyId($id)
	{
    return $this->getCustomersQuery()->andWhere(['shopify_id' => $id])->one();
	}

	public function getCustomer($id)
	{
 		return $this->getCustomersQuery()->andWhere(['id' => $id])->one();
	}



	public function getCustomersQuery()
	{
    return $this->hasMany(Customer::className(), ['shop_id' => 'id']);
	}

	public function getTagByName($name)
	{
    return $this->getTagsQuery()->andWhere(['name' => $name])->one();
	}

	public function getTag($id)
	{
 		return $this->getTagsQuery()->andWhere(['id' => $id])->one();
	}

	public function getTagsQuery()
	{
    return $this->hasMany(Tag::className(), ['shop_id' => 'id']);
	}


	public function productsCachedCount()
	{
  	return $this->getProductsQuery()->count();
	}


	public function getOrdersQuery()
	{
    return $this->hasMany(Order::className(), ['shop_id' => 'id'])
				->andWhere(['deleted' => 0]);
	}

	public function getProductVariantById($id)
	{
		$q = $this->getProductVariantsQuery();
		$q->andWhere(['id' => $id]);
		return $q->one();
	}

	public function getProductVariantsQuery()
	{
    return $this->hasMany(ProductVariant::className(), ['shop_id' => 'id']);
	}





	public function isVerbose()
	{
		if ('pst015.myshopify.com' == $this->host)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static function newShop($ps)
	{
  	$s = new static($ps);
		return $s;
	}

	public static function findByHost($h)
	{
  	$w = [];
		$w['host'] = $h;
		return static::findOne($w);
	}

  public function getUrlAdmin()
	{
    return "https://$this->host/admin";
	}

  public function getUrl()
	{
		return "https://$this->host";
	}

  public function money($v)
	{
		$v0 = $v;

    $v = abs($v);
		$v_num = $v;
   	$smf = $this->shopifyMoneyFormat;
		$v = number_format($v, 2);

		$awcs = str_replace(".", ",", $v);

		$amount_no_decimals_with_comma_separator = number_format($v_num, 0);

		$r = $smf;

		$r = str_replace('{{', '', $r);
		$r = str_replace('}}', '', $r);
		$r = str_replace('amount_with_comma_separator', $awcs, $r);
		$r = str_replace('amount_no_decimals_with_comma_separator', $amount_no_decimals_with_comma_separator, $r);
		$r = str_replace('amount', $v, $r);

		$r = strip_tags($r);

		if ($v0 < 0)
		{
    	$r = "-$r";
		}
		return $r;
	}


	public function getStoreById($id)
	{
		$q = $this->getStoresQuery();
		$q->andWhere(['id' => $id]);
		return $q->one();
	}

	public function getStoresQuery()
	{
    return $this->hasMany(Store::className(), ['shop_id' => 'id']);
	}

	public function getProductById($id)
	{
		$q = $this->getProductsQuery();
		$q->andWhere(['id' => $id]);
		return $q->one();
	}

	public function getProductsQuery()
	{
    return $this->hasMany(Product::className(), ['shop_id' => 'id']);
	}

	public function getShopifySubDomain()
	{
		$h = $this->host;
		$h = str_ireplace(".myshopify.com", "", $h);
		return $h;
	}

	public function getSubscription()
	{
  	$q = \admin\models\Subscription::find();
	  $q->andWhere(['shopify_name' => $this->getShopifySubDomain()]);
   	return $q->one();
	}

	public function isExcludedFromBilling()
	{
  	if ($this->exclude_from_billing)
		{
    	return true;
		}

		$ex = \admin\models\Setting::isExcludedFromBilling($this->host);

		if ($ex)
		{
    	return true;
		}

		return false;
	}

	public function isPlanUnlimited()
	{

		if ($this->isExcludedFromBilling())
		{
    	return true;
		}

		$sub = $this->subscription;
		if ($sub && $sub->plan && $sub->plan->stores_count_unlimited)
		{
			return true;
		}

		return false;
	}

	public function utcToshopifyTime($t)
	{
		if (is_null($t)) return $t;

  	$utz = new \DateTimeZone('UTC');
		$t_obj = new \DateTime($t, $utz);
		$tz = $this->timezone; // php const
		$tzo = new \DateTimeZone($tz);
		$t_obj->setTimezone($tzo);
		return $t_obj->format("Y-m-d H:i:s");
	}

	public function shopifyToUtcTime($t)
	{
		if (is_null($t)) return $t;

  	$tz = $this->timezone; // php const
		$tzo = new \DateTimeZone($tz);
		$t_obj = new \DateTime($t, $tzo);

		$utz = new \DateTimeZone('UTC');
		$t_obj->setTimezone($utz);

		return $t_obj->format("Y-m-d H:i:s");
	}

	public function fTime($t)
	{
  	return date('d-M-Y, D h:i a', strtotime($t));
	}

	public function getIconUrl()
	{
		$dir = appRoot()."/cache-icon";
		$file = $dir."/$this->id.ico";
    $icon_saved_url = Url::filePathToWebPath($file);
	  $file_default = appRoot()."/common/default.ico";
    $icon_default_url = Url::filePathToWebPath($file_default);

		if (!empty($_GET['ri']))
		{
			if (file_exists($file))
			{
				unlink($file);
			}
		}

		if ($this->icon_saved_time && $this->timeDeltaDb('icon_saved_time') < 60*60)
		{
			if (file_exists($file))
			{
	    		return $icon_saved_url;
			}

			return $icon_default_url;
		}

		$h = $this->host;
		$url_main = "https://$h/";
    $this->icon_saved_time = new Expression("NOW()");
    $this->saveEx();

		try
		{
    	$main_page_data = self::loadFromUrl($url_main);

			//$this->cronLog(__METHOD__.", $main_page_data");
			//$this->cronLog("<pre>".htmlspecialchars($main_page_data)."</pre>");

			$url_x = self::extractFavicon($main_page_data);

			if (!$url_x)
			{
				return $icon_default_url;
			}

			$icon_data = self::loadFromUrl($url_x);
			file_put_contents($file, $icon_data);
      return Url::filePathToWebPath($file);

		}
		catch(\Exception $e)
		{
  		$e_cl = get_class($e);
			$e_msg = $e->getMessage();

			if (stristr($e_cl, "guzzle"))
			{
      	//$this->err = $e_msg;
				//return $on_err;
			}

			throw $e;
		}

	}

	public static function extractFavicon($html)
  {
		$dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($html);

		$links = $dom->find('link');
		if (empty($links))
		{
			self::cronLog("<b>No link tag</b>");
    	return null;
		}

		$url = null;

		foreach($links as $link)
		{
    	//hr("rel: ".$link->rel);
			if (stristr($link->rel, 'icon'))
			{
      	$url = $link->href;
			}
		}

		if (!$url)
		{
			return null;
		}

		self::cronLog("Link to favicon is found: $url");

		if (substr($url, 0, 5) == '//cdn')
		{
    	$url = "https:$url";
		}
		self::cronLog(__METHOD__.": $url");

    return $url;

	}



	public static function loadFromUrl($url)
	{
		self::cronLog(__METHOD__.": $url");

		$client = new GuzzleClient([
    	'base_uri' => $url,
    	'timeout'  => 10.0,
			//'handler' => $stack,
		]);

		$hs = [];
		//$hs['Content-Type'] = "application/json";
		$rqx = [];
    $rqx['headers'] = $hs;

		$response = $client->request($method='get', $url='', $rqx);
		$b = $response->getBody();
    $b = (string)$b;
		//hr('body'); hr($b); exit;

		return $b;
	}

	public static function cronLog($m)
	{
		if (Yii::$app->cron)
		{
  		Yii::$app->cron->addMsg($m);
		}
	}

}

