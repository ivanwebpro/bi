<?php
namespace common\models\logs;

use Yii;
use yii\db\Expression;

class PageLog extends base\PageLog
{
 	public static function log()
	{
  	$m = new static();
		$m->saveWithException();
	}

 	public function behaviors()
	{
		return [
	        'ExceptionOnSave' => [
	            'class' => 'common\behaviors\ExceptionOnSave',
	        ],
			];
	}

	public static $track_app_user = true;

	public function init()
	{
  	$this->time = new Expression("NOW()");
		$this->url = $_SERVER['REQUEST_URI'];
		$this->server_vars = json_encode($_SERVER, JSON_PRETTY_PRINT);

		if (isset($_SESSION))
			$this->session = json_encode($_SESSION, JSON_PRETTY_PRINT);

		if (self::$track_app_user)
		{
			if (stristr($this->url, '/admin-app/'))
			{
				if (Yii::$app->user && ($u = Yii::$app->user->identity))
					$this->admin_app_user_id = $u->id;
			}
			else
			{
				if (Yii::$app->user && ($u = Yii::$app->user->identity))
				{
					$this->shop_id = $u->id;
				}
			}
		}

	}
}
