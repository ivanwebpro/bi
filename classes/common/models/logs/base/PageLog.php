<?php

namespace common\models\logs\base;

use Yii;

/**
 * This is the model class for table "log_page".
 *
 * @property integer $id
 * @property string $time
 * @property integer $user_id
 * @property string $IP
 * @property string $url
 * @property string $session
 *
 * @property User $user
 */
class PageLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%log_page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time', 'url'], 'required'],
            [['time'], 'safe'],
            [['shop_id'], 'integer'],
            [['url', 'session'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'shop_id' => 'Shop ID',
            'url' => 'Url',
            'session' => 'Session',
        ];
    }

}
