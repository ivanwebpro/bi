<?php

namespace common\grid;

use yii\grid\GridView as YiiGridView;
use common\helpers\Html;

class GridView extends YiiGridView
{
	public $header_title_up = '';
	public $header_title_up_options = [];
  public $tableAddClasses = [];

	public function init()
	{
		parent::init();
//    public $tableOptions = ['class' => 'table table-striped table-bordered'];
//public $options = ['class' => 'grid-view'];
		foreach($this->tableAddClasses as $cl)
		{
			$this->tableOptions['class'] .= " ".$cl;
		}


	}

	public function renderTableHeader()
	{
		$cells = [];

		$j = 0;
		foreach ($this->columns as $column)
		{
			$j++;

			$cell = $column->renderHeaderCell();

			if ($this->header_title_up && 1 == $j)
			{
				$column->headerOptions['rowspan'] = 2;
				$cell = $column->renderHeaderCell();
				$cells_header_title = [$cell];

			}
      else
			{
				/* @var $column Column */
				$cells[] = $cell;
			}

		}


		if ($this->header_title_up)
		{
			$opts = $this->header_title_up_options;
      $opts['colspan'] = count($cells);
      $cells_header_title[] = Html::tag('th', $this->header_title_up, $opts);

			$content = '';
			$content .= Html::tag('tr', implode('', $cells_header_title), $this->headerRowOptions);
			$content .= Html::tag('tr', implode('', $cells), $this->headerRowOptions);

		}
		else
		{
			$content = Html::tag('tr', implode('', $cells), $this->headerRowOptions);
		}

		if ($this->filterPosition === self::FILTER_POS_HEADER)
		{
			$content = $this->renderFilters() . $content;
		}
		elseif ($this->filterPosition === self::FILTER_POS_BODY)
		{
			$content .= $this->renderFilters();
		}

		return "<thead>\n" . $content . "\n</thead>";
	}
}
