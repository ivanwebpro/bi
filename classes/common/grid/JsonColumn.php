<?php

namespace common\grid;


use yii\grid\DataColumn;

class JsonColumn extends DataColumn
{
	public $format = 'raw';
	public $collapse_button = true;

  protected function renderDataCellContent($model, $key, $index)
  {
		$id = $model->id."-".$this->attribute;

		$pd = $model[$this->attribute];
		$pd = str_ireplace(",", " , ", $pd);
		$pd = str_ireplace(":", " : ", $pd);
		$pd = str_ireplace("\\/", " \\/ ", $pd);

		$pd = "<pre>$pd</pre>";
		
		if (!$this->collapse_button)
    	return $pd;

		return "<a class='btn btn-primary' role='button' data-toggle='collapse' href='#collapse-$id' aria-expanded='false'>Hide / show</a>
		<div class='collapse' id='collapse-$id'>
  		<div class='well'>
    		$pd
  </div>
</div>";

  }
}