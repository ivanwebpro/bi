<?php

namespace common\grid;


use yii\grid\DataColumn;

class BoolColumn extends DataColumn
{
	public $filter = ['0' => '-', '1' => 'Yes'];
	public $format = 'raw';
	public $valueBool;

  protected function renderDataCellContent($model, $key, $index)
  {
		//hr($model);
		//hr($key);
		//hre($index);
		if (is_callable($this->valueBool))
		{
      $v = call_user_func_array($this->valueBool, [$model]);
		}
		else
		{
    	$v = $model[$this->attribute];
		}


		if ($v)
			return "<span class='glyphicon glyphicon-ok' style='color: green'></span>";
		else
			return "<span class='glyphicon glyphicon-remove' style='color: red'></span>";
  }
}