<?php

return [
	
	'components' => [
		'db' => [
  		'username' => '%%place DB username here%%',
			'dsn' => 'mysql:host=%%place host of DB server here%%;dbname=%%place DB name here%%',
			'password' => '%%place DB password here%%',
		]
	]
];
