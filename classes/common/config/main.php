<?php
use \kartik\datecontrol\Module as KM;

include_once dirname(__DIR__)."/code/dbg.php";

$fs = [
	'/params.php',
	'/params-local.php'
];

$params = [];

foreach($fs as $f)
{
	$ff = __DIR__.$f;

	if (file_exists($ff))
	{
		$params = yii\helpers\ArrayHelper::merge($params, require $ff);
	}
}



$cfg = [

 'aliases' => [
            '@bower' => '@vendor/bower-asset',
            '@npm'   => '@vendor/npm-asset',
        ],

	'timeZone' => 'UTC',
	'name' => "Who's Selling What",
	'params' => $params,
  'vendorPath' => appRoot() . '/vendor',
	'shopify_client_id' => 'a9cfd3678d282679d69e39221d7902e1',
	//'shopify_client_id' => '5aa39bcdd1284123a954d63e464d655f',
	//'shopify_client_secret' => '53c3ad0bf806848380598441bc8454bf',
	'shopify_client_secret' => 'c3b8b13ea5c0bd2660d469a165a982c7',
	'shopify_scope' => 'write_products',
	'redirect_after_login' => ['product/index'],


	'modules' => [

	       'redactor' => [
	            'class' => 'yii\redactor\RedactorModule',
							'imageUploadRoute' => false,
							'fileUploadRoute' => false,
	            //'uploadDir' => '@webroot/path/to/uploadfolder',
	            //'uploadUrl' => '@web/path/to/uploadfolder',
	            'imageAllowExtensions'=>['jpg','png','gif'],
							'widgetClientOptions'=>['plugins' => ['fontcolor', 'video']],
	        ],

   		'datecontrol' =>  [
        'class' => '\kartik\datecontrol\Module',

 // format settings for displaying each date attribute (ICU format example)
        'displaySettings' => [
            //KM::FORMAT_DATE => 'dd-MM-yyyy',
            //KM::FORMAT_DATE => 'dd-MM-yyyy',
            KM::FORMAT_DATE => 'php:d-M-Y',
            KM::FORMAT_TIME => 'hh:mm:ss a',
            KM::FORMAT_DATETIME => 'dd-MM-yyyy hh:mm:ss a',
        ],

        // format settings for saving each date attribute (PHP format example)
        'saveSettings' => [
            KM::FORMAT_DATE => 'php:Y-m-d', // saves as unix timestamp
            KM::FORMAT_TIME => 'php:H:i:s',
            KM::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
        ],

				// automatically use kartik\widgets for each of the above formats
        'autoWidget' => true,

        // default settings for each widget from kartik\widgets used when autoWidget is true
        'autoWidgetSettings' => [
            KM::FORMAT_DATE => ['type'=>2, 'pluginOptions'=>['autoclose'=>true, 'weekStart' => 1,]], // example
            KM::FORMAT_DATETIME => [], // setup if needed
            KM::FORMAT_TIME => [], // setup if needed
        ],

        // custom widget settings that will be used to render the date input instead of kartik\widgets,
        // this will be used when autoWidget is set to false at module or widget level.
        'widgetSettings' => [
            KM::FORMAT_DATE => [
                'class' => 'yii\jui\DatePicker', // example
                'options' => [
                    'dateFormat' => 'php:d-M-Y',
                    'options' => ['class'=>'form-control'],
                ]
            ]
        ],
			],

/////////

  ],

  'components' => [


'assetManager' => [
 	'class' => "common\\components\\AssetManager",
				'appendTimestamp' => true,
				'forceCopy' => isLocalHost() || !empty($_GET['d_ac']),
				//'linkAssets' => !isLocalHost(),
        'bundles' => [
            "yii\\boostrap4\\BootstrapAsset" => [
                'sourcePath' => '@npm/bootstrap/dist'
								//            'sourcePath' => null , 'js' => [ '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js' ] ,

            ],
            "yii\\bootstrap4\\BootstrapPluginAsset" => [
                'sourcePath' => '@npm/bootstrap/dist'
            ]
        ]
    ],


			'view' => [
      	'class' => 'common\components\View',
			],


        'db' => [
            'class' => 'yii\db\Connection',
            ///'enableSchemaCache' => !isLocalHost(),
            'enableSchemaCache' => false,
            'schemaCacheDuration' => 60,

  					'username' => 'bi',
						'dsn' => 'mysql:host=127.0.0.1;dbname=bi',
						'password' => '7VR3a1iMsh7GigZO',


            'charset' => 'utf8',
						'tablePrefix' => 'bi_',
						'on afterOpen' => function($event) {
       				$event->sender->createCommand("SET time_zone = '".date('P')."'")->execute();
							},
        	],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'log' => [
            //'traceLevel' => YII_DEBUG ? 3 : 0,
            'traceLevel' => 3,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
										'except' => ['yii\debug\Module::checkAccess'],
                ],

                [
                  'class' => 'common\components\LogDbTarget',
									'logTable' => '{{%log_yii}}',
                  'levels' => ['error', 'warning'],
									'except' => ['yii\debug\Module::checkAccess', 'data'],
                ],

                [
                  'class' => 'common\components\LogDbTarget',
									'logTable' => '{{%log_data_process}}',
                  'levels' => ['error', 'warning'],
									'categories' => ['data'],
                ],
            ],
				],
		'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
			],

'mailer' => [
 			'class' => 'nickcv\mandrill\Mailer',
			'apikey' => 'CtMGEz6FdERbZtyVLZG5Jg',
			'viewPath' => '@common/mail',
       ],
            //'class' => 'yii\swiftmailer\Mailer',
						//'class' => 'yashop\ses\Mailer',
						//	'enableSwiftMailerLogging' => true,
						//'useFileTransport' => false,
            ///'viewPath' => '@common/mail',
  					//'transport' => [
              //'class' => 'Swift_SendmailTransport',
              //'host' => 'localhost',
              //'username' => 'username',
              //'password' => 'password',
              //'port' => '587',
              //'encryption' => 'tls',
          //],
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
           // 'useFileTransport' => true,
        	//],

    ],
];


if (isLocalHost() || isDebug())
{
	if (!isset($cfg['modules']))
		$cfg['modules'] = [];
	if (!isset($cfg['bootstrap']))
		$cfg['bootstrap'] = [];

 	$cfg['modules']['gii'] = 'yii\gii\Module';
	$cfg['bootstrap'][] = 'gii';

	$cfg['modules']['debug'] = [];
	$cfg['modules']['debug']['class'] = 'yii\debug\Module';
	$cfg['modules']['debug']['allowedIPs'] = ['*'];

	$cfg['modules']['debug']['panels'] = [
						'config' => false,
						//'config' => ['class' => 'yii\debug\panels\ConfigPanel'],
            'request' => false,
            //	'request' => ['class' => 'yii\debug\panels\RequestPanel'],
            'log' => ['class' => 'yii\debug\panels\LogPanel'],
            'profiling' => ['class' => 'yii\debug\panels\ProfilingPanel'],
            'db' => ['class' => 'yii\debug\panels\DbPanel', 'defaultOrder' => ['duration' => SORT_DESC]],
						//'assets' => false,
						'mail' => false,
            //'assets' => ['class' => 'yii\debug\panels\AssetPanel'],
            //'mail' => ['class' => 'yii\debug\panels\MailPanel'],
            'timeline' => ['class' => 'yii\debug\panels\TimelinePanel'],
            //'user' => ['class' => 'yii\debug\panels\UserPanel'],
            //'router' => ['class' => 'yii\debug\panels\RouterPanel'],
            'router' => false,
	];
	$cfg['bootstrap'][] = 'debug';
	//hre($cfg);
}



return $cfg;
