<?php


return [
	'adminEmail' => 'x@x.com',
	'supportEmail' => 'x@x.com',
	'user.passwordResetTokenExpire' => 3600,
];
