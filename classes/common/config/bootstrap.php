<?php
$d = dirname(dirname(__DIR__));

Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('console', $d.'/console');
Yii::setAlias('admin', $d.'/admin');
Yii::setAlias('account', $d.'/account');
Yii::setAlias('frontend', $d.'/frontend');
Yii::setAlias('api', $d.'/api');
Yii::setAlias('app_root', appRoot());
