<?php


namespace common\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Query;


class ExceptionOnSave extends Behavior
{
	public function saveEx()
	{
  	return $this->saveWithException();
	}

	public function saveWithException()
	{
		//try
		//{
			$class = get_class($this->owner);
    	$attrs = var_export($this->owner->attributes, true);

			if (!$this->owner->save())
			{
				$attrs = var_export($this->owner->attributes, true);
				$errs = var_export($this->owner->getErrors(), true);
	  		throw new \Exception("Can't save $class ($attrs): $errs");
			}
			else
				return true;
		//}
		//catch(\Exception $e)
		//{
      ///throw new \Exception("Can't save $class ($attrs): ".$e->getMessage());
		//}
	}
}
