<?php


namespace common\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Query;


class TimeDelta extends Behavior
{
 	public function timeDeltaDb($attr)
	{
		$query = new Query;
		$now = $query->select("NOW()")
			->scalar();
    $v = $this->owner->$attr;
		if (!$v)
			return false;

		$from = strtotime($v);
		$to = strtotime($now);
		$diff = ($to - $from);
		return $diff;
	}
}
