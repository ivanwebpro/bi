<?php


namespace common\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\db\Expression;

class DateTimeOnCreate extends Behavior
{
	public $attr;

 	public function events()
 	{
		return [
    	ActiveRecord::EVENT_BEFORE_VALIDATE => 'updateTime'
    ];
  }

	public function updateTime( $event )
	{
		if ($this->owner->isNewRecord)
		{
			$a = $this->attr;
			$this->owner->$a = new Expression("NOW()");
		}
	}
}
