<?php


namespace common\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Query;


class Slug extends Behavior
{
	public $auto = true;
	public $attr = 'slug';
	public $prefix ='';
	public $len=15;
	public $delimeter=20;
	public $low_case = false;

 	public function events()
 	{
		if (!$this->auto)
			return [];
			
		return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'setSlug'
        ];
  }

	public function setSlug( $event )
	{
   	if (empty($this->owner->{$this->attr}))
		{
			$this->updateSlug($this->attr);
		}
	}

	/*public function refresh()
	{
  	$this->updateSlug();
	} */

	public function updateSlug($attr = '')
	{
		if (!$attr)
			$attr = $this->attr;
			
		$this->owner->$attr = $this->generateSlug($attr);
	}


	public function generateSlug($fld)
	{
		$tbl = $this->owner->tableName();
		$try = 0;
    $tbl_tmp = '{{%tmp_key}}';
		$fld_tmp = 'key';

		// cleanup tmp
		$sql = "DELETE FROM $tbl_tmp
							WHERE created < DATE_SUB(NOW(), INTERVAL 7 DAY)";
		$connection = \Yii::$app->db;
    $q = $connection->createCommand($sql);
		$q->execute();

		do
		{
			$suid = $this->genSomeSlug(
						$this->prefix, $this->len, $this->delimeter);

			if ($this->low_case)
				$suid = strtolower($suid);

			$check_uniq = $this->verifyUniq($tbl, $fld, $suid);
			$check_uniq_tmp = $this->verifyUniq($tbl_tmp, $fld_tmp, $suid);
			$is_uniq = $check_uniq && !$check_uniq_tmp;

			$try++;
		} while (!$is_uniq && ($try < 100));

		if (!$is_uniq && $try > 100)
		{
			throw new \Exception("Cannot generate unique key after 100 attempts");
		}


		$ins = array();
		$ins['created'] = 'NOW()';
		$ins['key'] = $suid;
		$connection->createCommand()->insert($tbl_tmp, $ins)->execute();

		return $suid;
	}

	public function randomletter()
	{
    $digits='23456789'.
						'abcdefghijkmnpqrstuvwxyz'.
						'ABCDEFGHIJKLMNPQRSTUVWXYZ';

		$max = strlen($digits)-1;

		$rnd = $this->randomMinMax(0, $max);
    $letter = $digits[$rnd%$max];
		return $letter;
	}

	public function genSomeSlug($prefix ='', $len=50, $delimeter=5)
	{
		$suid = '';
  	for($i=0; $i<$len; $i++)
		{
			if ($delimeter)
			{
				if ($i>0 && (0 == $i%$delimeter))
					$suid .= '-';
			}

			$suid .= $this->randomletter();
		}

		return $prefix.$suid;
	}

	public function randomMinMax($min, $max)
	{
    return round($min + mt_rand() / mt_getrandmax() * ($max - $min));
	}

	public function verifyUniq($tbl, $fld, $val, $id = false)
	{
		$sql = "SELECT count(*) FROM $tbl WHERE `$fld` = :fld ";
		$ps = [':fld' => $val];

		if ($id)
		{
			$sql .= " AND id <> :id ";
			$ps[':id'] = $id;
		}

		$connection = \Yii::$app->db;
    $q = $connection->createCommand($sql, $ps);
		$cnt = $q->queryScalar();

		return ($cnt == 0);
	}
}
