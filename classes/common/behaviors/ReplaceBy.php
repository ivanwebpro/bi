<?php


namespace common\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Query;


class ReplaceBy extends Behavior
{
	public $attrs;

 	public function replaceBy($props, $new_only = [])
	{
		$w = [];
		foreach($this->attrs as $a)
		{
    	$w[$a] = $props[$a];
		}

		$class = get_class($this->owner);
		$obj = $class::find()->andWhere($w)->one();

		if (!$obj)
		{
			$obj = new $class;

			foreach($new_only as $k=>$v)
				$obj->$k = $v;
		}

		foreach($props as $k=>$v)
			$obj->$k = $v;

		$obj->saveWithException();

		return $obj;
	}

}
