<?php

namespace common\actions;

use Yii;
use yii\helpers\Html;

use account\models\ShopifyAuthForm;
use account\models\AuthRequest;
use account\models\Shop;
use account\models\AppCharging;
use admin\models\AppCfg;
use common\helpers\Url;

class ShopifyConnectAction extends \yii\base\Action
{
	public $special_charging_alias = '';

	public function shopByHost($h)
	{
		$shop = Shop::findByHost(['host' => $h]);

		if (!$shop)
		{
    	$shop = Shop::newShop(['host' => $h]);
			$shop->saveWithException();
		}

		return $shop;
	}


	public function run($msg = '', $logout = 0)
	{
		//hre($this);

		if (empty($_GET))
		{
    	Yii::$app->user->logout();
		}

		$model = new ShopifyAuthForm;

		if (isset($_GET['shop']) && empty($_GET['err']))
		{
			$s = $_GET['shop'];
		}
    elseif ($s = Yii::$app->session->get("SHOP_TO_AUTH"))
		{
			//$s =
		}

		if (!empty($s) && empty($_POST))
		{
			$shop = $this->shopByHost($s);
			$model->shop_name = str_ireplace(".myshopify.com", '', $s);
		}

		$vs = [
			'client_id' => Yii::$app->shopify_client_id,
			'client_secret' => Yii::$app->shopify_client_secret,
			'scope' => Yii::$app->shopify_scope,
			'redirect_uri' => Url::absolute(['/']),
			//'redirect_uri' => Url::absolute([$this->controller->id.'/'.$this->controller->action->id]),
		];

		foreach($vs as $k=>$v)
		{
			$model->$k = $v;
		}

		if (isset($_GET['code']))
		{
			if ($model->authValidate(Yii::$app->request->get()))
			{
				//hr("Code validated");
				try
				{
       		$model->authTokenSave();

					// add charges if configured
					$x_shop = $model->shop;

					$x_shop->shopify_client_id = Yii::$app->shopify_client_id;
          $x_shop->saveWithException();

          //$app_cfg = Yii::$app->appCfg;

					/*$ch = $app_cfg->isShouldBeCharged($model->shop);
          //hre($ch);

					if (false)
					{
						$x_shop->charging_enabled = 0;
						$x_shop->saveWithException();

						$ret_url = Url::absolute(["shopify/charges-activated", 'shop' => $x_shop->host]);
						$confirm_url = $app_cfg->urlForConfirmCharging($x_shop, $ret_url);
						hre($confirm_url);
						$app = Yii::$app;
            $app->session->set($app::SESSION_VAR_CHARGE, $x_shop->host);
            return $this->controller->redirect($confirm_url);
					} */

					$l_res = Yii::$app->user->login($x_shop, $duration_secs = 84600);

					$x_shop->last_ip = Yii::$app->request->remoteIP;// )$_SERVER['REMOTE_ADDR'];
					if ('prt04.myshopify.com' == $x_shop->host)
					{
          	$x_shop->last_ip = '127.0.0.1';
					}

					$x_shop->saveEx();

					if ($x_shop->isVerbose())
					{
						Yii::warning("l_res: $l_res", __METHOD__);
					}


					$x_shop->onAuthorized();
          //$x_shop->uninstalled = 0;
          $x_shop->saveWithException();

					Yii::$app->redirectAfterLogin();
				}
				//catch(\abc\Exception $e)
				catch(\GuzzleHttp\Exception\ClientException $e)
				{
					//$rs = $e->getResponse();
					//if (404 == $rs->getStatusCode())
					Yii::warning($e->getMessage(), __METHOD__);
					Yii::warning($e->getTraceAsString(), __METHOD__);

					$error = "Please try again, error: ".$e->getMessage();
        	Yii::$app->session->setFlash('danger', "Please try again, error: ".$e->getMessage());

					$c_id = $this->controller->id;
					$a_id = $this->controller->action->id;
					$u = ["$c_id/$a_id"];
					return $this->controller->redirectNoPassParams($u);
				}
				catch(\account\models\ShopifyAuthFormException $e)
				{
					Yii::warning($e->getMessage(), __METHOD__);
					Yii::warning($e->getTraceAsString(), __METHOD__);

					$error = "Please try again, error: ".$e->getMessage();
        	Yii::$app->session->setFlash('danger', "Please try again, error: ".$e->getMessage());


					$c_id = $this->controller->id;
					$a_id = $this->controller->action->id;
					$u = ["$c_id/$a_id"];
					return $this->controller->redirectNoPassParams($u);
				}
			}
		}

		//hre("Code not handled");

		// $model->shop_name ||
		//if (!empty($_POST) && ($model->load(Yii::$app->request->post())))

		$model->load(Yii::$app->request->post());

		if ($model->shop_name)
		{
			if ($model->validate())
			{
        $shop_host = $model->shop_name.".myshopify.com";

				$shop_test = \account\models\Shop::findOne(['host' => $shop_host]);

				$ex = \admin\models\Setting::isExcludedFromBilling($shop_host);

				if (!$ex && (!$shop_test || !$shop_test->exclude_from_billing))
				{
					// check subscribed
					$sub = \admin\models\Subscription::getActiveForShopifyName($model->shop_name);

					if (!$sub)
					{
	        	$msg = "Can't find active subscription for store `$shop_host`, please activate";
						return $this->controller->redirect(['activate/index', 'msg' => $msg]);
					}

					if (!$sub->plan->app_access)
					{
	        	$msg = "You have active subscription on plan `$sub->plan_name`,  but this plan does not allow to access to app";
						return $this->controller->redirect(['activate/index', 'msg' => $msg]);
					}
				}


				$ar = new AuthRequest();


				$shop = $this->shopByHost($shop_host);

				$ar->shop_id = $shop->id;//host = $model->shop_name.".myshopify.com";
				$ar->shop_host = $shop_host;
				$ar->saveWithException();
				$model->shop_id = $shop->id;
				$model->state = $ar->state;

				$app = Yii::$app;
				Yii::$app->session->set($app::SESSION_VAR_AUTH_STATE, $ar->state);

      	//hr($model);
      	//hr($model->authUrl());
      	//Yii::$app->request->redirect($model->authUrl());
				//Yii::$app->getResponse()->redirect($model->authUrl());
    		//Yii::$app->end();

				$this->controller->layout = 'simple_layout';

				//hre($model);

				$url = $model->authUrl();
				return $this->controller->redirect($url);
				//return $this->controller->render('auth-redirect-app', ['url'=>$url]);
			}
		}

		$r['model'] = $model;
		$r['msg'] = $msg;

		if (!empty($error))
		{
			$r['error'] = $error;
		}

		return $this->controller->render('index-app', $r);

	}
}


/*<?php

namespace common\actions;

use Yii;
use yii\helpers\Html;

use account\models\ShopifyAuthForm;
use account\models\AuthRequest;
use account\models\Shop;
use admin\models\AppCfg;
use common\helpers\Url;

class ShopifyConnectAction extends \yii\base\Action
{
	public function run($msg = '', $logout = 0)
	{

		$model = new ShopifyAuthForm;

		if (isset($_GET['shop']))
		{
			$s = $_GET['shop'];
		}
    elseif ($s = Yii::$app->session->get("SHOP_TO_AUTH"))
		{
			//$s =
		}

		if (!empty($s) && empty($_POST))
		{
			$shop = Shop::findByHost(['host' => $s]);
			if (!$shop)
			{
      	$shop = Shop::newShop(['host'=>$s]);
			}

			$model->shop_name = str_ireplace(".myshopify.com", '', $s);
		}

		$vs = [
			'client_id' => Yii::$app->shopify_client_id,
			'client_secret' => Yii::$app->shopify_client_secret,
			'scope' => Yii::$app->shopify_scope,
			'redirect_uri' => Url::absolute(['/']),
		];

		foreach($vs as $k=>$v)
		{
			$model->$k = $v;
		}

		if (isset($_GET['code']))
		{
			if ($model->authValidate(Yii::$app->request->get()))
			{
				//hr("Code validated");
				try
				{
       		$model->authTokenSave();

					// add charges if configured
					$x_shop = $model->shop;

          $x_shop->saveWithException();

					Yii::$app->user->login($x_shop, $duration_secs = 84600);
					$x_shop->onAuthorized();
          //$x_shop->uninstalled = 0;
          $x_shop->saveWithException();

					$url = Yii::$app->redirect_after_login;


          $direct_url = Url::absolute($url);
					//$iframe_url = Url::fixShopifyIframe($direct_url, $x_shop->host);
					return $this->controller->redirect($direct_url);

				}
				//catch(\abc\Exception $e)
				catch(\GuzzleHttp\Exception\ClientException $e)
				{
					//$rs = $e->getResponse();
					//if (404 == $rs->getStatusCode())
					Yii::warning($e->getMessage(), __METHOD__);
					Yii::warning($e->getTraceAsString(), __METHOD__);

					$error = "Please try again, error: ".$e->getMessage();
        	Yii::$app->session->setFlash('danger', "Please try again, error: ".$e->getMessage());
					return $this->controller->redirect(['/']);
				}
				catch(\account\models\ShopifyAuthFormException $e)
				{
					Yii::warning($e->getMessage(), __METHOD__);
					Yii::warning($e->getTraceAsString(), __METHOD__);

					$error = "Please try again, error: ".$e->getMessage();
        	Yii::$app->session->setFlash('danger', "Please try again, error: ".$e->getMessage());
					return $this->controller->redirect(['/']);
				}
			}
		}

		//hre("Code not handled");

		if ($model->shop_name || $model->load(Yii::$app->request->post()))
		{
			if ($model->validate())
			{
				$ar = new AuthRequest();

				$shop_host = $model->shop_name.".myshopify.com";
				$shop = Shop::findByHost($shop_host);
				if (!$shop)
				{
      		$shop = Shop::newShop(['host' => $shop_host]);
					$shop->saveWithException();
				}

				$ar->shop_id = $shop->id;//host = $model->shop_name.".myshopify.com";
				$ar->shop_host = $shop_host;
				$ar->saveWithException();
				$model->shop_id = $shop->id;
				$model->state = $ar->state;

				$app = Yii::$app;
				Yii::$app->session->set($app::SESSION_VAR_AUTH_STATE, $ar->state);

      	//hr($model);
      	//hr($model->authUrl());
      	//Yii::$app->request->redirect($model->authUrl());
				//Yii::$app->getResponse()->redirect($model->authUrl());
    		//Yii::$app->end();

				$this->controller->layout = 'simple_layout';
				$url = $model->authUrl();
				return $this->controller->redirect($url);
				//return $this->controller->render('auth-redirect-app', ['url'=>$url]);
			}
		}

		$r['model'] = $model;
		$r['msg'] = $msg;

		if (!empty($error))
		{
			$r['error'] = $error;
		}

		return $this->controller->render('index-app', $r);

	}
}
*/