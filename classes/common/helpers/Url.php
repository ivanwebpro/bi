<?php

namespace common\helpers;

use Yii;
use yii\base\InvalidParamException;

class Url extends \yii\helpers\Url
{
	public static function extractDomain($url)
	{
		$parse = parse_url($url);

		if (empty($parse['host']))
		{
			$url = trim($url);
    	$url = str_ireplace("http://", '', $url);
			$url = str_ireplace("https://", '', $url);
			$url = str_ireplace("//", '', $url);
			$url = str_ireplace("www.", '', $url);
			$parts = explode("/", $url);
			$url = $parts[0];
			return $url;
		}

		$h = $parse['host'];

		$h = str_ireplace("www.", '', $h);
		return $h;
	}

	public static function filePathToWebPath($fp)
	{
		$dr = $_SERVER['DOCUMENT_ROOT'];
		$dx = $fp;
		$dr = str_ireplace('\\', '/', $dr);
		$dx = str_ireplace('\\', '/', $dx);
		return str_ireplace($dr, '', $dx);
	}
	
	public static function utmLabels()
	{
		return [
				'utm_source',
				'utm_medium',
				'utm_campaign',
				'utm_term',
				'utm_content',
			];
	}

	public static function noTrailingSlash($url)
	{
		return rtrim($url, '/\\');
	}

	public static function absoluteApp($url)
	{
    //return Yii::$app->urlManager->createAbsoluteUrl($url);
	}

	/*public static function fixShopifyIframe($url, $host)
	{
		$parsed = parse_url($url);
		$parsed['host'] = $host;
		$parsed['path'] = "/admin/apps/photoslurp".$parsed['path'];
		return static::unparse($parsed);
	} */

	public static function unparse($parsed_url)
	{
  	$scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
  	$host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
  	$port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
  	$user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
  	$pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
  	$pass     = ($user || $pass) ? "$pass@" : '';
  	$path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
  	$query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
  	$fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
  	return "$scheme$user$pass$host$port$path$query$fragment";
	}

	public static function absolute($url, $force_https = true, $host_overload = '')
	{
		//return Yii::$app->urlManager->createAbsoluteUrl($url);
		//return Yii::$app->request->getHostInfo().Url::to($url);

	$url = Yii::$app->urlManager->createAbsoluteUrl($url);

		if ($host_overload)
		{
			 $ups = parse_url($url);
			 if ($ups['host'] != $host_overload)
			 {
			 	$url0 = $url;
       	$url = str_ireplace("//".$ups['host'], "//".$host_overload, $url);
			 }
		}

		if ($force_https && ($host_overload || !isLocalHost()))
		{
    	$url = str_ireplace("http://", "https://", $url);
		}

		return $url;		
	}


	public static function to($url = '', $scheme = false)
	{
  	return parent::to($url, $scheme);
	}

	public static function toCfg($url = '', $cfg)
	{
  	return parent::to($url);
	}

 public static function addParam($url, $k, $v)
	{
		if ('' == $url)
		{
			$url = $_SERVER['REQUEST_URI'];
		}

		$up = parse_url($url);

    $url_path = '';

		if (isset($up['host']))
		{
			if (isset($up['scheme']))
				$url_path .= $up['scheme'].':';

			$url_path .= '//'.$up['host'];
		}

		if (!isset($up['path']))
		{
    	$up['path'] = '';
		}

		$url_path .= $up['path'];

    $pars = array();

		if (isset($up['query']))
		{
			$url_query = $up['query'];
			parse_str($url_query, $pars);
		}

		$pars[$k] = $v;
		$url2 = $url_path.'?'.http_build_query($pars);

    return $url2;
	}

	public static function passParamsMeta()
	{
    $p_names = ['shop', 'no_embed'];//, 'shop_url_token'];
		return $p_names;
	}

	public static function passParams($url)
	{
		//if (!empty($))
		$p_names = static::passParamsMeta();

		if (is_array($url) && !empty($url['~pass_all']))
		{
			unset($url['~pass_all']);
			$p_names = array_keys($_GET);
		}

		/*if (strstr(json_encode($url), "funnel/index")
				&& strstr(json_encode($url), "index"))
		{
			hre($url);
		} */

		foreach($p_names as $p)
		{
    	if (isset($_REQUEST[$p]))
			{
				$v = $_REQUEST[$p];
      	if (is_string($url))
				{
					//hr($url);
        	$url = self::addParam($url, $p, $v, $overwrite = false);
				}
      	if (is_array($url) && !array_key_exists($p, $url))
				{
        	$url[$p] = $v;
				}
			}
		}

  	return $url;
	}

	public static function passParamsArray($a, $uf, $sf)
	{
  	foreach($a as $k=>$el)
		{
			if (isset($el['options'])
						&& array_key_exists('data-skip-shop-token', $el['options']))
			{
      	continue;
			}

    	if (isset($el[$uf]))
			{
      	$a[$k][$uf] = self::passParams($a[$k][$uf]);
			}

			if (isset($el[$sf]))
			{
				$a[$k][$sf] = self::passParamsArray($a[$k][$sf], $uf, $sf);
			}
		}

		return $a;
	}	
}