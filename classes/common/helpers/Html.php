<?php

namespace common\helpers;

use Yii;
use yii\helpers\Html as YiiHtml;
use yii\base\InvalidParamException;
use yii\db\ActiveRecordInterface;
use yii\validators\StringValidator;
use yii\web\Request;
use yii\base\Model;

class Html extends YiiHtml
{
	//public static function a($text, $url = null, $options = [])
	public static function a($text, $url = null, $options = [])
	{
		if (empty($options['_no_pass_params']))
		{
    	$url = Url::passParams($url);
		}
		else
		{
    	//unset()
		}


		return parent::a($text, $url, $options);
	}

	/*public static function ul($els)
	{

	} */

    public static function beginForm($action = '', $method = 'post', $options = [])
    {
			if (!empty($action))
			{
        $action = Url::to($action);
			}

        $hiddenInputs = [];

        $request = Yii::$app->getRequest();
        if ($request instanceof Request) {
            if (strcasecmp($method, 'get') && strcasecmp($method, 'post')) {
                // simulate PUT, DELETE, etc. via POST
                $hiddenInputs[] = static::hiddenInput($request->methodParam, $method);
                $method = 'post';
            }
            $csrf = ArrayHelper::remove($options, 'csrf', true);

            if ($csrf && $request->enableCsrfValidation && strcasecmp($method, 'post') === 0) {
                $hiddenInputs[] = static::hiddenInput($request->csrfParam, $request->getCsrfToken());
            }
        }

        if (!strcasecmp($method, 'get') && ($pos = strpos($action, '?')) !== false) {
            // query parameters in the action are ignored for GET method
            // we use hidden fields to add them back
            foreach (explode('&', substr($action, $pos + 1)) as $pair) {
                if (($pos1 = strpos($pair, '=')) !== false) {
                    $hiddenInputs[] = static::hiddenInput(
                        urldecode(substr($pair, 0, $pos1)),
                        urldecode(substr($pair, $pos1 + 1))
                    );
                } else {
                    $hiddenInputs[] = static::hiddenInput(urldecode($pair), '');
                }
            }
            $action = substr($action, 0, $pos);
        }

        $options['action'] = $action;
        $options['method'] = $method;
        $form = static::beginTag('form', $options);
        if (!empty($hiddenInputs)) {
            $form .= "\n" . implode("\n", $hiddenInputs);
        }

        return $form;
    }
}