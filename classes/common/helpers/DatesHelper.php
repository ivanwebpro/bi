<?php

namespace common\helpers;

use Yii;
use DateTime;
use account\models\Holiday;

class DatesHelper
{

	public static function deltaText($t)
	{
  	$now = time();
		$ts = strtotime($t);
		$delta = $now - $ts;
		$delta_seconds = $delta;
		$delta_minutes = round($delta/60);
		$delta_hours = round($delta_minutes/60);
		$delta_days = round($delta_hours/24);

		if ($delta_days > 1)
		{
    	return "$delta_days days ago";
		}
		else if (1 == $delta_days)
		{
      return "$delta_days day ago";
		}
		else if ($delta_hours > 1)
		{
      return "$delta_hours hours ago";
		}
		else if (1 == $delta_hours)
		{
      return "$delta_hours hour ago";
		}
		else if ($delta_minutes > 1)
		{
      return "$delta_minutes minutes ago";
		}
		else //if (1 == $delta_minutes)
		{
      return "$delta_minutes minute ago";
		}

	}

	public static function monthBizDaysRemaining($dto)
	{
		$now = clone $dto;
		$ldm = clone $now;
		$ldm->modify('last day of this month');

		$cnt = 0;
		$now->modify('tomorrow');
		while($now <= $ldm)
		{
			$wd = strtolower($now->format('D'));
			if ($wd != 'sun' && $wd != 'sat')
			{
    		$cnt++;
			}
      $now->modify('tomorrow');
		}

		return $cnt;
	}


	public static function holidaysYear($y)
	{
		static $holidays = [];

		if (isset($holidays[$y]))
		{
			return $holidays[$y];
		}

		$holidays[$y] = [];

		$hds = Holiday::find()->all();

		foreach($hds as $hd)
		{
			if ($hd['use_strtotime'])
			{
    		$s = $hd['strtotime'];
				$s = str_ireplace("%y", $y, $s);
				$date = date('Y-m-d', strtotime($s));
			}
			else
			{
				$date = sprintf("%04d-%02d-%02d", $y, $hd['month'], $hd['day']);
			}

      $el = [];
			$el['date'] = $date;
			$el['name'] = $hd['name'];//, $week_day";
			$holidays[$y][$date] = $el;
		}

		return $holidays[$y];
	}

  public static function utcToTerminal($utc)
	{
    $dt_fmt = 'Y-m-d H:i:s';
		$ttx = date($dt_fmt, strtotime("+3 hour", strtotime($utc)));
		return $ttx;
	}

	public static function utcNow()
	{
		$ts = time();
		$n = gmdate('Y-m-d H:i:s', $ts);
		return $n;
	}

	public static function terminalNow()
	{
    $ts = time();
		$ttx = gmdate('Y-m-d H:i:s', $ts+3*3600);
		return $ttx;
	}

	public static function extractDigitsDate($d)
	{
  	$y = substr($d, 0, 4);
  	$m = substr($d, 4, 2);
  	$d = substr($d, 6, 2);
		return "$y-$m-$d";
	}

	public static function monthName($m)
	{
		if (stristr($m, 'all'))
		{
			return "All months";
		}

  	$months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July ',
    'August',
    'September',
    'October',
    'November',
    'December',
		];

		return $months[$m-1];
	}


	public static function minutes($d1, $d2)
	{
  	$ds = strtotime($d2) - strtotime($d2);
		$dm = round($ds/60);
		return $dm;
	}

	public static function minute($dt)
	{
		list($date, $time) = explode(' ', $dt);
		list($th, $tm, $ts) = explode(':', $time);
		return ($tm*1);
	}

	public static function hour($dt)
	{
		list($date, $time) = explode(' ', $dt);
		list($th, $tm, $ts) = explode(':', $time);
		//list($dy, $dm, $dd) = explode('-', $date);
   	return ($th*1);
	}

	public static function day($dt)
	{
		$dt = str_ireplace("T", " ", $dt);
		//hre("Month($dt)");
		if (stristr($dt, " "))
		{
			list($date, $time) = explode(' ', $dt);
		}
		else
		{
    	$date = $dt;
		}

		list($dy, $dm, $dd) = explode('-', $date);
		return (1*$dd);
	}

	public static function month($dt)
	{
		$dt = str_ireplace("T", " ", $dt);
		//hre("Month($dt)");
		if (stristr($dt, " "))
		{
			list($date, $time) = explode(' ', $dt);
		}
		else
		{
    	$date = $dt;
		}

		list($dy, $dm, $dd) = explode('-', $date);
		return (1*$dm);
	}

	public static function year($dt)
	{
		$dt = str_ireplace("T", " ", $dt);
		//hre("Month($dt)");
		if (stristr($dt, " "))
		{
			list($date, $time) = explode(' ', $dt);
		}
		else
		{
    	$date = $dt;
		}

		list($dy, $dm, $dd) = explode('-', $date);
		return (1*$dy);
	}


	public static function moveToFirstPrevNonWeekendNonHoliday($date)
	{
		$res = $date;
		$holiday = '';

		do
		{
  		$res = self::yesterday($res);
		} while (self::isWeekend($res) || self::isHoliday($res, $holiday));

		return $res;
	}

	public static function weekdays()
	{
	  	$wdf = [
				'mo' => 'Monday',
				'tu' => 'Tuesday',
				'we' => 'Wednesday',
				'th' => 'Thursday',
				'fr' => 'Friday',
				'sa' => 'Saturday',
				'su' => 'Sunday',
				'any' => 'Any day',
				'non-holiday' => 'Non holidays',
			];

			return $wdf;
	}

	public static function isHolidayExchange($date, $exchange)
	{
 		$q_hs = Holiday::find()->andWhere(['exchange'=>$exchange]);
		$hs = $q_hs->all();

		foreach($hs as $h)
		{
    	if ($h->isMatch($date))
			{
      	return true;
			}
		}

		return false;
	}

	public static function isWeekend($date)
	{
		$weekDay = date('w', strtotime($date));
  	return ($weekDay == 0 || $weekDay == 6);
	}


	public static function hInterval($x1, $x2)
	{
  	list($d1, $t1) = explode(" ", $x1);
  	list($d2, $t2) = explode(" ", $x2);

		if ($d1 == $d2)
		{
    	return "$d1 $t1 ... $t2";
		}
		else
		{
      return "$x1 ... $x2";
		}
	}
	public static function hfDate($date)
	{
		$dx = date("jS F Y", strtotime($date));
    return $dx;

	}

	public static function addDaysToDate($date, $days)
	{
    $date = strtotime("+".$days." days", strtotime($date));
    return  date("Y-m-d", $date);
	}

	public static function addDays($date, $days)
	{
    $date = strtotime("+".$days." days", strtotime($date));
    return  date("Y-m-d", $date);
	}

	public static function subDays($date, $days)
	{
    $date = strtotime("-".$days." days", strtotime($date));
    return  date("Y-m-d", $date);
	}


	public static function briefInterval($int)
	{
  	//time_interval_m
		list($s, $e) = explode('-', $int);
		$s = self::briefTime(trim($s));
		$e = self::briefTime(trim($e));
		return "$s - $e";
	}

	public static function getSunDateBefore($date, $offset)
	{
		$dx = self::getDateBeforeDays($date);

		while (self::weekDayName($dx) != 'sun');
		{
			$dx = Dates::yesterday($dx);
		}

		return $dx;
	}




	public static function briefTime($t)
	{
		$t = str_replace(":00", "", $t);
		if (substr($t, 0, 1) == "0")
			$t = substr($t, 1);

		return $t;
  	/*list($hr,$ms) = explode(":", $t);

		if ($ms == "00")
		{

		}
		else*/
	}

	// 0-6 = Sun-Sat
	public static function weekDayNo($date)
	{
  	return date('w', strtotime($date));
	}

	// su-sa
	public static function weekDayTwoLettersToFull($wd)
	{
		$wd = strtolower($wd);
  	$wdf = [
			'non-holiday' => 'non-holiday',
			'mo' => 'Monday',
			'tu' => 'Tuesday',
			'we' => 'Wednesday',
			'th' => 'Thursday',
			'fr' => 'Friday',
			'sa' => 'Saturday',
			'su' => 'Sunday',
			'any' => 'any day'
			];


		if (empty($wdf[$wd]))
		{
    	throw new \Exception("unknown wd - $wd");
		}

		return $wdf[$wd];
	}

	public static function weekNo($date)
	{
    return date('W', strtotime($date));
	}

	public static function weekDayNameFull($date)
	{
    return date('l', strtotime($date));
	}

	public static function weekDayName($date)
	{
  	return substr(strtolower(date('D', strtotime($date))), 0, 2);
	}

  public static function timeIntervalM($f, $t)
	{
  	return self::fmtTimeM($f).' - '.self::fmtTimeM($t);
	}

	public static function fmtTimeM($t)
	{
		$t = trim($t);
	  list($hr, $min) = explode(":", $t);
		if (1*$hr>=12)
		{
			if (12 != $hr)
				$hr -= 12;

			$hr = sprintf("%02d", $hr);
			$min .= "pm";
		}
		else
			$min .= 'am';

		return $hr.':'.$min;
	}
	//12:15pm
	public static function reformatTime($t)
	{
		list($hr, $min_m) = explode(":", $t);
		$is_pm = strstr($t, 'pm') ? true : false;
		if ($is_pm && $hr != 12) $hr += 12;

		$min = intval($min_m);

		$ft = sprintf("%02d:%02d", $hr, $min);
		return $ft;
	}


  public static function changeDateFormatI2S($date)
	{
		if ($date)
		{
  		list($y, $m, $d) = explode("-", $date);
			return sprintf("%02d/%02d/%02d", $m, $d, $y-2000);
		}
		else
			return null;
	}

  public static function changeDateFormatI2S4($date)
	{
		if ($date)
		{
  		list($y, $m, $d) = explode("-", $date);
			//return sprintf("%02d/%02d/%02d", $m, $d, $y-2000);
			return sprintf("%02d/%02d/%04d", $m, $d, $y);
		}
		else
			return null;
	}

	public static function daysDiffDates($date1, $date2)
	{
		$datetime1 = new DateTime($date1);
		$datetime2 = new DateTime($date2);
		$diff = $datetime1->diff($datetime2);

		$sign = 1;
		if ($diff->invert)
			$sign = -1;

		return $sign * $diff->days;
	}

	public static function dateReformatFromUs($date_us, $format)
	{
		list($m, $d, $y) = explode("/", $date_us);
		$dt = sprintf("%02d/%02d/%02d", $m, $d, $y+2000);
		return date($format, strtotime($dt));
	}

	public static function dateUs($date)
	{
		return static::changeDateFormatI2S($date);
	}

	public static function dateUs4($date)
	{
		return static::changeDateFormatI2S4($date);
	}


	public static function dateInt($date)
	{
		return static::changeDateFormatS2I($date);
	}


  public static function changeDateFormatS2I($date)
	{
  	list($m, $d, $y) = explode("/", $date);
		return sprintf("20%02d-%02d-%02d", $y, $m, $d);
	}

	public static function getDateDiffDays($date, $n, $fmt = 'Y-m-d')
	{
		return date($fmt, strtotime("$n days", strtotime($date)));
	}

	public static function getDateBeforeDays($n, $fmt = 'Y-m-d')
	{
		return date($fmt, strtotime("-$n days"));
	}

	public static function today()
	{
  	return self::getDateBeforeDays(0);
	}

	public static function getDateNextDays($n, $fmt = 'Y-m-d')
	{
		return date($fmt, strtotime("+$n days"));
	}

  public static function setHour($dt, $h)
	{
		$t = new \DateTime($dt);
  	$t->setTime ($h, $t->format("i"), $t->format("s"));
		return $t->format(DT_FMT);
	}

  public static function setTime($dt, $h, $m=0, $s=0)
	{
		$t = new \DateTime($dt);
		$t->setTime($h, $m, $s);
		return $t->format(DT_FMT);
	}

  public static function addMinutes($date, $mins)
	{
    $date = strtotime("+".$mins." mins", strtotime($date));
    return  date("Y-m-d H:i:s", $date);
	}

  public static function addHours($date, $hours)
	{
    $date = strtotime("+".$hours." hours", strtotime($date));
    return  date("Y-m-d H:i:s", $date);

		//$d2 = self::getSqlScalar("DATE_ADD('$date', INTERVAL $hours HOUR)");
		//return $d2;
	}

  public static function subHours($date, $hours)
	{
    $date = strtotime("-".$hours." hours", strtotime($date));
    return  date("Y-m-d H:i:s", $date);

		//$d2 = self::getSqlScalar("DATE_ADD('$date', INTERVAL $hours HOUR)");
		//return $d2;
	}

	// $dob in format - YYYY-MM-DD
  public static function getTomorrowDate($date)
	{
		$datetime = new DateTime($date);
		$datetime->modify('+1 day');
		return $datetime->format('Y-m-d');
	}

  public static function tomorrow($date)
	{
		return static::getTomorrowDate($date);
	}

  public static function yesterday($date)
	{
		$datetime = new DateTime($date);
		$datetime->modify('-1 day');
		return $datetime->format('Y-m-d');
	}


	// $date in format - YYYY-MM-DD
	public static function getLastDayMonthDate($date)
	{
  	return date("Y-m-t", strtotime($date));
	}

	public static function getFirstDayMonthDate($date)
	{
  	list($y, $m, $d) = explode("-", $date);
		return "$y-$m-01";
	}



	public static function getSqlScalar($sql)
	{
		return Yii::$app->db->createCommand("SELECT $sql")->queryScalar();
		//return Yii::$app()->db
			//				->createCommand("SELECT $sql")
				//			->queryScalar();
	}



	public static function date($dt)
	{
    return self::getDatePartFromMySqlDateTime($dt);
	}

	public static function datePartFromDateTime($dt)
	{
  	return self::getDatePartFromMySqlDateTime($dt);
	}

	public static function getDatePartFromMySqlDateTime($d)
	{
		if (strstr($d, 'T')) { $d = str_replace("T", " ", $d); }

		if (!strstr($d, " ")) { return $d; }

		list($date, $time) = explode(' ', $d);
		return $date;
	}

	public static function convertDateTimeToSeconds($dt)
	{
  	$sql = "UNIX_TIMESTAMP('$dt')";
		$sec = self::getSqlScalar($sql);
		return $sec;
	}

  public static function getMinsFromHrMin($time)
	{
    list($th, $tm) = explode(':', $time);
		$th+=0;
		$tm+=0;
		return $th*60+$tm;
	}

  public static function convertMinsToHrMin($mins)
	{
		$th = floor($mins / 60);
		$tm = $mins % 60;
		return sprintf("%02d:%02d", $th, $tm);
	}

	public static function timestampToMySqlDateTime($ts)
	{
		$ts = intval($ts);
		$now = self::getSqlScalar("FROM_UNIXTIME($ts)");
		return $now;
	}

	public static function getNowMySql()
	{
		$now = self::getSqlScalar("NOW()");
		return $now;
	}

	public static function getNowMySqlDate()
	{
		$now = self::getSqlScalar("NOW()");
		list($date, $time) = explode(' ', $now);
		return $date;
	}

	public static function getTomorrowMySqlDate()
	{
		$tom = self::getSqlScalar("DATE_ADD(NOW(), INTERVAL 1 DAY)");
		list($date, $time) = explode(' ', $tom);
		return $date;
	}

	public static function utc2loc($d)
	{
  	return self::addSecondsToMySqlDate($d, 7*3600);
	}

	public static function addSeconds($t, $ss)
	{
   	$dt_fmt = 'Y-m-d H:i:s';
    $tx = date($dt_fmt, strtotime("+$ss seconds", strtotime($t)));
		return $tx;
	}

	public static function subMins($t, $ms)
	{
   	$dt_fmt = 'Y-m-d H:i:s';
    $tx = date($dt_fmt, strtotime("-$ms minutes", strtotime($t)));
		return $tx;
	}

	public static function addSecondsToMySqlDate($d, $ss)
	{
		$ss = intval($ss);
		$x = self::getSqlScalar("DATE_ADD('$d', INTERVAL $ss SECOND)");
		return $x;
	}

	public static function subSecondsFromMySqlDate($d, $ss)
	{
		$ss = intval($ss);
		$x = self::getSqlScalar("DATE_SUB('$d', INTERVAL $ss SECOND)");
		return $x;
	}

	public static function getTimeHrMinFromDateTimeWithWordDayTime($d)
	{
		$hm = self::getTimeHrMinFromDateTime($d);
		list($th, $tm) = explode(':', $hm);

		$h = $th*1;
		if ($h>=4 && $h<12)
			$dt = "утро";
		if ($h>=12 && $h<17)
			$dt = "день";
		if ($h>=17 && $h<21)
			$dt = "вечер";
		if ($h>=21 || $h<4)
			$dt = "ночь";

		return "$dt ($th:$tm)";
	}

	public static function getTimeHrMinFromDateTime($dt)
	{
		list($date, $time) = explode(' ', $dt);
		list($th, $tm, $ts) = explode(':', $time);
		return "$th:$tm";
	}

	public static function getNowMySqlTimeHrMin()
	{
		$now = self::getSqlScalar("NOW()");
		return self::getTimeHrMinFromDateTime($now);
	}


	public static function wordMonthDate($date)
	{
		list($dy, $dm, $dd) = explode('-', $date);
    $dp = $dd.' '.self::one_month($dm).' '.$dy.' г.';
		return $dp;
	}

	public static function humanFormat($mysql_date)
	{
		if (!$mysql_date)
			return '-';

		list($date, $time) = explode(' ', $mysql_date);
		list($th, $tm, $ts) = explode(':', $time);
		list($dy, $dm, $dd) = explode('-', $date);
		$tp = "$th:$tm";

		$days = self::getSqlScalar("TO_DAYS('$mysql_date')");
		$days_today = self::getSqlScalar("TO_DAYS(NOW())");
		$y_today = self::getSqlScalar("YEAR(NOW())");
		//BH::hr("$days == $days_today");

		if ($days == $days_today + 1)
    	$dp = "завтра";
		elseif ($days == $days_today)
    	$dp = "сегодня";
		elseif ($days == $days_today - 1)
    	$dp = "вчера";
		elseif ($days == $days_today - 1)
    	$dp = "позавчера";
		else
		{
			if ($y_today != $dy)
				$dp = $dd.' '.self::one_month($dm).' <nobr>'.$dy.' г.</nobr>';
			else
				$dp = $dd.' '.self::one_month($dm);

			return $dp;
		}

		return "$dp, $tp";


	}

	public static function date_change_marker_rev($date, $m1, $m2)
	{
		if ((false === strpos($date, $m1)))
			return $date;

		$dels = explode($m1, $date);
		$dels = array_reverse($dels);
	  $d2 = implode($m2, $dels);

		return $d2;
	}

  public static function convDDMMYYY_YYYYMMDD($date)
	{
		return self::date_change_marker_rev($date, '-', '-');
	}

  public static function convYYYYMMDD_DDMMYYY($date)
	{
		return self::date_change_marker_rev($date, '-', '-');
	}

}