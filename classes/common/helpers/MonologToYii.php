<?php

namespace common\helpers;

use Monolog\Logger;
use Monolog\Formatter\NormalizerFormatter;
use yii\log\Logger as YiiLogger;
use Monolog\Handler\AbstractProcessingHandler;
use Yii;

class MonologToYii extends AbstractProcessingHandler
{
	public function __construct($level = Logger::DEBUG, $bubble = true)
	{
		parent::__construct($level, $bubble);
	}

	protected function write(array $record)
	{
		$lvl = $record['level_name'];

		$tr = [
    	'DEBUG' => YiiLogger::LEVEL_WARNING,//LEVEL INFO LEVEL_TRACE,
    	'INFO' => YiiLogger::LEVEL_WARNING,
      'NOTICE' => YiiLogger::LEVEL_WARNING,
      'WARNING' => YiiLogger::LEVEL_WARNING,
      'ERROR' => YiiLogger::LEVEL_ERROR,
      'CRITICAL' => YiiLogger::LEVEL_ERROR,
      'ALERT' => YiiLogger::LEVEL_ERROR,
      'EMERGENCY' => YiiLogger::LEVEL_ERROR,
		];

		//hre($record);
		//Yii::getLogger()->log($record["formatted"], $tr[$lvl], $category = __CLASS__);
	}

	protected function getDefaultFormatter()
	{
		return new NormalizerFormatter();
	}
}
