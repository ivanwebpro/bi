<?php

namespace common\helpers;

class ArrayHelper extends \yii\helpers\ArrayHelper
{
	public static function findMinMaxId($a, $v)
	{
		$n = self::findMinMaxPos($a, $v);

		//hr("findMinMaxId, v: $v, n: $n");
		if (!is_null($n))
		{
			return $a[$n]['id'];
		}

    return null;
	}

	public static function findMinMaxPos($a, $v)
	{
		foreach($a as $n=>$el)
		{
      if (!is_null($el['min']) && $v < $el['min'])
			{
      	continue;
			}

      if (!is_null($el['max']) && $v > $el['max'])
			{
      	continue;
			}

			return $n;

		}

    return null;
	}

	public static function find($a, $k, $v)
	{
  	return static::searchElementInArray($a, $k, $v);
	}


	public static function kv2im($a)
	{
		$res = [];
  	foreach($a as $k=>$v)
		{
			$res[] = ['id' => $k, 'name' => $v];
		}

		return $res;
	}

	public static function jsonPretty($j)
	{
  	$a = json_decode($j, true);
		$jx = json_encode($a, JSON_PRETTY_PRINT);
		return $jx;
	}

	public static function explodeTextArea($ta)
	{
		$ta = preg_split( "/(,|\n)/", $ta);
		$ta = array_map('trim', $ta);
		return $ta;
	}

	public static function indexed($a)
	{
		$res = [];
		foreach($a as $el)
		{
			$res[] = $el;
		}

		return $res;
	}

	public static function keyValueInArray($a, $k, $v)
	{
		//hr(__METHOD__);
		//hr($k);
		//hr($v);

  	foreach($a as $el)
		{
    	if ($el[$k] == $v)
				return true;
		}

		return false;
	}

	public static function searchElementInArray($a, $k, $v)
	{
		//hr(__METHOD__);
		//hr($k);
		//hr($v);

  	foreach($a as $el)
		{
    	if ($el[$k] == $v)
				return $el;
		}

		return false;
	}
	public static function prepend($arr, $k, $v)
	{
  	$res = [$k=>$v];
		foreach($arr as $k=>$v)
		{
			$res[$k] = $v;
		}
		return $res;
	}
	
	public static function orderByField($arr, $f, $ascending = true)
	{
		if (is_array($f))
		{
    	$f1 = $f[0];
			$ka = [];
			foreach($arr as $el)
			{
				$f1v = $el[$f1];
				//hr($f1v);
      	if (!isset($ka[$f1v]))
					$ka[$f1v] = [];
				$ka[$f1v][] = $el;
			}

			//hre($ka);

			if ($ascending)
				ksort($ka);
			else
				krsort($ka);

			$arr = [];
			foreach($ka as $els)
			{
				$f2 = $f[1];
				$els = static::orderByField($els, $f2, $ascending);
				$arr = array_merge($arr, $els);
			}

			return $arr;
		}

  	$kv = [];
		foreach($arr as $k=>$v)
			$kv[$k] = $v[$f];

		if ($ascending)
			asort($kv);
		else
			arsort($kv);

		$res = [];
		foreach($kv as $k=>$v)
			$res[] = $arr[$k];

		return $res;
	}

	public static function mapValues(&$data, $f, $p = '')
	{
  	foreach($data as $k=>$v)
		{
    	$data[$k]['value'] = $f($data[$k]['value'], $p);
		}
	}

	public static function pcValues(&$data)
	{
  	foreach($data as $k=>$v)
		{
    	$data[$k]['value'] *= 100;
		}
	}


	public static function asTimeSerie($data, $date_from, $date_to, $date_field, $param_field)
	{
		$kv = [];
		foreach($data as $el)
		{
			$d = $el[$date_field];
			$v = $el[$param_field];
			$kv[$d] = $v;
		}

		// browse all dates and set 0 if not specified
		$date = $date_from;
		while($date <= $date_to)
		{
			if (!isset($kv[$date]))
				$kv[$date] = 0;
			$date = \Dates::tomorrow($date);
		}

		$vs = [];
		foreach($kv as $d=>$v)
		{
			$pt = [];
			$pt['date'] = $d;
			$pt['value'] = $v;
			$vs[] = $pt;
		}
    return $vs;
	}

  public static function topValues($d, $limit)
	{
  	foreach($d as $k=>$s)
		{
			$vc = 0;
			$vs = $s['values'];
			$max_id = 0;
			$vs2 = [];
			$total = 0;
    	foreach($vs as $v)
			{
				$vc++;
				if ($vc < $limit)
					$vs2[] = $v;
				else
				{
        	$total += $v['value'];
				}

				$max_id = max($max_id, $v['id']);
			}

			if (count($vs) < $limit)
				continue;

			$vs2[] = ['id' => $max_id, 'label' => 'Other', 'value' => $total];
			$d[$k]['values'] = $vs2;
		}

		return $d;
	}

	public static function orderByName($arr)
	{
  	$kv = [];
		foreach($arr as $el)
			$kv[$el['id']] = $el['name'];

		//hr($kv);
		asort($kv);
		//hre($kv);

		$res = [];
		foreach($kv as $k=>$v)
			$res[] = ['id'=>$k, 'name' =>$v];

		return $res;

	}

	public static function addIds($arr)
	{
		foreach($arr as $k=>$v)
			$arr[$k]['name'] .= " [".$arr[$k]['id']."]";//.$arr[$k]['name'];

		return $arr;
	}

	public static function kvToDateValue($arr)
	{
		$res = [];
  	foreach($arr as $k=>$v)
			$res[] = ['date' => $k, 'value' => $v*1];

		return $res;
	}

	public static function fillDatesKeys($date_from, $date_to)
	{
		$kv = [];
		$date = $date_from;
		while($date <= $date_to)
		{
			$kv[$date] = 0;
			$date = \Dates::tomorrow($date);
		}

		return $kv;
	}

	/*public static function mapGoogleAdwordsKeys($arr)
	{
		$tr = [
			'ga:adMatchedQuery' => 'Query'
      'ga:impressions' => string '5' (length=1)
              'ga:CTR' => string '60.0' (length=4)
              'ga:adClicks' => string '3' (length=1)
              'ga:CPC' => string '1.76' (length=4)
              'ga:adCost' => string '5.28' (length=4)
	*/

	public static function fldMax($arr, $fld)
	{
		$res = 0;
  	foreach($arr as $k=>$v)
		{
    	if ($fld == $k && is_scalar($v))
			{
				if (!isset($res) || $res < $v)
					$res = $v;
			}

			if (is_array($v))
			{
				$res_sub = self::fldSum($v, $fld);
				if (!isset($res) || $res < $res_sub)
					$res = $res_sub;
			}
		}

		return $res;
	}

	public static function fldSum($arr, $fld)
	{
		$res = 0;
  	foreach($arr as $k=>$v)
		{
    	if ($fld == $k && is_scalar($v))
				$res += $v;

			if (is_array($v))
				$res += self::fldSum($v, $fld);
		}

		return $res;
	}

	public static function valuesSum($arr, $fld = 'value')
	{
		if (!is_array($arr))
			return 0;

		$res = 0;
  	foreach($arr as $k=>$v)
		{
    	if ($fld == $k && is_scalar($v))
				$res += $v;

			if (is_array($v))
				$res += self::valuesSum($v, $fld);
		}

		return $res;
	}


}