<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetUI2 extends AssetBundle
{
    public $sourcePath = '@app_root/design2';
    //public $basePath = '@webroot';
		// public $baseUrl = '@web';

    public $css = [
        'main.css',
    ];
    public $js = [
    		"main.js",
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];

		public function init()
		{
    	parent::init();
		}
}
