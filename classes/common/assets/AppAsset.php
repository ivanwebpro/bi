<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@app_root/common';
    //public $basePath = '@webroot';
		// public $baseUrl = '@web';

    public $css = [
        'css/site.css',
        'css/yii.css',
    ];
    public $js = [

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

		public function init()
		{
    	parent::init();
		}

		public static function register($view)
    {
			if ($view->deny_bs3)
			{
				$msg = "Name: $name, UI styles conflict";
				Yii::warning($msg, __METHOD__);
				hre($msg); exit;
			}

			return parent::register($view);
		}
}
