<?php

namespace common\apis;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;
use common\helpers\MonologToYii;
use common\helpers\DatesHelper;
use account\models\LogShopifyApi;
use Yii;

class RecurlyApi
{

	public function __construct()
	{
  	\Recurly_Client::$subdomain = 'whossellingwhat';//.recurly.com';
		/* your private API key */
		//\Recurly_Client::$apiKey = '7e245613d55143138ab0645d54864b5c';
		\Recurly_Client::$apiKey = '8c76c6015b994188ab9db20bcf3dffcf';

	}

	public $err = '';

	public function changePlan($sub_id, $plan_code)
	{
		$this->err = '';

		try
		{
  		$subscription = \Recurly_Subscription::get($sub_id);
  		$subscription->plan_code = $plan_code;
  		$subscription->updateImmediately();     // Update immediately.
	  	// or $subscription->updateAtRenewal(); // Update when the subscription renews.

  		return true;
		}
		catch (\Recurly_ValidationError $e)
		{
		  $this->err = "Invalid Subscription data: $e";
			return false;
		}
		catch (\Recurly_NotFoundError $e)
		{
	  	$this->err = "Subscription Not Found: $e";
	    return false;
		}
	}

	public function cancelSafe($sub_code)
	{
  	return $this->getSafe('cancel', [$sub_code], $on_err=false);
	}

	public function cancel($sub_code)
	{
		$sub = \Recurly_Subscription::get($sub_code);
  	$sub->cancel();
		return true;
	}

	public function getSafe($call, $params, $on_err)
	{
  	try
		{
    	$r = call_user_func_array($call, $params);
			return $r;
		}
		catch(\Exception $e)
		{
    	$e_cl = get_class($e);
			$e_msg = $e->getMessage();

			if (stristr($e_cl, "recurly_"))
			{
      	$this->err = $e_msg;
				return $on_err;
			}

			throw $e;

		}
	}



	public function invoices($acc_code, $sub_code)
	{
		try
		{
			$invs = \Recurly_InvoiceList::getForAccount($acc_code);
			foreach($invs as $inv)
			{
				$subs = $inv->subscriptions->get();

				//var_dump($inv);
        //print "<pre>"; var_dump($inv->subscriptions->get()); print "</pre>"; exit;

				foreach($subs as $sub)
				{
        	if ($sub->uuid == $sub_code)
					{
						$el = [];

						$el['uuid'] = $inv->uuid;
						$el['state'] = $inv->state;
						$el['invoice_number'] = $inv->invoice_number;
						$el['total_in_cents'] = $inv->total_in_cents;
						$el['total'] = $el['total_in_cents']/100;
						$el['currency'] = $inv->currency;
						$el['total_fmt'] = $el['total']." ".$el['currency'];
						$el['created_at'] = $inv->created_at->format(DT_FMT);
						///$el['created_at'] = str_ireplace("T", " ", $el['created_at']);
            $res[] = $el;
						break;
					}
				}
			}

			return $res;
		}
		catch(\Exception $e)
		{
    	$e_cl = get_class($e);
			$e_msg = $e->getMessage();

			if (stristr($e_cl, "recurly_"))
			{
      	$this->err = $e_msg;
				return [];
			}

			throw $e;

		}
	}


}

