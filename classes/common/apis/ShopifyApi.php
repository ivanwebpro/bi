<?php

namespace common\apis;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;
use common\helpers\MonologToYii;
use common\helpers\DatesHelper;
use account\models\LogShopifyApi;
use Yii;

class ShopifyApi
{
	public $shop_host;
	public $shop_id;
	public $token;

	public function verifyHMAC($ps, $client_secret)
	{
  	if (empty($ps['hmac']))
		{
			return false;
		}

		$hmac = $ps['hmac'];
		unset($ps['hmac']);
    ksort($ps);

		$pairs = [];
		foreach($ps as $k=>$v)
		{
			$k = str_replace('&', '%26', $k);
			$v = str_replace('&', '%26', $v);

			$k = str_replace('%', '%25', $k);
			$v = str_replace('%', '%25', $v);

			$k = str_replace('=', '%3D', $k);

    	$pairs[] = "$k=$v";

		}

		$s = implode('&', $pairs);
		$hmac_correct = hash_hmac('sha256', $s, $client_secret);

		if (isDebug())
		{
			//hr("HMAC validation");
			//hr($ps);
			//hr("Source for hmac: ".htmlspecialchars($s));
			//hr("CORRECT HMAC $hmac_correct)");
		}

		if ($hmac_correct == $hmac)
		{
    	return true;
		}
		else
		{
    	return false;
		}
	}

	public function recurringApplicationChargesPost($params)
	{
    $method = 'POST';
  	$url = '/admin/recurring_application_charges.json';
		return $this->call($method, $url, $params);
	}

	public function recurringApplicationChargesGetOne($id, $params = [])
	{
    $method = 'GET';
  	$url = "/admin/recurring_application_charges/$id.json";
		return $this->call($method, $url, $params);
	}

	public function recurringApplicationChargesPostActivate($id, $params = [])
	{
    $method = 'POST';
  	$url = "/admin/recurring_application_charges/$id/activate.json";
		return $this->call($method, $url, $params);
	}

	public function recurringApplicationChargesGet($params = [])
	{
    $method = 'GET';
  	$url = '/admin/recurring_application_charges.json';
		return $this->call($method, $url, $params);
	}


	public function countriesGet($params = [])
	{
    $method = 'GET';
  	$url = '/admin/countries.json';
		return $this->call($method, $url, $params);
	}

	public function shippingZonesGet($params = [])
	{
    $method = 'GET';
  	$url = '/admin/shipping_zones.json';
		return $this->call($method, $url, $params);
	}

	public function redirectPost($path, $target)
	{
		$params = ['redirect'=>['path'=>$path, 'target'=>$target]];
    $method = 'POST';
  	$url = '/admin/redirects.json';
		return $this->call($method, $url, $params);
	}

	public function redirectDelete($redirect_id)
	{
    $method = 'DELETE';
  	$url = "/admin/redirects/$redirect_id.json";
		return $this->call($method, $url, $params=[]);
	}

	public function redirectPutPath($redirect_id, $path)
	{
    $method = 'PUT';
  	$url = "/admin/redirects/$redirect_id.json";
    $params = ['redirect'=>['id'=>$redirect_id, 'path'=>$path]];
		return $this->call($method, $url, $params);
	}

	public function redirectPut($redirect_id, $path, $target)
	{
    $method = 'PUT';
  	$url = "/admin/redirects/$redirect_id.json";
		$rx = ['id'=>$redirect_id, 'path'=>$path, 'target'=>$target];
    $params = ['redirect'=> $rx];
		return $this->call($method, $url, $params);
	}

	public function redirectByPath($path)
	{
    $method = 'GET';
  	$url = "/admin/redirects.json";
		return $this->call($method, $url, $params=['path' => $path]);
	}


	public function customerPost($params)
	{
    $method = 'POST';
  	$url = '/admin/customers.json';
		return $this->call($method, $url, $params);
	}

	public function customerGetMany($params = [])
	{
    $method = 'GET';
  	$url = '/admin/customers.json';
		return $this->call($method, $url, $params);
	}

	public function smartCollectionGetOne($id, $params = [])
	{
    $method = 'GET';
  	$url = "/admin/smart_collections/$id.json";
		return $this->call($method, $url, $params);
	}

	public function customCollectionGetOne($id, $params = [])
	{
    $method = 'GET';
  	$url = "/admin/custom_collections/$id.json";
		return $this->call($method, $url, $params);
	}


	public function customCollectionGetMany($params = [])
	{
    $method = 'GET';
  	$url = '/admin/custom_collections.json';
		return $this->call($method, $url, $params);
	}

	public function customCollectionCountGet($params = [])
	{
    $method = 'GET';
  	$url = '/admin/custom_collections/count.json';
		return $this->call($method, $url, $params);
	}

	public function smartCollectionGetMany($params = [])
	{
    $method = 'GET';
  	$url = '/admin/smart_collections.json';
		return $this->call($method, $url, $params);
	}

	public function smartCollectionCountGet($params = [])
	{
    $method = 'GET';
  	$url = '/admin/smart_collections/count.json';
		return $this->call($method, $url, $params);
	}

	public function customerCountGet($params = [])
	{
    $method = 'GET';
  	$url = '/admin/customers/count.json';
		return $this->call($method, $url, $params);
	}

	public function customerAddressPost($id, $params)
	{
    $method = 'POST';
  	$url = "/admin/customers/$id/addresses.json";
		return $this->call($method, $url, $params);
	}

	public function customerPut($id, $params)
	{
    $method = 'PUT';
  	$url = "/admin/customers/$id.json";
		return $this->call($method, $url, $params);
	}

	public function metafieldPut($id, $val)
	{
    $method = 'PUT';
  	$url = "/admin/metafields/$id.json";
		$params = [];
		$params['metafield'] = [];
		$params['metafield']['id'] = $id;
		$params['metafield']['value'] = $val;
		//$params['metafield']['value_type'] = 'string';
		return $this->call($method, $url, $params);
	}

	public function customerGetMetafields($id)
	{
    $method = 'GET';
  	$url = "/admin/customers/$id/metafields.json";
		$params = [];
		return $this->call($method, $url, $params);
	}

	public function customerGet($id)
	{
    $method = 'GET';
  	$url = "/admin/customers/$id.json";
		$params = [];
		return $this->call($method, $url, $params);
	}


	public function customerSearchGet($params)
	{
    $method = 'GET';
    //$url = '/admin/customers/search.json?'.http_build_query($params);
  	$url = '/admin/customers/search.json';
		return $this->call($method, $url, $params);
	}

	public function transactionPost($id, $params)
	{
    $method = 'POST';
  	$url = "/admin/orders/$id/transactions.json";
		return $this->call($method, $url, $params);

	}

	public function orderPost($params)
	{
    $method = 'POST';
  	$url = "/admin/orders.json";
		return $this->call($method, $url, $params);
	}

	public function orderPut($id, $params)
	{
    $method = 'PUT';
  	$url = "/admin/orders/$id.json";
		return $this->call($method, $url, $params);
	}

	public function orderCancel($id, $params)
	{
    $method = 'POST';
  	$url = "/admin/orders/$id/cancel.json";
		return $this->call($method, $url, $params);
	}

	public function orderCountGet($params)
	{
    $method = 'GET';
  	$url = '/admin/orders/count.json';

		//$d_today = DatesHelper::today();
		//$d_start = DatesHelper::subDays($d_today, 58);
		//$params['created_at_min'] = $d_start;

		$params['status'] = 'any';

		return $this->call($method, $url, $params);
	}

	public function orderGetMany($params)
	{
    $method = 'GET';
  	$url = '/admin/orders.json';

		$d_today = DatesHelper::today();
		$d_start = DatesHelper::subDays($d_today, 58);
		//$params['created_at_min'] = $d_start;

		$params['status'] = 'any';
		return $this->call($method, $url, $params);
	}


	public function orderGetOne($id)
	{
    $method = 'GET';
  	$url = "/admin/orders/$id.json";
		$params = [];
		return $this->call($method, $url, $params);
	}

	public function orderGet($id)
	{
    $method = 'GET';
  	$url = "/admin/orders/$id.json";
		$params = [];
		return $this->call($method, $url, $params);
	}


	public function webHookPost($params)
	{
    $method = 'POST';
  	$url = '/admin/webhooks.json';
		return $this->call($method, $url, $params);
	}

	public function webHookDelete($id)
	{
    $method = 'DELETE';
  	$url = "/admin/webhooks/$id.json";
		$params = [];
		return $this->call($method, $url, $params);
	}

	public function webHookGet()
	{
    $method = 'GET';
  	$url = '/admin/webhooks.json';
		$params = [];
		return $this->call($method, $url, $params);
	}

	public function shopGet()
	{
    $method = 'GET';
  	$url = '/admin/shop.json';
		$params = [];
		return $this->call($method, $url, $params);
	}

	public function getToken()
	{

	}

	public function strip_html_tags( $text )
	{
    $text = preg_replace(
        array(
          // Remove invisible content
            '@<head[^>]*?>.*?</head>@siu',
            '@<style[^>]*?>.*?</style>@siu',
            '@<script[^>]*?.*?</script>@siu',
            '@<object[^>]*?.*?</object>@siu',
            '@<embed[^>]*?.*?</embed>@siu',
            '@<applet[^>]*?.*?</applet>@siu',
            '@<noframes[^>]*?.*?</noframes>@siu',
            '@<noscript[^>]*?.*?</noscript>@siu',
            '@<noembed[^>]*?.*?</noembed>@siu',
          // Add line breaks before and after blocks
            '@</?((address)|(blockquote)|(center)|(del))@iu',
            '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
            '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
            '@</?((table)|(th)|(td)|(caption))@iu',
            '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
            '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
            '@</?((frameset)|(frame)|(iframe))@iu',
        ),
        array(
            ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
            "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
            "\n\$0", "\n\$0",
        ),
        $text );
    return strip_tags( $text );
	}

	public static $last_call_time;

	public static $cache_get_requests_enabled = false;
	public static $cache_get_requests_seconds = 3600;

	public function call($method, $url, $params = [])
	{
		if (static::$cache_get_requests_enabled && 'get' == strtolower($method))
		{
			$kc = [__CLASS__, $this->shop_host, __METHOD__, $url, $params];
			$v = Yii::$app->cache->get($kc);
			if ($v !== false)
			{
				return $v;
			}
		}


		if (!empty(self::$last_call_time))
		{
			$dt = round(microtime(true) - self::$last_call_time, 3);

			$min_dt = 1;
			if ($dt < $min_dt)
			{
				$sleep_time = round($min_dt - $dt, 3);
				$m = "$dt sec since last run, waiting $sleep_time sec before Shopify API call";
				if (Yii::$app->cron)
				{
					Yii::$app->cron->addMsg($m);
				}

        usleep($sleep_time * 1000 * 1000);
			}
		}

		$logger = new Logger('my_logger');
		$logger->pushHandler(new MonologToYii());

		$stack = HandlerStack::create();
		$stack->push(
	    Middleware::log(
	        $logger,
	        new MessageFormatter('URI: {uri} \n DATA: {req_body}\n RESPONSE  HEADERS {res_headers}')
	    )
		);

		$sal = new LogShopifyApi();
   	$sal->shop_id = $this->shop_id;
    $sal->last_call_time =
			self::$last_call_time - floor(time()/84600)*84600;
    $sal->stack = debugPrintCallingFunction();


    //$stack->push([$sal, 'log'], get_class($sal));
    //$stack->push($sal->log_f());
    $stack->push(Middleware::tap($before = null, [$sal, 'log']), get_class($sal));

		$client = new GuzzleClient([
    	'base_uri' => "https://$this->shop_host",
    	'timeout'  => 10.0,
			'handler' => $stack,
		]);

		$hs = [];
		$hs['Content-Type'] = "application/json";
		if ($this->token)
		{
			$hs['X-Shopify-Access-Token'] = $this->token;
		}

		//try
		//{

		$rqx = [];
    $rqx['headers'] = $hs;

		if (!empty($params))
		{
			if ('get' == strtolower($method))
			{
      	$url .= "?".http_build_query($params);
			}
			else
			{
    		$rqx['json'] = $params;
			}
		}

		if (Yii::$app->cron)
		{
			Yii::$app->cron->addMsg("<b>Shopify Call:</b> $url");
		}

			$response = $client->request($method, $url, $rqx);
		/*}
		catch(GuzzleHttp\Exception\GuzzleException $e)
		{
			$rq = $e->getRequest();
			$rs = $e->getResponse();
			//hr($r);
			$t = (string)$rq->getUri();//getRequestTarget();
			$b = (string)$rq->getBody();

			hr("URI: $t");
			hr("BODY: $b");
			print $this->strip_html_tags((string)$rs->getBody());
			exit;

			//hre();
    	//hre($e);
		} */

		//$response = $request->send();

		$b = $response->getBody();
    $b = (string)$b;

		if ($ba = json_decode($b, true))
		{
			if (!is_array($ba))
			{
				throw new \Exception("Error on decoding response ($b)");
			}
		}

		self::$last_call_time = microtime(true);

		if (static::$cache_get_requests_enabled && 'get' == strtolower($method))
		{
			Yii::$app->cache->set($kc, $b, static::$cache_get_requests_seconds);
		}

		if (!empty($_GET['rx99as1']))
		{
    	Yii::$app->cron->addMsg($ba, $lim = false);
		}

		return $ba;
	}

	public function scriptTagGet()
	{
    $method = 'GET';
  	$url = '/admin/script_tags.json';
		$params = [];
		return $this->call($method, $url, $params);
	}

	public function scriptTagPost($params)
	{
    $method = 'POST';
  	$url = '/admin/script_tags.json';
		return $this->call($method, $url, $params);
	}

	public function scriptTagDelete($id)
	{
    $method = 'DELETE';
  	$url = "/admin/script_tags/$id.json";
		$params = [];
		return $this->call($method, $url, $params);
	}


	public function getSafe($call, $params)
	{
  	try
		{
    	$r = call_user_func_array($call, $params);
			return $r;
		}
		catch(\GuzzleHttp\Exception\ClientException $e)
		{
			$rs = $e->getResponse();

			if (404 == $rs->getStatusCode())
			{
				return false;
			}

			throw $e;
		}
	}

	public function productGetExists($id)
	{
  	try
		{
    	$dj = $this->productGet($id);
			//$dj = json_decode($j, true);
			return $dj['product'];
			//return true;
		}
		catch(\GuzzleHttp\Exception\ClientException $e)
		{
			$rs = $e->getResponse();

			if (404 == $rs->getStatusCode())
			{
				return false;
			}

			throw $e;
		}
	}


	public function productGet($id, $params = [])
	{
    $method = 'GET';
  	$url = "/admin/products/$id.json";
		return $this->call($method, $url, $params);
	}

	public function productPost($params)
	{
		$params = ['product' => $params];
    $method = 'POST';
  	$url = "/admin/products.json";
		return $this->call($method, $url, $params);
	}


	public function productGetOne($id, $params = [])
	{
    $method = 'GET';
  	$url = "/admin/products/$id.json";
		return $this->call($method, $url, $params);
	}

	public function productGetMetafields($id, $params = [])
	{
    $method = 'GET';
  	$url = "/admin/products/$id/metafields.json";
		return $this->call($method, $url, $params);
	}

	public function productGetCustomCollections($id, $params = [])
	{
    $method = 'GET';
  	$url = "/admin/custom_collections.json?product_id=$id";
		return $this->call($method, $url, $params);
	}

	public function productGetSmartCollections($id, $params = [])
	{
    $method = 'GET';
  	$url = "/admin/smart_collections.json?product_id=$id";
		return $this->call($method, $url, $params);
	}

	public function productCountGet($params)
	{
    $method = 'GET';
  	$url = '/admin/products/count.json';
		return $this->call($method, $url, $params);
	}

	public function productGetMany($params)
	{
    $method = 'GET';
  	$url = '/admin/products.json';
		return $this->call($method, $url, $params);
	}

}
