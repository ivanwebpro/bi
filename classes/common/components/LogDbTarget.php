<?php

namespace common\components;

use yii\helpers\VarDumper;

class LogDbTarget extends \yii\log\DbTarget
{
  public function export()
  {
		//hre("AA");

    $tableName = $this->db->quoteTableName($this->logTable);
    $sql = "INSERT INTO $tableName ([[level]], [[category]], [[log_time]], [[prefix]], [[message]], [[time]])
            VALUES (:level, :category, :log_time, :prefix, :message, :time)";
    $command = $this->db->createCommand($sql);
    foreach ($this->messages as $message) {
        list($text, $level, $category, $timestamp) = $message;
        if (!is_string($text)) {
            // exceptions may not be serializable if in the call stack somewhere is a Closure
            if ($text instanceof \Exception) {
                $text = (string) $text;
            } else {
                $text = VarDumper::export($text);
            }

        }

				$t = $this->db
					->createCommand("SELECT FROM_UNIXTIME($timestamp)")
					->queryScalar();
				//hre($t);

				$bv = [
            ':level' => $level,
            ':category' => $category,
            ':log_time' => $timestamp,
            ':time' => $t,//new \yii\db\Expression("FROM_UNIXTIME($timestamp)"),
									//strftime( , $timestamp),
            ':prefix' => $this->getMessagePrefix($message),
            ':message' => $text,
        ];

				//hre($bv);
        $command->bindValues($bv);

				//hre($command);
				$command->execute();
    }
	}
}