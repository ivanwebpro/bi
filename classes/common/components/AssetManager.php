<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use yii\helpers\Url;


class AssetManager extends \yii\web\AssetManager
{
  protected function loadBundle($name, $config = [], $publish = true)
  {
		if (stristr($name, 'bootstrap\\'))
		{
      //hre(func_get_args()); exit;
		}

		if (stristr($name, 'bootstrap4'))
		{
			//hr(func_get_args()); exit;
			$config['sourcePath'] = '@npm/bootstrap/dist';
		}

		//
		return parent::loadBundle($name, $config, $publish);
		/*
      if (!isset($config['class'])) {
          $config['class'] = $name;
      }
      /* @var $bundle AssetBundle */
			/*
      $bundle = Yii::createObject($config);
      if ($publish) {
          $bundle->publish($this);
      }

      return $bundle;*/
  }

}
