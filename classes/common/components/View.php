<?php

namespace common\components;

use Yii;
use \yii\web\View as YiiView;

class View extends YiiView
{
	public $black = false;
	public $menu_display = true;
	public $custom_css_split_test_create = false;
	public $body_title;



	public function appendTime($url)
	{
		$f = $_SERVER['DOCUMENT_ROOT'].$url;
		if (file_exists($f))
		{
			$t = filemtime($f);
			$url .= "?t=$t";
		}
		return $url;
	}

	public function appendHost($url)
	{
		if (!stristr($url, 'http:') && !stristr($url, 'https:')
					&& !stristr($url, '//'))
		{
			$proto = "http";
			if (Yii::$app->request->isSecureConnection)
    		$proto = "https";
			$url = $proto.'://'.$_SERVER['HTTP_HOST']."".$url;
		}

		return $url;
	}

	public function registerJsFileByFilePath($url, $options = [], $key = null)
	{
  	$dr = $_SERVER['DOCUMENT_ROOT'];
		$dr = str_ireplace('\\', '/', $dr);
		$url = str_ireplace('\\', '/', $url);
		$url = str_ireplace($dr, '', $url);
    return $this->registerJsFile($url, $options, $key);
	}

	public function registerJsFile($url, $options = [], $key = null)
  {
		$url = $this->appendTime($url);
		$url = $this->appendHost($url);
		parent::registerJsFile($url, $options, $key);
	}

	public function registerCssFile($url, $options = [], $key = null)
  {
		$url = $this->appendTime($url);
		$url = $this->appendHost($url);
		parent::registerCssFile($url, $options, $key);
	}

	public $bs3 = false;
	public $deny_bs3 = false;
	public $deny_bs4 = false;
	public $bs4 = false;

	public function registerAssetBundle($name, $position = null)
  {
		if ($this->deny_bs3)
		{
			if ('yii\bootstrap\BootstrapAsset' == $name || 'yii\bootstrap\BootstrapPluginAsset' == $name)
			{
				$msg = "Name: $name, UI styles conflict";
				Yii::warning($msg, __METHOD__);
				hre($msg); exit;
			}
		}

		if ($this->deny_bs4)
		{
    	if (stristr($name, "boostrap4"))
			{
				$msg = "Name: $name, UI styles conflict";
				Yii::warning($msg, __METHOD__);
				hr($msg); exit;
			}
		}

		return parent::registerAssetBundle($name, $position);

	}


}