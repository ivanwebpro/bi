<?php

namespace common\components;

use yii\web\Application as YiiApplication;
use common\helpers\Url;
use common\models\Order;
use Yii;
use account\models\AppCfg;
use account\helpers\CronHelper;

class Application extends YiiApplication
{
	public $shopify_ns = 'a_app';
	public $shopify_update_form_secret;

	public $shopify_client_id;
	public $shopify_client_secret;
	public $shopify_scope;
	public $redirect_after_login;

	public $debug = false;
	public $cron = 0;
	public $guid = '';
	public $shopify_proxy_slug = '';
	public $shopify_params = [];



	const SESSION_VAR_AUTH_STATE = 'shopify_auth_state';
	const SESSION_VAR_CHARGE = 'SESSION_VAR_CHARGE';

	protected function bootstrap()
  {
		parent::bootstrap();
		$this->cron = new \account\helpers\CronHelper(__METHOD__);
	}

	public function getRecurlyApi()
	{
		return new \common\apis\RecurlyApi;
	}

	public function getAppCfg()
	{
  	$model = AppCfg::factory();
		return $model;
	}





}