<?php

namespace common\components;

use common\components\Options;

class EmailTargetWithThrottle extends \yii\log\EmailTarget
{
	public $min_interval = 1800;

  public function export()
  {
		$opt_name = __METHOD__."-last-send";

		if (Options::st()->getTimeDeltaFromNow($opt_name) < $this->min_interval)
			return;

		parent::export();

		Options::st()->setNow($opt_name);
	}
}