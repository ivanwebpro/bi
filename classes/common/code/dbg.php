<?php

define("CRON_EVENT", "CRON_EVENT");
define("SESSION_VAR_CHARGE", "SESSION_VAR_CHARGE");

define("DT_FMT", 'Y-m-d H:i:s');
define("DT_FMT_FILE", 'Y-m-d_H-i-s');
define("DATE_FMT", 'Y-m-d');


$app_root = appRoot();

function cookieValiationKey($dir)
{
	$cookie_validation_key_file = $dir.'/.cookie_validation_key';
	//hre("cookie_validation_key_file: $cookie_validation_key_file");

	if (!file_exists($cookie_validation_key_file))
	{
		$cookie_validation_key = md5(time());
		file_put_contents($cookie_validation_key_file, $cookie_validation_key);
	}
	else
	{
		$cookie_validation_key = file_get_contents($cookie_validation_key_file);
	}

	return $cookie_validation_key;
}

if (true)//isLocalHost() || !empty($_REQUEST['dpf']))
{
	defined('YII_DEBUG') or define('YII_DEBUG', true);
}

defined('YII_ENV') or define('YII_ENV', 'dev');


if (stristr($_SERVER['REQUEST_URI'], 'cron'))
{

ini_set('session.use_cookies', '0');

class FakeSessionHandler implements SessionHandlerInterface
{
  private $savePath;

  function open($savePath, $sessionName)
  {
  	return true;
  }

  function close()
  {
  	return true;
  }

  function read($id)
  {
  	return '';
  }

  function write($id, $data)
  {
    return true;
  }

  function destroy($id)
  {
  	return true;
  }

  function gc($maxlifetime)
  {
  	return true;
  }
}

$handler = new FakeSessionHandler();
session_set_save_handler($handler, true);
}

if (isset($_GET['i']))
{
	phpinfo();
	exit;
}


function cm($msg)
{
	Yii::$app->cron->addMsg($msg);
}

function appRootWeb()
{
	$proto = "http";
	if (Yii::$app->request->isSecureConnection || !isLocalHost())
  	$proto = "https";

  if (empty($_SERVER['HTTP_HOST']))
	{
  	$h = '';
	}
	else
	{
		$h = $_SERVER['HTTP_HOST'];
	}

	if (!$h)
	{
  	return $p;
	}
	
	//return Yii::getAlias($css_alias);
	$dr = $_SERVER['DOCUMENT_ROOT'];
	$dx = dirname(dirname(dirname(__DIR__)));
	$dr = str_ireplace('\\', '/', $dr);
	$dx = str_ireplace('\\', '/', $dx);
  $p = str_ireplace($dr, '', $dx);


	$u = "$proto://".$h.$p;
	return $u;
}

function appRoot()
{
	$r =  dirname(dirname(dirname(__DIR__)));
	//hre($r);
	return $r;
}

function appClassesRoot()
{
	return appRoot()."/classes";
}


function isDebug()
{
	if (isLocalHost()
				|| !empty($_COOKIE['XDEBUG_SESSION'])
				|| !empty($_REQUEST['dbg'])
			)
	{
  	return true;
	}
	else
	{
  	return false;
	}

}

function isLocalHost()
{
	$is_localhost = false;
	if (('127.0.0.1' == $_SERVER['REMOTE_ADDR']
			|| '::1' == $_SERVER['REMOTE_ADDR'])
			|| !empty($_REQUEST['loc']))
	{
		$is_localhost = true;
	}

	return $is_localhost;
}

function hrb($s = '')
{
	print "<b>";
	hr($s);
	print "</b>";
}

function hrr($s = '')
{
	print "<b style ='color:red'>";
	hr($s);
	print "</b>";
}

function pre($s = '')
{
	print "<pre>";
	hr($s);
	print "</pre>";
}

function hre($s = '')
{
	print debugPrintCallingFunction();
	hr($s);
	exit;
}

function hrt($s = '')
{
	print "<hr><b>".date('c')."</b>";
	hr($s);
}

function hro($s = '')
{
	ob_start();
	hr($s);
	return ob_get_clean();
}

function hr($s = '')
{
	//var_dump(debugPrintCallingFunction());

	$trace=debug_backtrace();

	if (isset($trace[1]))
	{
		$caller = $trace[1];
		if ('dbg' == $caller['function'])
			$caller = $trace[2];

		//var_dump($caller);

		$caller_str = "Called by {$caller['function']}";
		if (isset($caller['class']))
    	$caller_str .= " in {$caller['class']}";

		if (isset($caller['line']))
			$caller_str .= ", line $caller[line]";
	}
	else
		$caller_str = "Called by MAIN";


	//$caller_str = debugPrintCallingFunction();
	$caller_str = '';

	if (is_scalar($s))
	{
		//print "<hr><i>$caller_str</i>, ".date('c').": $s<hr>";
		print "<hr>$s<hr>";
	}
	else
	{
		//print "<hr><i>$caller_str</i>, ".date('c').": <pre>";
		print "<hr><pre>";
		var_dump($s);
		print "</pre><hr>";
	}
}

/*function hr($s = '')
{
		if (is_scalar($s))
			print "<hr>$s<hr>";
		else
		{
			print "<hr>";
			var_dump($s);
			print "<hr>";
		}
}*/


function dbg($s = '')
{
	if (isset($_GET['dbg']))
 		hr($s);
}

function tr_vd($s = '')
{
	hr($s);
	hr("Back trace: <br>".debugPrintCallingFunction());
}

function fmt_mkb($mem_usage)
{
	if ($mem_usage < 1024)
            echo $mem_usage." bytes";
        elseif ($mem_usage < 1048576)
            echo round($mem_usage/1024,2)." kilobytes";
        else
            echo round($mem_usage/1048576,2)." megabytes";
}

function echo_memory_usage() {
			echo "<hr>";
        $mu = memory_get_usage(true);
				fmt_mkb($mu);
				echo " // ";
        $mp = memory_get_peak_usage(true);
				fmt_mkb($mp);

        echo "<hr>";
    }

$log_errors = true;
$display_errors = true;

function myErrorHandler($errno, $errstr, $errfile, $errline)
{
	$job_data = error_get_job_data();

//Job Data ($job_data) <br>
	if (strstr($errfile, 'vendor\yiisoft\yii2\views\errorHandler\exception.php'))
		return;

	if (strstr($errfile, 'vendor/yiisoft/yii2/views/errorHandler/exception.php'))
		return;


	$text = "errno: $errno, errstr: $errstr,
												errfile: $errfile, errline: $errline".
												"<br>Back trace: <br>".
												debugPrintCallingFunction();

	global $display_errors;
	if ($display_errors)
		hr("<b style='color:red'>$text</b>");

	global $log_errors;
	if ($log_errors)
		add_log($text);
}


function add_log_hr($t)
{
	add_log($t);
	hr($t);
}

function add_log($text)
{
	if (!is_scalar($text))
		$text = json_encode($text);

	$tbl = 'iv2_log_error';

	$log = array();
	$log['time'] = 'NOW()';
	$log['err_data'] = $text;
	$log['request'] = json_encode($_REQUEST);
	make_insert($tbl, $log);
}


function add_to_log($text)
{
	add_log($text);
}


//set_error_handler('myErrorHandler');

$job_data = 'not set';

function error_set_job_data($s)
{
	global $job_data;
	$job_data = $s;
}

function error_get_job_data()
{
	global $job_data;
	return $job_data;
}


function debugPrintCallingFunction()
{
    $file = 'n/a';
    $func = 'n/a';
    $line = 'n/a';
    $debugTrace = debug_backtrace();
    if (isset($debugTrace[1])) {
        $file = isset($debugTrace[1]['file']) ? $debugTrace[1]['file'] : 'n/a';
        $line = isset($debugTrace[1]['line']) ? $debugTrace[1]['line'] : 'n/a';
    }
    if (isset($debugTrace[2])) $func = $debugTrace[2]['function'] ? $debugTrace[2]['function'] : 'n/a';

    $out = "call from $file, $func, $line";

		$trace = '';
		$bt = debug_backtrace();
		$sp = 0;
		foreach($bt as $k=>$v)
    {
			//pre($v);
        extract($v);

			$args = '';
			if (isset($v['args']))
			{
				$args2 = array();
				foreach($v['args'] as $k => $v)
				{
        	if (!is_scalar($v))
						$args2[$k] = "Array";
					else
            $args2[$k] = $v;
				}

				$args = implode(", ", $args2);
			}


        $file=substr($file,1+strrpos($file,"/"));
        //if($file=="db.php")continue; // the db object
        $trace.=str_repeat("&nbsp;",++$sp); //spaces(++$sp);
        $trace.="file=$file, line=$line,
										function=$function(".
										var_export($args, true).")<br>";
    }

		$out .= $trace;
    return $out;
}

class Api_Exception_NotFound extends Exception
{
}

class v2_Exception extends Exception
{
	private $extra_messages = array();

	public function addExtraMessage($msg)
	{
		$this->extra_messages[] = $msg;
	}

	public function getExtraMessage($msg)
	{
		return implode(' // ', $this->extra_messages);
	}

	public function __construct($message = null, $code = 0, Exception $previous = null)
		{
    	// некоторый код
      $file_path = $this->getFile();
      $file_path = str_replace($_SERVER['DOCUMENT_ROOT'].'/',
																	'', $file_path);
			$file_path = str_replace('/', '.', $file_path);
			$file_path = str_replace('.php', '', $file_path);

			$category = $file_path."->".get_class($this);

			$message2 = $message." | ".$this->__toString();

			add_log("$message2, $category");

      // убедитесь, что все передаваемые параметры верны
    	parent::__construct($message, $code, $previous);
    }
}
