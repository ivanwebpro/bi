<?php

namespace common\traits;

trait Alias
{
	public static function byAlias($alias)
	{
  	return static::findOne(['alias' => $alias]);
	}
}