<?php

namespace common\widgets;

use kartik\daterange\DateRangePicker as KDateRangePicker;
class DateRangePicker extends KDateRangePicker
{
	/*public $title = '';
	public $active_id = 10;
	public $els = [];
	public $url_fld = 'id';
	public $type;
	public $field;
	public $default;


	public function init()
	{
		parent::init();
	} */

	protected function initRange()
	{
  	parent::initRange();
    //return;

		if (!empty($this->pluginOptions['rangesOverload']))
		{


			$opts = $this->pluginOptions;
			$opts['ranges'] = $this->pluginOptions['rangesOverload'];

      $range = [];

        foreach ($opts['ranges'] as $key => $value) {
            if (!is_array($value) || empty($value[0]) || empty($value[1])) {
                throw new InvalidConfigException(
                    "Invalid settings for pluginOptions['ranges']. Each range value must be a two element array."
                );
            }
            $range[$key] = [static::parseJsExpr($value[0]), static::parseJsExpr($value[1])];
        }
        $this->pluginOptions['ranges'] = $range;


		}
	}
}
