<?php

use common\helpers\Html;
use common\helpers\Url;
use account\models\Candle;

if ($this->title)
{
?>
<h2><?=$this->title?></h2>
<?php
}

	$els = $this->els;

	foreach($els as $k=>$v)
	{
		$opts = ['class' => 'btn btn-default', 'style' => 'margin-bottom: 10px'];

		if ($k == $this->active_id)
		{
    	$opts['class'] = 'btn btn-success';
		}

		$u = [];//$_GET;
		$u[0] = '';

		$u[$this->url_fld] = $k;
		echo Html::a($v, $u, $opts);
		echo "&nbsp;&nbsp;&nbsp;";
	}
