<?php

use common\helpers\Html;
use common\helpers\Url;
use account\models\Candle;

if ($this->title)
{
?>
<h2><?=$this->title?></h2>
<?php
}

	$objs = $this->objs;
	$n_fld = $this->obj_name_fld;

	foreach($objs as $obj)
	{
		$opts = ['class' => 'btn btn-success'];

		if ($obj->id == $this->active_id)
		{
    	$opts = ['class' => 'btn btn-primary'];
		}

		$u = $_GET;
		$u[0] = '';

		$u[$this->url_fld] = $obj->id;
		echo Html::a($obj->$n_fld, $u, $opts);
		echo "&nbsp;&nbsp;&nbsp;";
	}
?>
<hr>