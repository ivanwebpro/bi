<?php

use common\helpers\Url;
use common\helpers\Html;

$u = ['app/desktop-notifications-json'];
if (!empty($_GET['dt']))
{
	$u['test'] = 1;
}

$u = Url::to($u);


echo Html::tag('div', '', ['desktop_notification_url' => $u]);

