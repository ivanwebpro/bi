"use strict";

// request permission on page load
$(function()
{
	$('[name="Shop[notify_desktop_enabled]"]').change(function()
	{
		if (!Notification)
		{
    	alert('Desktop notifications is not available in your browser. Try Google Chrome');
    	return;
  	}

		if (Notification.permission !== "granted")
		{
    	Notification.requestPermission();
		}
	});

	function notifyMe(body)
	{
  	if (Notification.permission !== "granted")
		{
    	Notification.requestPermission();
		}
  	else
		{
	    var notification = new Notification("Who's selling what", {
  	    //icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
    	  body: body,
    	});
		}

    //notification.onclick = function () {
      //window.open("url:....");
    //};
  }


	var $div = $('[desktop_notification_url]')
  var url = $div.attr('desktop_notification_url');

	function updateDesktopNotifications()
	{
		console.log('run loading', (new Date));

		var a_cfg = {};
		a_cfg.method = 'get';
		a_cfg.url = url;
		a_cfg.data = {};
		a_cfg.dataType = 'json';

		a_cfg.success = function(data)
		{
			if (data.enabled && data.message)
			{
      	notifyMe(data.message);
			}
		};

		$.ajax(a_cfg);
	}

	setInterval(updateDesktopNotifications, 10*1000);

});


