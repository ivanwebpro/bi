<?php

use common\helpers\Html;
use common\helpers\Url;
use account\models\Candle;

$class = 'ajax_load';

$var = "ajax_load_".(static::$n_widget);
if (!empty($this->id))
{
	$var = "ajax_load_".($this->id);
}
$sp = '';

$a = Html::a("URL", $this->url, ['target'=>'blank_']);

$sp .= "<div class='row'>";


if ($this->refresh_checkbox)
{
	$sp .= "<div class='col-xs-4'>";
	$sp .= Html::checkbox($var, $checked = $this->refresh_auto, $options = ['label' =>
"autorefresh [$var], $a", 'encode' => false]);
	$sp .= "</div>";
}


$sp .= "<div class='col-xs-10'>";


$sp .= "<p class='text-right' style='font-style: italic; display: none' data-object=lu-wrapper>Last updated: <span data-ajax-load-updated-time-id='$var'></span></p>";
$sp .= "</div>";

if ($this->refresh_btn)
{
	$sp .= "<div class='col-xs-2'>";
	$sp .= "<p>";
	$sp .= Html::a(
		//"Refresh",
		"<span class='glyphicon glyphicon-refresh'></span>",
		'#',
		['class'=>'btn btn-info', 'data-refresh' => 1,
		'data-target-id' => $var]);
	$sp .= "</p>";
	$sp .= "</div>";
}


$sp .= "</div>";


$sp .= Html::tag('div', '', [
	'class'=> $class,
	'data-var' => $var,
	'id' => $var,
]);


$data = [];
$data['id'] = $var;
$data['url'] = Url::to($this->url);
$data['refresh_secs'] = $this->refresh_secs;
$data['refresh_auto'] = $this->refresh_auto;
$data['refresh_hide_sel'] = $this->refresh_hide_sel;

$sp .= "<script>";
$sp .= "var $var = ".json_encode($data, JSON_PRETTY_PRINT).";";
$sp .= "</script>";


echo $sp;

