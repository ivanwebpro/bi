"use strict";

var ajax_load_func = [];
var ajax_load_cfg = [];

jQuery(document).ready(function()
{
	var f_class = "ajax-load-focus";

	$(document).on("blur", 'input', function() {
		$('input').removeClass(f_class);
	});

	$(document).on("focus", 'input', function() {
		$(this).addClass(f_class);
	});

	$('.ajax_load').each(function(i, el)
	{
		var $el = $(el);

		var var_name = $el.attr('data-var');
		var id = var_name;//"ajax_load_" + i;
		ajax_load_cfg[id] = window[var_name];
    ajax_load_cfg[id]['id'] = id;
    ajax_load_cfg[id]['var_name'] = var_name;
    ajax_load_cfg[id].el = $el;

		ajax_load_func[id] = function(force, cb)
		{
			if (!force)
			{
				force = false;
			}

			/*if (!force)
			{
				var $f = ajax_load_cfg[id].el.find("." + f_class);
       	if ($f.length > 0)
				{
       		setTimeout(ajax_load_func[id], ajax_load_cfg[id]['refresh_secs']*1000);
					return;
				}

				var $chk = $("[name="+id+"]");

				if (!$chk.prop('checked'))
				{
        	setTimeout(ajax_load_func[id], ajax_load_cfg[id]['refresh_secs']*1000);
					return;
				}
			} */

			 $.ajax({
        	url: ajax_load_cfg[id]['url'],
					success: function(data)
					{
						var sel = ajax_load_cfg[id].refresh_hide_sel;
						if (sel)
						{
            	$(sel).hide();
						}

						ajax_load_cfg[id].el.html(data);
					},
					error: function(xhr, textStatus, errorThrown)
					{
						ajax_load_cfg[id].el.html(xhr.responseText);
					},
					complete: function(xhr, st)
					{
						//console.log('complete: ' + id);
						//console.log('ra', ajax_load_cfg[id]['refresh_auto']);
						//console.log('f', force);
						//console.log('!f', !force);
						//console.log((!force && ajax_load_cfg[id]['refresh_auto']));


						if (!force && ajax_load_cfg[id]['refresh_auto'])
						{
							var to = ajax_load_cfg[id]['refresh_secs']*1000;
							//console.log('set to', to);
        			setTimeout(ajax_load_func[id], to);
						}

						var $lut = $('[data-ajax-load-updated-time-id="'+id+'"]');
            $lut.text(new Date);
						$lut.parent('[data-object=lu-wrapper]').show();


						if (cb) cb();
					}
				});

		};

		if (ajax_load_cfg[id]['refresh_auto'])
		{
			var x_force=false;
			//var x_force=true;
    	ajax_load_func[id](x_force);
		}
		else
		{
			var x_force=true;
    	ajax_load_func[id](x_force);
		}
	});


	$('[data-refresh]').click(function(e)
	{
		e.preventDefault();
		var $btn = $(this);
		var id = $btn.attr('data-target-id');
		//console.log(id);
		//console.log(ajax_load_cfg);
		ajax_load_cfg[id].el.html('<div class="progress" data-widget=progress-reload><div class="progress-bar progress-bar-striped active" role="progressbar" style="width: 100%"></div></div>');
		//loading_animation_show();

		var x_force=true;
    ajax_load_func[id](x_force);
	});

});