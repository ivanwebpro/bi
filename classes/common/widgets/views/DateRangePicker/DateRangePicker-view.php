<?php
	$u = appRootWeb()."/libs/vue/vue.dev.js";
	echo "<script src='$u'></script>";
?>

<script src="https://unpkg.com/vue-rangedate-picker"></script>

<div id='app'>
	<div class="daterange-wrapper">

		<vue-rangedate-picker @selected="onDateSelected" i18n="EN" righttoleft="true" />

	</div>
</div>
