"use strict";

var loading_animation_enabled = true;
var loading_animation_show;
var loading_animation_hide;

jQuery(function()
{
	var $ = jQuery;


	function _loadingAnimationHide()
	{
		var $w = $('.x-loading-animation-wrapper');
		$w.hide();
	}

	function _loadingAnimationShow(msg)
	{
		if (typeof msg === "undefined")
			var msg = '';

		$('.x-loading-animation-msg').html(msg);

		var winWidth = $(window).width();
		var winHeight = $(window).height();
		var docHeight = $(document).height();
		var scrollPos = $(window).scrollTop();

		var width = 200;
	  /*if (0.8*winWidth < width)
		{
			width = Math.round(0.8 * winWidth);
		} */

		var $dlg = $('.x-loading-animation-modal-box');

		var dlgLeft = (winWidth - width) / 2;
    var dlgHeight = $dlg.height();

		if (dlgHeight > winHeight)
			var disTop = scrollPos + 5;
		else
		 	var disTop = scrollPos + (winHeight - dlgHeight)/4;

		$dlg.css({
			'width': width + 'px',
			'left': dlgLeft + 'px',
			'top': disTop + 'px',
			'z-index': loadingAnimationGetNextZIndex()+20,
			});

		var $fade = $(".x-loading-animation-modal-fade");
		$fade.css(
			{
				'width': winWidth + 'px',
				'height': docHeight + 'px'
			});

		$fade.show();
		$dlg.show();
		var $w = $('.x-loading-animation-wrapper');
		$w.show();
	}

	function loadingAnimationGetNextZIndex()
	{
		var index_highest = 10;
		$('*').each(function()
		{
	    var index_current = parseInt($(this).css("z-index"), 10);
	    if(index_current > index_highest)
			{
	    	index_highest = index_current;
	    }
		});

		return index_highest + 1;
	}

loading_animation_show = function()
{
	if (!loading_animation_enabled)
		return;

	_loadingAnimationShow();
	//$('.modal').not('#loading_modal').modal('hide');
	//$('#loading_modal').modal('show');
};

//$(document).on('click', function() { loading_animation_show(); });

$(document).on('click', 'a.wait-animation', function()
{
	loading_animation_enabled = false;

	setTimeout(function() {
  	loading_animation_enabled = true;
	}, 2*1000);
});

$(window).on("beforeunload", function()
{
	//console.log("beforeunload");
  loading_animation_show();
});

loading_animation_hide = function()
{
	//$('#loading_modal').modal('hide');
	_loadingAnimationHide();
};

$(document).on("click", function()
{
	loading_animation_hide();
});


$(document).on("pjax:end", function()
{
	loading_animation_hide();
});

$(document).on("pjax:start", function()
{
	loading_animation_show();
});

});


