"use strict";



jQuery(document).ready(function()
{
	var $ = jQuery;

	var pg = '<div class="progress" style="height: 32px;" data-widget=progress-reload><div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" style="width: 100%; "></div></div>';


	$(document).on('click', '[data-ajax-toggle]', function(e)
	{
		e.preventDefault();

		var $el = $(this);
		var ajax_url = $el.attr('href');

		var $w = $el.closest('[data-ajax-toggle-wrapper]')

		$w.css('width', $w.width() +'px');

		$.ajax({
	   	url: ajax_url,
			data: {ajax: 1},

			beforeSend: function()
			{
    		$w.html(pg);
				return true;
			},

			success: function(data)
			{
				$w.html(data);
			},
			error: function (request, status, error)
			{
				$w.html(request.responseText);
			},
			complete: function(xhr, st)
			{
			}
		 });

	});


});