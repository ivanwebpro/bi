<?php

namespace common\widgets;


class SelObj extends base\Widget
{
	public $title = '';
	public $active_id = 10;
	public $field = 10;
	public $objs = [];
	public $obj_name_fld = 'name';
	public $url_fld = 'id';
	public function init()
	{
		parent::init();
	}
}
