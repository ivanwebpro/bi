<?php

namespace common\widgets;

use common\helpers\Html;

use Yii;
use yii\base\InvalidCallException;
use yii\base\Widget;
use yii\base\Model;
use yii\widgets\ActiveFormAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;

class ActiveForm extends \yii\widgets\ActiveForm
{
	public $action_blank = false;
	public $no_auto_require = false;
  //public $fieldClass = 'common\widgets\ActiveField';
  public $enableClientScript = false;

	public function submitButton($label = 'Save', $opts=['class' => 'btn btn-primary'] )
	{
		return Html::submitButton($label, $opts);
	}

	public static function begin($config = [])
	{
		if (!isset($config['enableClientValidation']))
			$config['enableClientValidation'] = false;

    if (!isset($config['fieldConfig']))
			$config['fieldConfig'] = [];

    //if (!isset($config['fieldConfig']['template']))
			//$config['fieldConfig']['template'] = "{label}<span class='colon'>:</span>\n{input}\n{hint}\n{error}";

		$b = parent::begin($config);

		//if ($config['action'])
		//if (empty($config['action_blank']))
    return $b;

		//$b = str_replace(" action", " action='' old-action", $b);
		//return $b;

	}

	public function run()
	{
		if (empty($this->action_blank))
			return parent::run();

        if (!empty($this->_fields)) {
            throw new InvalidCallException('Each beginField() should have a matching endField() call.');
        }

        $content = ob_get_clean();
        echo Html::beginForm($this->action, $this->method, $this->options);
        echo $content;

        if ($this->enableClientScript) {
            $id = $this->options['id'];
            $options = Json::htmlEncode($this->getClientOptions());
            $attributes = Json::htmlEncode($this->attributes);
            $view = $this->getView();
            ActiveFormAsset::register($view);
            $view->registerJs("jQuery('#$id').yiiActiveForm($attributes, $options);");
        }

        echo Html::endForm();
	}


	public function field($model, $attribute, $options = [])
  {
		//if ($model->isAttributeRequired($attribute))
			//$options['inputOptions']['required'] = true;
		//if ($model->isAttributeRequired($attribute))
			//$options['required'] = true;

		$obj = parent::field($model, $attribute, $options);

		//hre($obj);
		if (empty($this->no_auto_require))
		{
			if ($model->isAttributeRequired($attribute))
				$obj->inputOptions['required'] = true;
		}

		if ('email' == $attribute)
			$obj->inputOptions['type'] = 'email';


		return $obj;
	}
}