<?php

namespace common\widgets;


class AjaxLoad extends base\Widget
{
	public $url;
	public $id;
	public $refresh_secs;
	public $refresh_auto = false;
	public $refresh_btn = false;
	public $refresh_checkbox = false;
	public $refresh_hide_sel = '';

	public $initial_content = '';

  public static $n_widget = 0;


	public function init()
	{
		static::$n_widget++;
		parent::init();
	}
}
