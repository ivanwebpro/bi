<?php

namespace common\widgets;

use common\helpers\ArrayHelper;
use common\helpers\Html;
use common\helpers\Url;

class NavBs4 extends \yii\bootstrap4\Nav
{
  public function renderItem($item)
    {
        if (is_string($item)) {
            return $item;
        }
        if (!isset($item['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $options = ArrayHelper::getValue($item, 'options', []);
        $items = ArrayHelper::getValue($item, 'items');
        $url = ArrayHelper::getValue($item, 'url', '#');
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);

        if (isset($item['active'])) {
            $active = ArrayHelper::remove($item, 'active', false);
        } else {
            $active = $this->isItemActive($item);
        }

        if (empty($items)) {
            $items = '';
        } else {
            $linkOptions['data-toggle'] = 'dropdown';
            Html::addCssClass($options, ['widget' => 'dropdown']);
            Html::addCssClass($linkOptions, ['widget' => 'dropdown-toggle']);
            if (is_array($items)) {
                $items = $this->isChildActive($items, $active);
                $items = $this->renderDropdown($items, $item);
            }
        }

        Html::addCssClass($options, ['ni' => 'nav-item']);
        Html::addCssClass($linkOptions, 'nav-link');

        if ($this->activateItems && $active) {
            Html::addCssClass($options, 'active'); // In NavBar the "nav-item" get's activated
            Html::addCssClass($linkOptions, 'active');
        }

        $li_content = '';

				if (!empty($item['li_prepend']))
				{
        	$li_content .= $item['li_prepend'];
				}

				$li_content .= Html::a($label, $url, $linkOptions) . $items;

				if (!empty($item['li_append']))
				{
        	$li_content .= $item['li_append'];
				}

        return Html::tag('li', $li_content, $options);
    }


}
