<?php

namespace common\widgets;

use common\helpers\Html;
use common\helpers\ArrayHelper;

class Modal extends \yii\bootstrap\Modal
{
 	public $toggle_button_wrapper = false;

	protected function renderToggleButton()
	{
		$inner = parent::renderToggleButton();

    if ($this->toggle_button_wrapper)
		{
			$wrapper = $this->toggle_button_wrapper;
			$tag = ArrayHelper::remove($wrapper, 'tag', 'div');
    	$inner = Html::tag($tag, $inner, $wrapper);
		}

		return $inner;

	}
}