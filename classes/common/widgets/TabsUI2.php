<?php

namespace common\widgets;

use common\helpers\ArrayHelper;
use common\helpers\Html;
use common\helpers\Url;


class TabsUI2 extends \yii\bootstrap4\Tabs
{

	public $ul_wrap_open = '';
	public $ul_wrap_close = '';

	public $tabs_wrap_open = '';
	public $tabs_wrap_close = '';

	protected function renderItems()
	{
		$headers = [];
		$panes = [];

		if (!$this->hasActiveTab()) {
			$this->activateFirstVisibleTab();
		}

		foreach ($this->items as $n => $item)
		{
			if (!ArrayHelper::remove($item, 'visible', true))
			{
				continue;
			}

			if (is_string($item))
			{
      	//hr("items");
				//hr($this->items); exit;
			}
			if (!array_key_exists('label', $item))
			{
				throw new InvalidConfigException("The 'label' option is required.");
			}

			$encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
			$label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
			$headerOptions = array_merge($this->headerOptions, ArrayHelper::getValue($item, 'headerOptions', []));
			$linkOptions = array_merge($this->linkOptions, ArrayHelper::getValue($item, 'linkOptions', []));
			Html::addCssClass($linkOptions, 'nav-link');

			if (isset($item['items']))
			{
				Html::addCssClass($headerOptions, ['widget' => 'dropdown']);

				if ($this->renderDropdown($n, $item['items'], $panes))
				{
					Html::addCssClass($headerOptions, 'active');
				}

				Html::addCssClass($linkOptions, ['widget' => 'dropdown-toggle']);
				if (!isset($linkOptions['data-toggle']))
				{
					$linkOptions['data-toggle'] = 'dropdown';
				}


				/** @var Dropdown $dropdownClass */
				$dropdownClass = $this->dropdownClass;
				$header = Html::a($label, "#", $linkOptions) . "\n"
					. $dropdownClass::widget([
					'items' => $item['items'],
					'clientOptions' => false,
					'view' => $this->getView()
				]);
			}
			else
			{
				$options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
				$options['id'] = ArrayHelper::getValue($options, 'id', $this->options['id'] . '-tab' . $n);

				Html::addCssClass($options, ['widget' => 'tab-pane']);
				if (ArrayHelper::remove($item, 'active'))
				{
					Html::addCssClass($options, 'active');
					Html::addCssClass($linkOptions, 'active');

					if (!isset($item['url']))
					{
						$linkOptions['aria-selected'] = 'true';
					}
				}

				if (isset($item['url']))
				{
					$header = Html::a($label, $item['url'], $linkOptions);
				}
				else
				{
					if (!isset($linkOptions['data-toggle']))
					{
						$linkOptions['data-toggle'] = 'tab';
					}

					if (!isset($linkOptions['aria-selected']))
					{
						$linkOptions['aria-selected'] = 'false';
					}

					$linkOptions['aria-controls'] = $options['id'];
					$header = Html::a($label, '#' . $options['id'], $linkOptions);
				}

				if ($this->renderTabContent)
				{
					$tag = ArrayHelper::remove($options, 'tag', 'div');
					$panes[] = Html::tag($tag, isset($item['content']) ? $item['content'] : '', $options);
				}
			}

			Html::addCssClass($headerOptions, 'nav-item');

			$tag_header = ArrayHelper::remove($headerOptions, 'tag', 'li');
			$headers[] = Html::tag($tag_header, $header, $headerOptions);

		}

    $r = '';

		$r .= $this->ul_wrap_open;
		$r .= Html::tag('ul', implode("\n", $headers), $this->options);
		$r .= $this->ul_wrap_close;

		$r .= $this->tabs_wrap_open;
		$r .= $this->renderPanes($panes);
		$r .= $this->tabs_wrap_close;

		return $r;

	}
}
