<?php

namespace common\widgets;


class SelList extends base\Widget
{
	public $title = '';
	public $active_id = 10;
	public $els = [];
	public $url_fld = 'id';
	public $type;
	public $field;
	public $container_class;
	public $default;


	public function init()
	{
		parent::init();
	}
}
