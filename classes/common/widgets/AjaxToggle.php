<?php

namespace common\widgets;


class AjaxToggle extends base\Widget
{
	public $id;
	public $initial_content = '';
	public $toggle_selector = '';
	public $toggle_url = '';

  public static $n_widget = 0;

	public function init()
	{
		static::$n_widget++;

    if (empty($this->id))
		{
			$n = static::$n_widget;
    	$this->id = "ajax_toggle_$n";
		}

		parent::init();
	}
}
