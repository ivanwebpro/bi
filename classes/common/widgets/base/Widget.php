<?php

namespace common\widgets\base;

use Yii;
use yii\helpers\Html;

class Widget extends \yii\base\Widget
{
	public function getViewVars()
	{
  	return [];
	}

	public function init()
	{
		parent::init();

		$dir = $_SERVER['DOCUMENT_ROOT'];

		$js = $this->getWidgetJsWeb();

		if (!empty($_GET['d_wjs']) && stristr(get_called_class(), 'triggerchange'))
		{
    	//hre($js);
		}

		if (file_exists($dir.$js))
		{
			$this->view->registerJsFile($js, ['depends' => '\common\assets\AppAsset']);
		}

		$css = $this->getWidgetCssWeb();


		if (file_exists($dir.$css))
		{
			$this->view->registerCssFile($css);
		}
		else
		{
    	//hre($css);
			//hr("CSS: ".$dir.$css);
		}
	}

	public function getAlias()
	{
  	return $this->getClassEnding();
	}

  public function getClassEnding()
	{
		$ns = explode("\\", get_called_class());
		$class = end($ns);
		return $class;
	}

	public function getWidgetViewFile()
	{
  	return $this->getWidgetFolderFile().'/'.
							$this->alias.'-view.php';
	}

	public function getWidgetJsWeb()
	{
  	return $this->getWidgetFolderWeb().'/'.$this->alias.'.js';
	}

	public function getWidgetCssWeb()
	{
  	return $this->getWidgetFolderWeb().'/'.$this->alias.'.css';
	}

	public function getWidgetViewsFolderName()
	{
		//return (new \ReflectionClass($this))->getShortName();
		return $this->alias;
	}

	public function getWidgetFolderFile()
	{
		$r = dirname(__DIR__)."/views/".$this->getWidgetViewsFolderName();
		//hr($r);
		return $r;

  	//return dirname(__DIR__)."/views/".basename(get_called_class());
	}

	public function getWidgetFolderWeb()
	{
		//return Yii::getAlias($css_alias);
		$dr = $_SERVER['DOCUMENT_ROOT'];
		$dx = $this->getWidgetFolderFile();
		$dr = str_ireplace('\\', '/', $dr);
		$dx = str_ireplace('\\', '/', $dx);
		return str_ireplace($dr, '', $dx);
	}

	public function run()
	{
		$vvs = $this->getViewVars();
		foreach($vvs as $vv)
			${$vv} = $this->$vv;

		if (file_exists($f = $this->getWidgetViewFile()))
		{
			ob_start();
			include $f;
			return ob_get_clean();
		}
	}
}
