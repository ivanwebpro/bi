<?php

namespace common\widgets;

use common\helpers\Html;
use common\helpers\ArrayHelper;

class ModalAjax extends \lo\widgets\modal\ModalAjax
{
 	public $toggle_button_wrapper = false;

	public $toggleButton2 = false;

  protected function renderToggleButton2()
  {
  	if (($toggleButton = $this->toggleButton2) !== false)
		{
    	$tag = ArrayHelper::remove($toggleButton, 'tag', 'button');
      $label = ArrayHelper::remove($toggleButton, 'label', 'Show');
      if ($tag === 'button' && !isset($toggleButton['type']))
			{
      	$toggleButton['type'] = 'button';
      }

    	return Html::tag($tag, $label, $toggleButton);
    }
		else
		{
    	return null;
    }
  }


	protected function renderToggleButton()
	{
		$inner = parent::renderToggleButton();

		$inner .= $this->renderToggleButton2();

    if ($this->toggle_button_wrapper)
		{
			$wrapper = $this->toggle_button_wrapper;
			$tag = ArrayHelper::remove($wrapper, 'tag', 'div');
    	$inner = Html::tag($tag, $inner, $wrapper);
		}

		return $inner;

	}

 public function runZ()
    {
        parent::run();

        /** @var View */
        $view = $this->getView();
        $id = $this->options['id'];

        //ModalAjaxAsset::register($view);

        if (!$this->url && !$this->selector) {
            return;
        }

        switch ($this->mode) {
            case self::MODE_SINGLE:
                $this->registerSingleModal($id, $view);
                break;

            case self::MODE_MULTI:
                $this->registerMultyModal($id, $view);
                break;
        }

        if (!isset($this->events[self::EVENT_MODAL_SUBMIT])) {
            $this->defaultSubmitEvent();
        }

        $this->registerEvents($id, $view);
    }
}