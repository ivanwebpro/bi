<?php

namespace common\widgets;

use common\helpers\Html;

use Yii;
use yii\base\InvalidCallException;
use yii\base\Widget;
use yii\base\Model;
use yii\widgets\ActiveFormAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\widgets\ActiveField as YiiActiveField;

class ActiveField extends YiiActiveField
{
	public $clean_parts = false;


  public function error($options = [])
  {
  	if ('' == $this->model->getFirstError($this->attribute))
		{
    	$this->parts['{error}'] = '';
      return $this;
		}

		return parent::error($options);
	}


	public function render($content = null)
	{
		/*if ($this->clean_parts)
		{
			//hr($this->parts);
			$ps = ['input', 'label', 'error', 'hint'];

			foreach($ps as $p)
			{
				$k = "{".$p."}";
				if (empty($this->parts[$k]))
				{
					$this->template = str_replace($k, '', $this->template);
					//unset($this->parts[$k]);
				}
			}
		} */

		return parent::render($content);
	}

}